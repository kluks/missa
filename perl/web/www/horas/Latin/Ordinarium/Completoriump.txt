#Incipit
V. Iube, domne, bened�cere.
Benedictio. Noctem qui�tam et finem perf�ctum conc�dat nobis D�minus omn�potens. Amen.

#Lectio brevis
!1 Pet 5:8-9
Fratres: S�brii est�te, et vigil�te: quia advers�rius vester di�bolus tamquam leo r�giens c�rcuit, qu�rens quem d�voret: cui res�stite fortes in fide. 
$Tu autem

V. Adjut�rium n�strum + in n�mine D�mini. 
R. Qui fecit c�lum et terram.
$Pater noster
$Confiteor
$Misereatur
$Indulgentiam

V. Conv�rte nos +++ Deus, salut�ris noster.
R. Et av�rte iram tuam a nobis.
$Deus in adjutorium
&Gloria
&Alleluia

#Psalmi

#Hymnus {:H-Completorium:} 
v. Te lucis ante t�rminum,
Rerum Cre�tor, p�scimus,
Ut pro tua clem�ntia
Sis pr�sul et cust�dia.
_
Procul rec�dant s�mnia,
Et n�ctium phantasmata;
Host�mque n�strum c�mprime,
Ne pollu�ntur c�rpora.
_
* Pr�sta, Pater pi�ssime,
Patr�que compar �nice,
Cum Sp�ritu Par�clito
Regnans per omne s�culum.
Amen.

#Capitulum Responsorium Versus
!Jer 14:9
v. Tu autem in nobis es, D�mine, et nomen sanctum tuum invoc�tum est super nos: ne derel�nquas nos, D�mine, Deus noster. 
R. Deo gr�tias.
_
R.br. In manus tuas, D�mine, comm�ndo sp�ritum meum, * Allel�ia, allel�ia
R. In manus tuas, D�mine, comm�ndo sp�ritum meum, * Allel�ia, allel�ia
V. Redem�sti nos, D�mine, Deus verit�tis. 
R. Allel�ia, allel�ia
&Gloria1
R. In manus tuas, D�mine, comm�ndo sp�ritum meum, * Allel�ia, allel�ia
_
V. Cust�di nos, D�mine, ut pup�llam �culi, allel�ia
R. Sub umbra al�rum tu�rum pr�tege nos, allel�ia

#Canticum Nunc dimittis
Ant. Salva nos 
&canticum(3)
&Gloria
Ant. {::}Salva nos, D�mine, vigil�ntes, cust�di nos dormi�ntes; ut vigil�mus cum Christo, et requiesc�mus in pace. Allel�ia 

#Preces Dominicales
v. Kyrie el�ison. Christe el�ison. Kyrie, el�ison.
&pater_noster
$Credo
V. Bened�ctus es, D�mine, Deus patrum nostr�rum. 
R. Et laud�bilis et glori�sus in s�cula.
V. Benedic�mus Patrem et F�lium cum Sancto Sp�ritu. 
R. Laud�mus, et superexalt�mus eum in s�cula.
V. Bened�ctus es, D�mine, in firmam�nto c�li. 
R. Et laud�bilis, et glori�sus, et superexalt�tus in s�cula.
V. Bened�cat et cust�diat nos omn�potens et mis�ricors D�minus. 
R. Amen.
V. Dign�re, D�mine, nocte ista. 
R. Sine pecc�to nos custod�re.
V. Miser�re nostri, D�mine. 
R. Miser�re nostri.
V. Fiat miseric�rdia tua, D�mine, super nos. 
R. Quem�dmodum sper�vimus in te.
V. D�mine ex�udi orati�nem meam.
R. Et clamor meus ad te v�niat

#Oratio
&Dominus_vobiscum1
v. Or�mus. 	
r. V�sita, qu�sumus, D�mine, habitati�nem istam, et omnes ins�dias inim�ci ab ea l�nge rep�lle: �ngeli tui sancti h�bitent in ea, qui nos in pace cust�diant; et bened�ctio tua sit super nos semper.
$Per Dominum

#Conclusio
&Dominus_vobiscum
&Benedicamus_Domino
Benedictio. Bened�cat et cust�diat nos omn�potens et mis�ricors D�minus, + Pater, et F�lius, et Sp�ritus Sanctus.
R. Amen.

&antiphona_finalis
V. Div�num aux�lium + maneat semper nob�scum.
R. Amen.
$Pater noster
$Ave Maria
$Credo
