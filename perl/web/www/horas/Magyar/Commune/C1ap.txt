[Name]
Commune Evangelistarum tempore Paschali

[Rule]
Psalmi Dominica
Antiphonas horas
Una Antiphona

[Capitulum Vespera]
!Wis 5:1
v. Akkor az igaz teljes biztons�ggal �ll szemben azokkal, akik sanyargatt�k, �s lebecs�lt�k a f�radoz�s�t.
$Deo gratias

[Hymnus Vespera]
{:H-Tristeserantapostoli:}v. B�sultak az apostolok,
Mert Krisztus meghalt keser�n,
Gonosz szolg�k mondtak re�
Istentelen �t�letet.
_
Igazmond� angyal szav�t
Hallj�k a j�mbor asszonyok:
A hiv� ny�jhoz m�r k�zel
Krisztus az �r�m h�rivel.
_
A retteg� apostolok
El� viszik a dr�ga h�rt,
�m �tjukon ragyogva m�r
El�tt�k Krisztus l�ba j�r.
_
Galilea hegyeire
Sietnek az apostolok
S betelik v�gyuk: boldogok
J�zus kegyes f�ny�ben �k.
_
Hogy mind�r�kk� megmaradj,
H�sv�ti �r�m�l nek�nk,
Mentsd meg a b�nhal�l el�l,
Kiket az �let �jra sz�lt.
_
* Atyaistennek tisztelet
S Fi�nak, ki felt�madott
S a Vigasztal�nak vel�k
Most �s �r�k id�k�n �t.
�men.

[Versum 1]
V. Szentek �s igazak �rvendezzetek az �rban, alleluia.
R. Isten a ti �r�r�szetek, alleluia.

[Ant 1]
�r�k vil�goss�g * vil�gos�tsa meg szentjeidet Uram, az �r�kk�val�sag vil�gossaga

[Invit]
Az Urat, az Apostolok kir�ly�t * J�jjetek �ldjuk. Alleluia.

[Hymnus Matutinum]
@Commune/C1p:Hymnus Vespera

[Lectio1]
@Commune/C1a:Lectio1

[Lectio2]
@Commune/C1a:Lectio2

[Lectio3]
@Commune/C1a:Lectio3

[Lectio7]
@Commune/C1a:Lectio7

[Lectio8]
@Commune/C1a:Lectio8

[Lectio9]
@Commune/C1a:Lectio9

[Capitulum Laudes]
!Wis 5:1
v. Akkor az igaz teljes biztons�ggal �ll szemben azokkal, akik sanyargatt�k, �s lebecs�lt�k a f�radoz�s�t.
$Deo gratias

[Hymnus Laudes]
{:H-Paschalemundogaudium:}v. F�ldszerte h�sv�t �r�m�t
Sz�ps�gesebb nap hirdeti:
�j f�nyben l�tj�k f�nyleni
Ma J�zust az Apostolok.
_
Krisztus test�n a sebhelyek
Csillag m�dj�ra f�nylenek.
Csod�lj�k, h�s�ges tan�k,
�s hangos sz�val hirdetik.
_
Krisztus, kir�ly, j�s�gos �r,
Vedd sz�veinket birtokul.
Nyelv�nk nevednek r�ja le
K�teles h�la�nek�t.
_
Hogy mind�r�kk� megmaradj
H�sv�ti �r�m�l nek�nk,
Mentsd meg a b�nhal�l el�l,
Kiket az �let �jrasz�lt.
_
* Dics�s�g teneked, Urunk,
Ki holtodb�l felt�mad�l,
Aty�nak s Szentl�leknek is,
Most �s �r�k id�k�n �t.
�men.

[Versum 2]
V. �rt�kes az �r szem�ben, alleluia
R. Szentjei hal�la, alleluia

[Ant 2]
Jeruzs�lem le�nyai * j�jjetek �s n�zz�tek a v�rtan�k koron�j�t, amellyel az �r megkoron�zta �ket. dics�s�ge �s �r�me napj�n. alleluia, alleluia.

[Lectio Prima]
!Rom 8:28
v. Tudjuk azt is, hogy akik Istent szeretik, azoknak minden javukra v�lik, hiszen � saj�t elhat�roz�s�b�l v�lasztotta ki �ket.

[Capitulum Tertia]
!Wis 5:1
v. Akkor az igaz teljes biztons�ggal �ll szemben azokkal, akik sanyargatt�k, �s lebecs�lt�k a f�radoz�s�t.
$Deo gratias

[Responsory Tertia]
R.br. Szentek �s igazak, �rvendezzetek az �rban, Alleluia, alleluia
R. Szentek �s igazak, �rvendezzetek az �rban, Alleluia, alleluia.
V. Isten az �r�kr�szetek
R. Alleluia, alleluia
&Gloria
R. Szentek �s igazak, �rvendezzetek az �rban, Alleluia, alleluia.
_
V. �r�k vil�goss�g vil�gositsa meg szentjeidet Uram. Alleluia
R. Az �r�kk�val�sag vil�goss�ga. Alleluia

[Capitulum Sexta]
!Sap. 5.5
v. Behold how they are numbered among the children of God, and their lot is among the saints.
$Deo gratias

[Responsory Sexta]
R.br. �r�k vil�goss�g vil�gositsa meg szentjeidet Uram alleluia, alleluia
R. �r�k vil�goss�g vil�gositsa meg szentjeidet Uram alleluia, alleluia
V. Az �r�kk�val�sag vil�goss�ga
R. Alleluia, alleluia
&Gloria
R. �r�k vil�goss�g vil�gositsa meg szentjeidet Uram * Alleluia, alleluia.
_
V. �r�k boldogs�g a koszor�juk, alleluia
R. �r�m �s boldogs�g a r�sz�k, alleluia

[Capitulum Nona]
!Rom 8:28
v. Tudjuk azt is, hogy akik Istent szeretik, azoknak minden javukra v�lik, hiszen � saj�t elhat�roz�s�b�l v�lasztotta ki �ket.
$Deo gratias

[Responsory Nona]
R.br. �r�k boldogs�g a koszor�juk, alleluia, alleluia
R. �r�k boldogs�g a koszor�juk, alleluia, alleluia
V. �r�m �s boldogs�g a r�sz�k
R. Alleluia, alleluia
&Gloria
R. �r�k boldogs�g a koszor�juk, alleluia, alleluia
_
V. �rt�kes az �r szem�ben, alleluia
R. Szentjei hal�la, alleluia

[Ant 3]
Szentek �s igazak * �rvendezzetek az �rban, alleluia, Isten a ti �r�r�szetek, alleluia.
