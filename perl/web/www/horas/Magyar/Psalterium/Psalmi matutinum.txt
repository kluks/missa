[Day0]
Boldog f�rfi * aki az Ur t�rv�ny�n elm�lkedik;;1
Szolg�ljatok az �rnak f�lelemmel, * cs�kolj�tok meg a l�b�t retteg�ssel;;2
Kelj fel Uram * ments meg engem Istenem;;3
V. �jjel megeml�kezem nevedr�l, Uram
R. �s megtartom t�rv�nyedet.
Milyen csod�latos * sz�les e vil�gon a te neved, Urunk;;8
Mint igazs�gos b�r� * �lt�l tr�nodra;;9(1-11) 
Kelj fel, Uram * nehogy elb�zza mag�t az ember;;9(12-21)
V. M�g �jszaka is f�lkelek
R. Hogy �ldjalak igaz v�gz�sed�rt.
Uram mi�rt * vagy oly messze;;9(22-32)
Kelj fel, Uram * � Istenem, emeld f�l kezed;;9(33-39)
Igaz az Ur * szereti az igazs�goss�got;;10
V. Szemem m�r az �jjeli �rs�g el�tt �bred, 
R. Hogy megfontoljam tan�t�sod szav�t.

[Day1]
Az �r az �gb�l * az emberekre tekint,;;13
Aki igazs�gban �l * szent hegyeden nyugszik meg Uram;;14
Ford�tsd fel�m f�led Uram * jusson el�d szavam!;;16
V. Mutasd meg csod�latos irgalmass�godat
R. Hisz megmented az ellens�gt�l azokat, akik benned b�znak
Szeretlek * Uram, �n er�ss�gem,;;17(1-16)
�gy viszonozta az �r * igaz voltomat;;17(17-35)
�ljen az �r * dics�s�g �dv�m Isten�nek;;17(36-51)
V. A n�pek k�z�tt dics��tlek, Uram
R. Nevedr�l �nekelek
A megpr�b�ltat�s napj�n * hallgasson meg az �r;;19
Hatalmadnak �r�l * Uram, a kir�ly;;20
Dics��tlek, Uram * mivel megmentett�l;;29
V. Ti, akik szeretitek, dics��ts�tek az Urat
R. Magasztalj�tok szent eml�k�t

[Day2]
Gy�zd le azokat Uram * kik ellenem harcolnak;;34(1-10)
Oltalmazd meg * d�h�kt�l �letem Uram;;34(11-17)
�bredj fel Uram * kelj v�delmemre;;34(18-28)
V. Az �n nyelvem pedig hirdesse igazs�goss�godat
R. S mind�r�kk� zengje dics�reted!
Ne irigyeld * aki boldogul megszegve a t�rv�nyt;;36(1-15)
A b�n�s karja elt�rik *  �m az igazat az �r t�mogatja;;36(16-29)
N�zd az �rtatlant * tekints az igazra;;36(30-40)  
V. B�zz�l az �rban �s k�vesd az �tj�t 
R. O meg�riz a gonoszt�l s � felemel, hogy ti�d legyen a f�ld
Ne b�ntess engem * felindul�sodban;;37(1-11)
Siess seg�ts�gemre * Uram, �dv�ss�gem;;37(12-23)
Vedd le r�lam * csap�saidat;;38
V. Istenem, ne z�rk�zz el s�r� szavam el�l 
R. El�tted csak j�vev�ny vagyok, zar�ndok,

[Day3]
Sz�p vagy, szebb * mint b�rki az emberek fiai k�z�l. Ajkadon kellem �mlik el;;44(1-10)
Magasztaljanak a n�pek * Isten�nk mind�r�kk�;;44(11-18)
T�maszunk a szorongattat�sban * Isten�nk;;45
V. Vel�nk a Seregek Ura
R. J�kob Istene a mentsv�runk
Nagy az �r * m�lt� a dics�retre, Isten�nk v�ros�ban;;47
A sz�m b�lcsess�get hirdet * okos besz�det sugall a sz�vem;;48(1-13)
Ne m�ltatlankodj * dics�s�ge nem megy el a gazdaggal a sirba;;48(14-21)  
V. Az �n lelkemet Isten kimenti az alvil�g karmaib�l
R. �s mag�hoz vesz.
Az �r * az istenek Istene sz�lt;;49(1-15)
Sz�lljatok magatokba * akik megfeledkeztek Istenr�l;;49(16-23)
Elfogadod az igaz �ldozatokat * amit olt�rodra visznek;;50
V. Hadd halljam �r�mnek �s boldogs�gnak h�r�t
R. Ujjongani fognak megt�rt csontjaim

[Day31]
Sz�p vagy, szebb * mint b�rki az emberek fiai k�z�l. Ajkadon kellem �mlik el;;44(1-10)
Magasztaljanak a n�pek * Isten�nk mind�r�kk�;;44(11-18)
T�maszunk a szorongattat�sban * Isten�nk;;45
V. Vel�nk a Seregek Ura
R. J�kob Istene a mentsv�runk
Nagy az �r * m�lt� a dics�retre, Isten�nk v�ros�ban;;47
A sz�m b�lcsess�get hirdet * okos besz�det sugall a sz�vem;;48(1-13)
Ne m�ltatlankodj * dics�s�ge nem megy el a gazdaggal a sirba;;48(14-21)  
V. Az �n lelkemet Isten kimenti az alvil�g karmaib�l
R. �s mag�hoz vesz.
Az �r * az istenek Istene sz�lt;;49(1-6)
A dics�ret �ldozat�t * vidd az �r el�;;49(7-15)
Sz�lljatok magatokba * akik megfeledkeztek Istenr�l;;49(16-23)
V. Aki tisztel engem, az a h�dolat �ldozat�t hozza
R. J�n, hogy megmentse n�p�t

[Day4]
�dv�m �s dics�s�gem * Istent�l v�rom, � az �n mened�kem.;;61
L�ss�tok az �r tetteitt * harsogj�tok it�let�t;;65(1-12)
Gyertek ide mind * hallj�tok mily nagy dolgot tett velem;;65(13-20)
V. K�r�sem nem vetette el
R. Irgalmass�g�t nem vonta meg t�lem.
F�lkel az Isten * ellens�gei sz�tsz�r�dnak;;67(1-11)
Isten a mi �dv�ss�g�nk Istene * a Hatalmas meg�v a hal�lt�l;;67(12-24)
Templomaiban * �ldj�tok Istent az Urat;;67(25-36)
V. F�lelmetes az Isten az � szent hely�n
R. Izrael Istene, � ad n�p�nek er�t �s hatalmat
Ments meg, Istenem, * a v�z m�r torkomig �r;;68(1-13)
Ellens�geimt�l * ments meg Uram;;68(14-29)
Keress�tek az Urat * �s �lni fog lelketek;;68(30-37)
V. Dics��tem Isten nev�t �nekemben
R. �nnepi h�l�val magasztalom

[Day5]
Parancsul adta J�kobnak * s mint t�rv�nyt rendelte Izraelnek;;77(1-8)
Csod�kat tett * az aty�k szeme l�tt�ra;;77(9-16)
Mann�t hullatott az �gb�l * eledel�l, �gi kenyeret adott nekik;;77(17-31)
V. Isten r�csapott a szikl�ra, �s megeredt a v�z
R. El�t�rt a forr�s
Isten a szikl�juk * a f�ls�ges Isten a szabad�t�juk.;;77(32-41)
Kiszabad�totta * az ellens�g hatalm�b�l �ket;;77(42-58)
Isten �p�tette * a szent�lyt, szil�rdd� tette, mint a f�ldet;;77(59-72)
V. J�d�t v�lasztotta ki mag�nak
R. Sion hegy�t, melyet szeretett.
Neved dics�s�g��rt * ments meg minket, Isten�nk, bocs�sd meg, b�neinket;;78
�n vagyok az Ur * Izrael Istene, aki kihoztalak t�ged Egyiptom f�ldj�r�l;;80
Ne hallgass Istenem * ellens�geid fennen hordj�k fej�ket;;82
V. Hadd ismerjenek meg t�ged, akinek neve: �r
R. Az eg�sz vil�gon egyed�l te vagy f�ls�ges.

[Day6]
Mindv�gig eml�kezik * sz�vets�g�re, �g�ret�re, a mi Urunk Isten�nk;;104(1-15)
Az �r megsokas�totta n�p�t * er�sebb� tette elnyom�in�l;;104(16-27)
Ujjong�s k�zt vezette ki n�p�t * �r�mrivalg�s k�zepette v�lasztottait.;;104(28-45)  
V. Mert megeml�kezett szent �g�ret�r�l 
R. amit �brah�mnak, a szolg�j�nak tett.
Megmentette �ket * neve miatt;;105(1-15)
Elfeledt�k Istent * aki megmentette �ket;;105(16-28)
�m ism�t r�tekintett * nyomor�s�gukra, mihelyt meghallotta k�ny�rg�s�ket;;105(29-48)
V. Ments meg minket, Urunk, mi Isten�nk
R. Hogy magasztaljuk szent nevedet`
Az �rhoz ki�ltottak * s megmentette �ket minden f�lelm�kt�l;;106(1-14)
�k is l�tt�k az �r tetteit * szeml�lt�k csod�it;;106(15-30)
L�tj�k az igazak * �s �rvendeznek, meg�lik az Isten irgalm�t;;106(31-43)
V. Adjanak h�l�t az Urnak a n�p gy�lekezet�ben
R. A v�nek tan�cs�ban dics��ts�k!

[Quad 0 Versum]
V. Paizs gyan�nt vesz t�ged k�r�l az � igazs�ga; 
R. Nem fogsz f�lni az �jjeli r�mt�l, 

[Quad5 0 Versum]
V. Isten! ne vesz�tsd el az istentelenekkel az �n lelkemet, 
R. �s a v�rszop� f�rfiakkal �letemet

[Pasc 0 Versum]
V. Felt�madott az �r a sirb�l. Alleluja
R. Aki �rett�nk a keresztf�n f�gg�tt. Alleluja

[Quad 1 Versum]
V. Megszabad�tott engem a vad�szok t�r�t�l.
R. �s az �les szavakt�l

[Quad5 1 Versum]
V. Ments meg, oh Isten! lelkemet a fegyvert�l; 
R. Egyetlenemet az eb kez�b�l.

[Pasc 1 Versum]
V. Felt�madott az �r a sirb�l. Alleluja
R. Aki �rett�nk a keresztf�n f�gg�tt. Alleluja

[Quad 2 Versum]
V. V�llaival meg�rny�koz t�ged, 
R. Sz�rnyai alatt biztons�gban vagy.

[Quad5 2 Versum]
V. Szabad�ts meg engem az oroszl�n sz�j�b�l; 
R. Engem, leal�zottat, az egyszarv�ak szarvait�l. 

[Pasc 2 Versum]
V. Felt�madott az �r a sirb�l. Alleluja
R. Aki �rett�nk a keresztf�n f�gg�tt. Alleluja

[Quad 3 Versum]
V. Paizs gyan�nt vesz t�ged k�r�l az � igazs�ga; 
R. Nem fogsz f�lni az �jjeli r�mt�l, 

[Quad5 3 Versum]
V. Isten! ne vesz�tsd el az istentelenekkel az �n lelkemet, 
R. �s a v�rszop� f�rfiakkal �letemet

[Pasc 3 Versum]
V. �r�m t�lt�tte el a tan�tv�nyokat. Alleluja
R. Az �r l�tt�ra. Alleluja

[Quad 4 Versum]
V. Ipse liberavit de laqueo venantium
R. Et a verbo aspero

[Quad5 4 Versum]
V. Ments meg, oh Isten! lelkemet a fegyvert�l; 
R. Egyetlenemet az eb kez�b�l.

[Pasc 4 Versum]
V. Felt�madott az �r a sirb�l. Alleluja
R. Aki �rett�nk a keresztf�n f�gg�tt. Alleluja

[Quad 5 Versum]
V. V�llaival meg�rny�koz t�ged, 
R. Sz�rnyai alatt biztons�gban vagy. 

[Quad5 5 Versum]
V. Szabad�ts meg engem az oroszl�n sz�j�b�l; 
R. Engem, leal�zottat, az egyszarv�ak szarvait�l. 

[Pasc 5 Versum]
V. Valoban feltamadott az Ur. Alleluia
R. �s megjelent Simonnak. Alleluia

[Quad 6 Versum]
V. Paizs gyan�nt vesz t�ged k�r�l az � igazs�ga; 
R. Nem fogsz f�lni az �jjeli r�mt�l, 

[Quad5 6 Versum]
V. Isten! ne vesz�tsd el az istentelenekkel az �n lelkemet, 
R. �s a v�rszop� f�rfiakkal �letemet

[Pasc 6 Versum]
V. �r�m t�lt�tte el a tan�tv�nyokat. Alleluja
R. Az �r l�tt�ra. Alleluja

[Pasc Ant Dominica]
Alleluia * A k�vet elhenger�tetettek. Alleluja. A s�r bej�rata el�l Alleluja, alleluia.
;;
;;
V. Felt�madott az �r a sirb�l. Alleluja
R. Aki �rett�nk a keresztf�n f�gg�tt. Alleluja
Alleluia * Kit keresel, asszony? alleluia: �lot a holtak kozott? alleluia, alleluia.
;;
;;
V. Val�ban felt�madt az �r, alleluia.
R. �s megjelent Simonnak, alleluia.
Alleluia * Ne f�lj, M�ria, alleluia: feltamadott az Ur, alleluia, alleluia.
;;
;;
V. �r�m t�lt�tte el a tan�tv�nyokat. Alleluja
R. Az �r l�tt�ra. Alleluja

[Pasc Ant Feria]
Alleluia * Alleluia, Alleluia
;;
;;
V. Az �r meguj�t minket �l� rem�nnyel, alleluia.
R. Krisztus halottaib�l val� felt�mad�sa �ltal, alleluia.
Alleluia * Alleluia, Alleluia
;;
;;
V. Isten felt�masztotta Krisztust halottaib�l, alleluia.
R. Hogy hit�nk �s rem�ny�nk Istenben legyen, alleluia.
Alleluia * Alleluia, Alleluia
;;
;;
V. Isten az Urat t�masztotta fel, alleluia.
R. �s minket is felt�maszt hatalm�val, alleluia.

