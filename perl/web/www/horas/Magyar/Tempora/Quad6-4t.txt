@Tempora/Quad6-4

[Lectio1]
Olvasm�ny Jeremi�s pr�f�ta siralmainak k�nyv�b�l
!Lam 1:1-6
1 �, mily elhagyottan �l a v�ros, amely egykor tele volt n�ppel! Olyan, mint az �zvegy, b�r nagy volt a n�pek k�z�tt. A tartom�nyok �rn�j�t, l�m, robotra fogt�k.
2 Csak s�r, egyre s�r �jjelente, k�nnyei peregnek az arc�n. Azok k�z�l, akik szerett�k, egy sincs, hogy vigasztalja. Bar�tai mind cserbenhagyt�k, �s ellens�geiv� lettek.
3 Sz�mkivet�sbe ker�lt J�da, al�vetve elnyom�snak, neh�z szolgas�gnak. A pog�ny n�pek k�z�tt lakik, s nem tal�l nyugalmat. �ld�z�i mind utol�rt�k, ott, ahol nem volt menekv�se.
4 A Sionra viv� utak gy�szolnak, mert nincs, aki �nnepelni j�nne. Kapui mind elhagyottak, papjai s�hajtoznak. L�nyai gy�szban vannak, �s � maga is csupa keser�s�g.
5 Akik szorongatt�k, f�l� kerekedtek, ellens�gei d�sk�lkodnak. Maga az �r s�jtott le r�, t�m�rdek v�tke miatt. Gyermekei fogs�gba mentek, az elnyom� szeme l�tt�ra.
6 Sion le�ny�nak elt�nt minden dics�s�ge. Fejedelmei olyanok lettek, mint a kosok, amelyek nem tal�lnak legel�t. Erej�ket vesztve vonszolj�k magukat �ld�z�ik el�tt.
r. Jerusalem, Jerusalem, t�rj meg Uradhoz Istenedhez.

[Lectio2]
!Lam 1:7-11
7 Jeruzs�lem eml�kezet�be id�zi nyomor�s�ga �s elnyomat�sa napjait, amikor n�pe ellens�g kez�re ker�lt, �s senki nem sietett seg�ts�g�re. Elnyom�i l�tt�k ezt, �s mulattak pusztul�s�n.
8 S�lyosan v�tkezett Jeruzs�lem, �s tiszt�talann� v�lt. Azok, akik tisztelt�k, most mind megvetik, mert l�tj�k gyal�zat�t. � maga meg csak s�hajtozik, �s sz�gyen�ben elford�tja arc�t.
9 Szenny bor�tja ruh�ja szeg�ly�t; nem gondolta volna, hogy ilyen v�gre jut. Sz�rny� m�lyre s�llyedt, �s nincs, aki vigasztalja. "Uram, l�sd meg nagy nyomor�s�gomat, mert ellens�gem ujjong f�l�ttem."
10 Az ellens�g kiny�jtotta kez�t minden dr�gas�ga fel�. S l�tnia kellett, amint a pog�nyok behatoltak szent�ly�be; azok, akiknek megtiltottad, hogy bel�pjenek gy�lekezetedbe.
11 Eg�sz n�pe s�hajtozik, �s keny�r ut�n eseng. Odaadj�k �lelem�rt minden kincs�ket, csak hogy megments�k �let�ket. "N�zz r�m, Uram, �s vedd �szre, mennyire megvetnek!"
r. Jerusalem, Jerusalem, t�rj meg Uradhoz Istenedhez.

[Lectio3]
!Lam 1:12-16
12 �, ti mindny�jan, akik erre j�rtok az �ton, n�zzetek ide �s l�ss�tok: Van-e oly f�jdalom, mint az �n f�jdalmam? Mert les�jtott r�m az �r izz� haragj�nak napj�n.
13 T�zet bocs�tott le a magasb�l, s az �tj�rta minden csontomat. H�l�t fesz�tett ki a l�bam el�tt, �s elbuktatott. Elhagyatott lettem, s nyomorult egyszer s mindenkorra.
14 S�lyosan r�m nehezedik v�tkeim ig�ja, az � keze r�tta �ssze. Az �r ig�ja nyomja nyakamat, s marad�k er�m fogyt�n van. Ellens�geim k�nye-kedv�re hagyott az �r, s nincs er�m, hogy ellen�lljak.
15 Megal�zta az �r vit�z harcosaimat; �nnepi gy�lekezetet h�vott egybe ellenem, hogy eltiporja ifjaimat. Sajt�j�ban az �r �sszetaposta J�da hajadon le�ny�t.
16 Ez�rt s�rok oly keservesen, szememb�l patakzik a k�nny. S vigasztal�m, aki er�t �nthetne bel�m, messze van t�lem. Fiaim nyomor�s�gra jutottak, mert az ellens�g fel�lkerekedett.
r. Jerusalem, Jerusalem, t�rj meg Uradhoz Istenedhez.

