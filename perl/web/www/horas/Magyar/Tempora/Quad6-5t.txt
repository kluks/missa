@Tempora/Quad6-5

[Lectio1]
Olvasm�ny Jeremi�s pr�f�ta siralmainak k�nyv�b�l
!Lam 2:8-12
8 Elhat�rozta az �r, hogy lerombolja Sion le�ny�nak falait. Kifesz�tette a m�r�zsin�rt, s nem vonta vissza a kez�t, am�g teljesen el nem puszt�totta. Gy�szba borult a s�nc �s a k�fal; romokban hever mind a kett�.
9 Kapui a f�ldbe s�llyedtek; leverte �s �sszet�rte z�rait. Kir�lya �s fejedelmei a pog�nyok k�zt vannak, megsz�nt a t�rv�ny. Pr�f�t�i nem kapnak t�bb� l�tom�st az �rt�l.
10 A f�ld�n �lnek sz�tlanul Sion le�ny�nak v�nei. Hamut sz�rtak a fej�kre, �s sz�rzs�kba �lt�ztek. M�lyen lehorgasztott�k a fej�ket Jeruzs�lem hajadon le�nyai.
11 Szemem elsorvadt a s�r�st�l, a bens�m megrend�lt. Megfordult a sz�vem bennem n�pem le�ny�nak roml�sa miatt. Mert apr� gyerekek �s csecsem�k pusztulnak el a v�ros terein.
12 Azt k�rdezgett�k anyjukt�l, hogy hol a keny�r �s a bor, amikor elpusztultak, mint a sebes�ltek a v�ros terein, �s amikor kilehelt�k lelk�ket anyjuk �l�ben.
r. Jerusalem, Jerusalem, t�rj meg Uradhoz Istenedhez.

[Lectio2]
!Lam 2:13-17
13 Mihez hasonl�tsalak, mivel vesselek �ssze, Jeruzs�lem le�nya? Ki tudna megvigasztalni, Sionnak sz�z le�nya? Mert nagy, mint a tenger, a f�jdalmad. Ki gy�gy�thatna meg?
14 Amit pr�f�t�id j�vend�ltek neked, az csal�rds�g volt �s balgas�g. Soha nem t�rt�k fel v�tkedet, hogy ind�tsanak megt�r�sre, Hamis l�tom�sokkal hitegettek, becsaptak �s f�lrevezettek.
15 Akiknek erre visz az �tjuk, �sszecsapj�k a kez�ket, amikor megl�tnak. Felszisszennek, fej�ket cs�v�lj�k Jeruzs�lem le�nya miatt: Ezz� lett a sz�pek sz�pe, az eg�sz f�ldnek �kess�ge?
16 Kit�rta ellened a sz�j�t minden ellens�ged. F�ttyentenek, fogukat csikorgatj�k, �s ezt mondj�k: "Felfaltuk! Ez az a nap, amelyre v�rtunk - meg�rt�k, megl�ttuk."
17 Amit elhat�rozott, azt v�gbevitte az �r, teljes�tette szav�t, amelyet m�r r�g kimondott. K�ny�rtelen�l lerombolt! Ellens�geid ujjonghatnak f�l�tted, mert elt�lt�tte �ket er�vel.
r. Jerusalem, Jerusalem, t�rj meg Uradhoz Istenedhez.

[Lectio3]
!Lam 3:1-9
1 Olyan ember vagyok, aki �t�ltem a nyomor�s�got haragj�nak vesszeje alatt.
2 A s�t�ts�gbe vitt �s vezetett, nem a vil�goss�gba.
3 Egyed�l ellenem emeli f�l a kez�t �jra meg �jra, eg�sz nap.
4 Elsorvasztotta h�somat �s b�r�met, �sszet�rte csontjaimat.
5 K�r�ls�ncolt �s k�r�lvett engem ep�vel �s gy�trelemmel.
6 A s�t�ts�gben adott nekem lak�st a r�g megholtak k�z�tt.
7 Fallal vett k�r�l, nem mehetek ki, s�lyos bilincsbe vert.
8 Ha ki�ltok �s k�ny�rg�k, kiz�rja im�ds�gomat.
9 Faragott k�vekkel z�rta el utamat, eltorlaszolta �sv�nyemet.
10 Mint a lesben �ll� medve, olyan lett hozz�m, vagy mint a rejtek�n megb�v� oroszl�n.
11 �sv�nyemet elt�r�tette, �s �sszet�pett, borzalomm� tett.
12 Kifesz�tette �j�t �s c�lba vett, nyilainak c�lpontj�v� lettem.
r. Jerusalem, Jerusalem, t�rj meg Uradhoz Istenedhez.

[Lectio4]
From the Treatise of St Austin, Bishop (of Hippo,) Upon the Psalms
!On Psalm lxiii. 2.
Thou hast hidden me from the secret counsel of the wicked, from the insurrection of the workers of iniquity. Now let us fix our eyes upon our Head. Many martyrs have. suffered such things as He suffered, but God's hiding of His suffering ser; vants is not so well seen in the Martyrs, as it is in the Captain of the Martyrs. And it is in Him that we best see how it fared with them. He was hidden from the secret counsel of the wicked; hidden by God, being Himself God; hidden, as touching the Manhood, by God the Son, and the very Manhood, Which is taken into God the Son; because He is the Son of man, and He is the Son of; God Son of God, as being in the form of God; Son of man, as having taken upon Him the form of a servant, (Phil. ii. 6, 7,) Whose life no man taketh from Him, but Who layeth it down of Himself. He hath power to lay it down, and He hath power to take it again, (John x. 18.) What then was all that they which hated Him could do? They could kill the Body, but they were not able to kill the Soul. Consider this very earnestly. It had been a small thing for the Lord to preach to the Martyrs by His word, if He had not also nerved them by His example.

[Lectio5]
We know what secret counsel was that of the wicked Jews, and what insurrection was that of the workers of iniquity. Of what iniquity were they the workers? The murder of our Lord Jesus Christ. Many good works, saith He, have I showed you for which of those works go ye about to kill Me? He had borne with all their weaknesses : He had healed all their diseases : He had preached unto them the kingdom of heaven : He had discovered to them their iniquities, that they might rather hate them, than the Physician That came to cure them. And now at last, without gratitude for all the tenderness of His healing love, like men raging in an high delirium, throwing themselves madly on the Physician, Who had come to cure them, they took counsel together how they might kill Him, as if to see if He were a Man and could die, or Something more than a man, and That would not let Himself die. In the Wisdom of Solomon we recognize their words, (ii. 18, 19, 20,) Let us condemn Him with a shameful death Let us examine Him; for, by His own saying, He shall be respected. If He be the Son of God, let Him help Him.

[Lectio6]
They whet heir tongue like a sword. The Jews cannot say : We did not murder Christ albeit they gave Him over to Pilate His judge, that they themselves might seem free of His death. For when! Pilate said unto them, Take ye Him: and kill Him, they answered, It; is not lawful for us to put any man to death. They could throw th� blame of their sin upon a human judge : but did they deceive God, the Great Judge? In that which Pilate; did, he was their accomplice, but in comparison with them, he had far the lesser sin. (John xix. 11.) Pilat� strove as far as he could, to deliver Him out of their hands; for the which reason also he scourged Him, (John xix. 1,) and brought Him forth to, them He scourged not the Lord for cruelty's sake, but in the hope that; he might so slake their wild thirst for blood: that, perchance, even they might be touched with compassion, and cease to lust for His death, when they saw What He was after the flag's elation. Even this effort he made.! But when Pilate saw that he could! not prevail, but that rather a tumult was made, (Matth. xxvii. 24,) ye know how that he took water, and washed his hands before the multitude, saying: I am innocent of the i Blood of this Just Person. And yet he delivered Him to be crucified! But if he were guilty who did it against his will, were they innocent; who goaded him on to it? No. Pilate gave sentence against Him. and commanded Him to be crucified. But ye, O ye Jews, ye also are His murderers! Wherewith? With your tongue, whetted like a sword. And when? But when ye cried, Crucify Him! Crucify Him!

[Lectio7]
Szent P�l Apostol zsid�khoz irott level�b�l
!Heb 4:11-15
11 T�rekedj�nk teh�t r�, hogy bejussunk nyugalm�nak ebbe az (orsz�g�ba), nehogy valaki �pp�gy eless�k, mint el�bb a hitetlens�g p�ld�j�n l�ttuk.
12 Az Isten szava ugyanis eleven, �that� �s minden k�t�l� kardn�l �lesebb, behatol a l�lek �s szellem, az �z �s a vel� gy�ker�ig, meg�t�li a sz�v gondolatait �s �rz�seit.
13 Semmilyen teremtett dolog nem marad el�tte rejtve; minden f�l van fedve �s nyitva van az el�tt, akinek sz�mad�ssal tartozunk.
14 Mivel teh�t olyan kiv�l� f�papunk van, aki �thatolt az egeken, J�zus, az Isten Fia, legy�nk �llhatatosak a hitvall�sban.
15 F�papunk ugyanis nem olyan, hogy ne tudna egy�tt�rezni gy�nges�geinkkel, hanem olyan, aki hozz�nk hasonl�an mindenben k�s�rt�st szenvedett, a b�nt�l azonban ment maradt.
16 J�ruljunk teh�t bizalommal a kegyelem tr�nj�hoz, hogy irgalmat tal�ljunk �s kegyelmet kapjunk, amikor seg�ts�gre szorulunk.

[Lectio8]
!Heb 5:1-7
1 �vakodjunk ez�rt att�l, hogy k�z�letek b�rki �gy v�lekedjen, m�g k�slekedhet bemenni nyugalm�ba, mert m�g �rv�nyben van az �g�ret.
2 Hozz�nk �pp�gy eljutott az �dv�ss�g �r�mh�re, mint �hozz�juk; �m nekik nem v�lt hasznukra a tan�t�s, mert nem csatlakoztak azokhoz, akik hittel hallgatt�k.
3 Nyugalma orsz�g�ba ugyanis csak akkor jutunk el, ha h�v�k vagyunk. Mert ezt mondta: Megesk�dtem h�t haragomban, hogy nyugalmamba be nem mennek.
4 A tiszts�get mag�t�l senki sem v�llalhatja, csak akit az Isten megh�v, mint �ront.
5 �gy Krisztus sem �nmag�t emelte f�papi m�lt�s�gra, hanem az, aki �gy sz�lt hozz�: "A Fiam vagy, ma sz�ltelek."
6 M�sutt meg ezt mondja: "Pap vagy mind�r�kk� Melkizedek rendje szerint."
7 F�ldi �let�ben hangosan ki�ltozva, k�nnyek k�z�tt im�dkozott, s k�ny�rg�tt ahhoz, aki meg tudta menteni a hal�lt�l, �s h�dolat��rt meghallgat�sra tal�lt.

[Lectio9]
!Heb 5:8-14
8 Annak ellen�re, hogy � volt a Fi�, a szenved�sb�l engedelmess�get tanult.
9 M�ve befejezt�vel pedig �r�k �dv�ss�get szerzett azoknak, akik engedelmeskednek neki,
10 mert az Isten �t hirdette ki f�papnak, Melkizedek rendje szerint.
11 Err�l m�g sok mondanival�nk volna, de neh�z nektek kifejteni, mert k�z�mb�sen hallgatj�tok.
12 Ha az id�t tekintj�k, m�r tan�t�knak kellene lennetek, s m�gis arra szorultok, hogy az isteni tan�t�s elemeire oktassanak benneteket. Tejre van sz�ks�getek, nem szil�rd �telre.
13 Aki tejen �l, j�ratlan az igaz tan�t�sban, hiszen m�g kisgyerek.
14 A t�k�letesnek viszont, aki gyakorlattal megfelel� �rz�ket szerzett a j� �s a rossz megk�l�nb�ztet�s�re, szil�rd eledel val�.

