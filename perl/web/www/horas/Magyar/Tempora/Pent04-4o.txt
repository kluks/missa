[Rank]
Feria quinta infra Hebdomadam IV post Octavam Pentecostes;;Feria;;1

[Rule]
Oratio Dominica

[Lectio1]
Olvasm�ny S�muel els� k�nyv�b�l
!1 Sam 19:1-3
1 Saul elmondta a fi�nak, Jonat�nnak �s a tisztjeinek, hogy meg sz�nd�kozik �lni D�vidot. Saul fia, Jonat�n azonban nagyon szerette D�vidot.
2 Ez�rt tudt�ra adta D�vidnak a dolgot: "Aty�mnak, Saulnak az a sz�nd�ka, hogy meg�l. Ez�rt holnap kora reggel vonulj vissza �s rejt�zz el.
3 Majd kimegyek, aty�m mell� szeg�d�m a t�borban, ahol leszel, �s besz�lek aty�mmal �rdekedben. Azt�n, ha megtudom, hogy �ll a dolog, elmondom neked."

[Responsory1]
@Tempora/Pent01-1:Responsory1

[Lectio2]
!1 Sam 19:4-7
4 Jonat�n csakugyan sz�t emelt D�vid �rdek�ben atyj�n�l. �gy besz�lt: "Ne k�vessen el a kir�ly b�nt szolg�ja, D�vid ellen, hiszen � sem v�tett ellened semmit, ink�bb javadra szolg�ltak tettei.
5 Kock�ztatta �let�t �s legy�zte a filiszteust. �gy nagy gy�zelmet szerzett az �r eg�sz Izraelnek. Magad is l�ttad �s �r�lt�l neki. Mi�rt szennyezed h�t be magad �rtatlan v�rrel, �s �l�d meg D�vidot minden ok n�lk�l?"
6 Saul meghallgatta Jonat�nt �s megesk�d�tt: "Amint igaz, hogy az �r �l, nem �l�m meg!"
7 Erre Jonat�n h�vta D�vidot �s elmondta neki a dolgot. Azt�n odavezette Saulhoz �s D�vid tov�bbra is szolg�lta, mint azel�tt.

[Responsory2]
@Tempora/Pent01-1:Responsory2

[Lectio3]
!1 Sam 19:8-12
8 Amikor a h�bor�skod�s folytat�dott, D�vid kivonult, �s harcolt a filiszteusok ellen, s olyan nagy veres�get m�rt r�juk, hogy megfutamodtak el�le.
9 De Sault hatalm�ba ker�tette az �rnak egy rossz szelleme; amikor a h�z�ban �lt, l�ndzs�t tartva a kez�ben, D�vid meg pengette a h�rf�t,
10 Saul megk�s�relte, hogy D�vidot l�ndzs�j�val a falhoz szegezze, de D�vid kit�rt Saul t�mad�sa el�l, �gyhogy a l�ndzsa a falba f�r�dott. Erre D�vid elmenek�lt �s mentette az �let�t. M�g akkor �jszaka
11 Saul elk�ldte embereit D�vid h�z�ba, hogy �rizz�k, mert reggel meg akarta �lni. De Michal, a feles�ge figyelmeztette D�vidot: "Ha nem ker�lsz m�g az �jjel biztons�gba, reggel meg�lnek."
12 Azt�n Michal leengedte D�vidot az ablakon, s � fogta mag�t �s mentette az �let�t.

[Responsory3]
@Tempora/Pent01-1:Responsory3
