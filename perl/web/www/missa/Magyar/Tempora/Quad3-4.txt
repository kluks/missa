[Rank]
Feria Quinta infra Hebdomadam III in Quadragesima;;Feria major;;2

[Rule]
no Gloria
Suffragium=Sanctorum;Vivis;;
Super popul

[Introitus]
!  
v. I am the salvation of the people, says the Lord. From whatever tribulation they shall cry to Me, I will hear them; and I will be their Lord forever.
!Ps 77:1
Hearken, My people, to My teaching; incline your ears to the words of My mouth.
&Gloria
v. I am the salvation of the people, says the Lord. From whatever tribulation they shall cry to Me, I will hear them; and I will be their Lord forever.

[Oratio]
May the blessed feast of Your saints, Cosmas and Damian, glorify You, O Lord; for on this day in Your marvelous providence, You gave them glory everlasting, and gave us their help.
$Per Dominum

[Lectio]
Olvasm�ny Jeremi�s pr�f�ta k�nyv�b�l
!Jer 7:1-7
v. Ezt a sz�zatot int�zte az �r Jeremi�shoz:  �llj ki az �r h�z�nak kapuj�ba �s hirdesd ott ezt az �zenetet! Mondd: Hallj�tok az �r szav�t, J�da n�pe mindny�jan, akik bel�ptek e kapukon, hogy az Urat im�dj�tok!  Ezt �zeni a Seregek Ura, Izrael Istene: Jav�ts�tok meg utaitokat �s tetteiteket, akkor majd veletek lakom ezen a helyen.  Ne b�zzatok az ilyen hazug besz�dekben: "Az �r temploma, az �r temploma, az �r temploma."  Mert ha jobbak lesztek �letm�dotokban �s tetteitekben; ha jog szerint b�ntok egym�ssal;  ha nem nyomj�tok el az idegent, az �rv�t �s az �zvegyet; ha nem ontotok �rtatlan v�rt ezen a helyen, �s nem szeg�dt�k idegen istenek nyom�ba a saj�t vesztetekre,  akkor veletek lakom majd ezen a f�ld�n, amelyet r�gt�l fogva aty�itoknak adtam, mind�r�kre. 

[Graduale]
!Ps 144:15-16
The eyes of all look hopefully to You, O Lord, and You give them their food in due season.
V. You open Your hand and satisfy the desire of every living thing.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 4:38-44
v. J�zus a zsinag�g�b�l kij�ve, Simon h�z�ba ment. Simon any�sa magas l�zban fek�dt. Sz�ltak neki miatta.  F�l� hajolt, parancsolt a l�znak, �s az nyomban elhagyta. Mindj�rt fel is kelt �s kiszolg�lta.  Napnyugta ut�n hozz�tartoz�ik elvitt�k hozz� a k�l�nf�le bajokban szenved� betegeket. Egyenk�nt r�juk tette kez�t, s meggy�gy�totta �ket.  Sokakb�l gonosz l�lek ment ki, �gy ki�ltozva: "Te az Isten Fia vagy!" De � r�juk sz�lt, s nem engedte sz�hoz jutni �ket, mert hisz tudt�k, hogy � a Krisztus.  Amikor megvirradt, kiment egy elhagyatott helyre. A n�p kereste, m�g meg nem tal�lta. Marasztalt�k, hogy ne hagyja ott �ket.  De � azt felelte: "M�s v�rosokban is kell hirdetnem az Isten orsz�g�t, hiszen ez a k�ldet�sem."  �s hirdette J�dea zsinag�g�iban. 

[Offertorium]
!Ps 137:7
Though I walk amid distress, You preserve me, O Lord; against the anger of my enemies You raise Your hand; Your right hand saves me.

[Secreta]
We offer You, O Lord, in honor of the meritorious death of Your just ones, this sacrifice which is the source of martyrdom.
$Per Dominum

[Communio]
!Ps 118:4-5
You have commanded that Your precepts be diligently kept. Oh, that I might be firm in the ways of keeping Your statutes!

[Postcommunio]
O Lord, we ask You through the merits of Your blessed martyrs, Cosmas and Damian, that Your holy sacrament will assure us of salvation.
$Per Dominum

[Super populum]
!Prayer over the people
v. Let us pray.
v. Bow your heads to God.
v. May Your heavenly favor, O God, increase the number of Your subjects, and help them always to obey Your commands.
$Per Dominum
