[Rank]
Feria Quinta in Coena Domini;;Feria privilegiata Duplex I classis;;7

[Rule]
Gloria
Credo
Prefatio=Crucis
Communicantes
Post Missam

[Introitus]
!Gal 6:14.
v. But it behooves us to glory in the cross of Our Lord Jesus Christ: in Whom is our salvation, life, and resurrection; by whom we are saved and delivered.
!Ps. 66. 2
May God have mercy on us, and bless us: may He cause the light of His countenance to shine upon us; and may He have mercy on us.
v. But it behooves us to glory in the cross of Our Lord Jesus Christ: in Whom is our salvation, life, and resurrection; by whom we are saved and delivered.

[Oratio]
 O God, from whom Judas received the punishment of his guilt, and the thief the reward of his confession: grant unto us the full fruit of Thy clemency; that even as in His Passion, our Lord Jesus Christ gave to each a retribution according to his merits, so having taken away our old sins, He may bestow upon us the grace of His Resurrection. Who with Thee liveth and reigneth in the unity of the Holy Ghost, God, world without end.
$Qui tecum

[Lectio]
Olvasm�ny szent P�l apostol Korintusiakhoz irott els� level�b�l
!1 Cor 11:20-32.
v. Amikor ugyanis egybegy�lt�k, m�r nem az �r vacsor�j�t eszitek,  hiszen �tkez�skor ki-ki a saj�t vacsor�j�t veszi el�, hogy elfogyassza, s az egyik �hen marad, a m�sik pedig d�zs�l.  Nincs otthonotok ev�sre-iv�sra? Vagy megvetitek Isten egyh�z�t, �s megsz�gyen�titek a szeg�nyeket? Mit mondjak nektek? Dics�rjelek? Ez�rt nem dics�rlek benneteket.  �n ugyanis az �rt�l kaptam, amit k�z�ltem is veletek: Urunk J�zus el�rul�s�nak �jszak�j�n fogta a kenyeret,  h�l�t adott, megt�rte, �s �gy sz�lt: "Vegy�tek �s egy�tek, ez az �n testem �rtetek. Ezt tegy�tek az �n eml�kezetemre."  Ugyan�gy vacsora ut�n fogta a kelyhet, �s �gy sz�lt: "Ez a kehely az �j sz�vets�g az �n v�remben. Ezt tegy�tek, valah�nyszor isztok bel�le, az �n eml�kezetemre."  Valah�nyszor ugyanis e kenyeret eszitek, �s e kehelyb�l isztok, az �r hal�l�t hirdetitek, am�g el nem j�n.  Ez�rt aki m�ltatlanul eszi a kenyeret vagy issza az �r kelyh�t, az �r teste �s v�re ellen v�t.  Teh�t vizsg�lja meg mag�t mindenki, s csak �gy egy�k a keny�rb�l �s igy�k a kehelyb�l,  mert aki csak eszik �s iszik an�lk�l, hogy megk�l�nb�ztetn� az �r test�t, saj�t �t�let�t eszi �s issza.  Ez�rt sokan gy�ng�k �s betegek k�z�letek, t�bben pedig meghaltak.  Ha meg�t�ln�nk magunkat, nem vonn�nk magunkra �t�letet.  Ha azonban az �r �t�l meg benn�nket, az feny�t�s�nkre szolg�l, hogy ezzel a vil�ggal egy�tt el ne k�rhozzunk. 

[Graduale]
!Phil 2:8-9
Christ became obedient for us unto death, even to the death of the cross. 
V. For which cause God also exalted Him and hath given Him a Name which is above all names.

[Evangelium]
Evang�lium szent J�nos Apostol k�nyv�b�l
!Joann 13:1-15
v. H�sv�t �nnepe el�tt t�rt�nt: J�zus tudta, hogy el�rkezett az �ra, amikor a vil�gb�l vissza kell t�rnie az Aty�hoz, mivel szerette �v�it, akik a vil�gban maradtak, mindv�gig szerette.  Vacsora k�zben t�rt�nt, amikor a s�t�n m�r f�l�bresztette az �rul� J�d�snak, Simon fi�nak sz�v�ben a gondolatot, hogy �rulja el.  J�zus tudta, hogy az Atya mindent a kez�be adott, s hogy Istent�l j�tt �s Istenhez t�r vissza.  M�gis f�lkelt a vacsora mell�l, levetette fels�ruh�j�t, fogott egy v�szonkend�t, �s maga el� k�t�tte.  Azt�n vizet �nt�tt egy mosd�t�lba, majd hozz�fogott, hogy sorra megmossa, s a derek�ra k�t�tt kend�vel megt�r�lje tan�tv�nyainak a l�b�t.  Amikor Simon P�terhez �rt, az tiltakozott: "Uram, te akarod megmosni az �n l�bamat?!"  J�zus �gy v�laszolt: "Most m�g nem �rted, amit teszek, de k�s�bb majd meg�rted."  P�ter tov�bb tiltakozott: "Az �n l�bamat ugyan meg nem mosod soha!" "Ha nem moslak meg - felelte J�zus -, nem lehetsz k�z�ss�gben velem."  Akkor, Uram, ne csak a l�bamat, hanem a fejemet �s a kezemet is!" - mondta Simon P�ter.  De J�zus ezt felelte: "Aki megf�rd�tt, annak csak a l�b�t kell megmosni, s akkor eg�szen tiszta lesz. Ti tiszt�k vagytok, de nem mindny�jan."  Tudta, hogy ki �rulja el, az�rt mondta: "Nem vagytok mindny�jan tiszt�k."  Amikor megmosta l�bukat, f�lvette fels�ruh�j�t, �jra asztalhoz �lt �s �gy sz�lt hozz�juk: "Tudj�tok, mit tettem veletek?  Ti Mesternek �s �rnak h�vtok, s j�l teszitek, mert az vagyok.  Ha teh�t �n, az �r �s Mester megmostam l�batokat, nektek is meg kell mosnotok egym�s l�b�t.  P�ld�t adtam, hogy amit �n tettem, ti is tegy�tek meg. 

[Offertorium]
!Ps 117:16 et 17.
The right hand of the Lord hath wrought strength: the right hand of the Lord hath exalted me. I shall not die, but live, and shall declare the works of the Lord.

[Secreta]
We beseech Thee, O holy Lord, Father almighty, everlasting God, that He Himself may render our Sacrifice acceptable to Thee, Who, by the tradition of today, taught His disciples to do this in remembrance of Him, Jesus Christ, Thy Son, our Lord,
$Qui tecum

[Communicantes]
Communicating and celebrating the most sacred day in which our Lord Jesus Christ was betrayed for us: and also honoring in the first place the memory of the glorious and ever Virgin Mary Genetr�cis ej�sdem Dei et D�mini nostri Jesu Christi: sed and of the blessed Apostles and Martyrs Peter and Paul, Andrew, James, John, Thomas, James, Philip, Bartholomew, Matthew, Simon, and Thaddeus; Linus, Cletus, Clement, Xystus, Cornelius, Cyprian, Lawrence, Chrysogonus, John and Paul, Cosmas and Damian, and of all Thy Saints, through whose merits and prayers, grant that we may in all things be defended by the help of Thy protection. (He joins his hands.) Through the same Christ our Lord. Amen. 	
_
_
! Tenens manus expansas super Oblata, dicit: 
 We therefore beseech Thee, O Lord, graciously to accept this offering of our service, and that of Thy whole family, which we make to Thee in memory of the day on which our Lord Jesus Christ gave to His disciples the Mysteries of His Body and Blood to be celebrated; and to dispose our day in Thy peace preserve us from eternal damnation, and rank us in the number of Thine Elect. (He joins his hands.) Through the same Christ our Lord. Amen. 
_
_
v. Which oblation do Thou, O God, vouchsafe in all respects, (He signs thrice the oblation with the Sign of the Cross.) to bless, + approve, + ratify, + make worthy and acceptable; (He signs again the Host and chalice with the Sign of the Cross.) that it may be made for us the Body + and Blood + of Thy most beloved Son Jesus Christ our Lord. 
_
_
Who, the day before He suffered for our salvation and that of all men, that is, on this day, took bread into His most sacred and venerable hands (He raises his eyes to heaven.) and with His eyes lifted up towards heaven unto Thee, God, His almighty Father, giving thanks to Thee, (He signs the host with the Sign of the Cross) He blessed + it, broke it and gave it to His disciples saying: Take and eat ye all of this, 

[Communio]
!Joann 13:12, 13 et
 The Lord Jesus, after He had supped with His disciples, washed their feet, and said to them: Know you what I, your Lord and Master, have done to you? I gave you an example, that you also may do likewise.

[Postcommunio]
 Strengthened with life-giving Food, we beseech Thee, O Lord, our God, that what we do in our mortal life may bring us to the reward of life immortal with Thee. 
$Per Dominum

[Post Missam]
!!Pange Lingua
v. Sing, my tongue, the Savior's glory:
Of His Flesh the mystery sing;
Of His Blood all price exceeding.
Shed by our immortal King.
Destined for the world's redemption
From a noble womb to spring. 
_  
v. Of a pure and spotless Virgin,
Born for us on earth below,
He, as Man with man conversing,
Stayed the seeds of truth to sow,
Then He closed in solemn order
Wondrously His life of woe. 	
_
On the night of His last supper,
Seated with His chosen band,
He, the paschal victim eating,
First fulfills the Law's command;
Then as food to all His brethren
Gives himself with His own hand. 	
_
v. Word made Flesh, the bread of nature,
By His words to Flesh He turns;
Wine into His Blood He changes:
What though sense no change discerns,
Only be the heart in earnest,
Faith her lesson quickly learns. 	
_
v. Down in adoration falling,
Lo, the Sacred Host we hail,
Lo, o'er ancient forms departing
Newer rites of grace prevail;
Faith for all defects supplying,
Where the feeble senses fail. 
_
v. To the everlasting Father
And the Son who reigns on high
With the Holy Ghost proceeding
Forth from each eternally,
Be salvation, honor, blessing,
Might and endless majesty.
Amen.
_
_
!!Stripping the altar
!  When Vespers are not recited, the celebrant proceeds to strip the altars, reciting alternately the following psalm:
_
_
!Antiphone
!Ps 21:19
Ant. They parted my garments amongst them, and upon my vesture they cast lots.
v. My God, my God, look upon me: why hast Thou forsaken me? Far from my salvation are the words of my sins.
O my God, I shall cry by day and Thou wilt not hear: and by night, and it shall not be reputed as folly in me.
But Thou dwellest in holiness, O Thou Praise of Israel.
In Thee our fathers have hoped: they have hoped, and Thou hast delivered them.
They cried unto Thee, and were delivered: they trusted in Thee, and were not confounded.  	
But I am a worm and no man: a reproach of men, and the outcast of the people.
All they that saw me have laughed me to scorn: they have spoken with the lips, and wagged their head.
He trusted the Lord, let Him rescue him: let Him deliver him, seeing He delighteth in him. 
For Thou art He that hast drawn em out of the womb: my hope from the breasts of my mother. I was cast upon Thee from the womb.
From my mother's womb thou art my God: depart not from me.
For tribulation is very near; for there is none to help me.
Many calves have surrounded me: fat bulls have besieged me.  	
They gaped upon me with their mouths, as a ravening and a roaring lion.
I am poured out like water, and all my bones are scattered.
My heart is like melting wax in the midst of my bowels.
My strength is dried up like a potsherd, and my tongue cleaveth to my jaws: and Thou hast brought me into the dust of death.  	
For many dogs have compassed me: the council of the malignant have beseiged me.
They pierced my hands and my feet: they have told all my bones.  	
They look and stare upon me. They part my garments among them, and upon my vesture do they cast lots.
But Thou, O Lord, remove not Thy help to a distance from me: look towards my defense.
 O God, deliver my soul from the sword: my darling from the power of the dog!
 Save me from the lion's mouth: and my affliction from the horns of the unicorns.
 I will declare Thy Name unto my brethren: in the midst of the church will I praise Thee.
Ye that fear the Lord, praise Him: all ye seed of Jacob, glorify Him.
 Let all the seed of Israel fear Him. Because He hath not slighted nor despised the supplication of the poor man.  	
 Neither hath He hid His face from me: but when I cried unto Him, He heard me.
 With Thee is my praise in the great church: I will pay my vows in the sight of them that fear Him.  	
The poor shall eat and be filled, and they shall praise the Lord that seek Him: their hearts shall live for ever and ever.
All the ends of the earth shall remember and turn unto the Lord.  	
And all the kindreds of the Gentiles shall adore in His sight.  	
For the kingdom is the Lord's: and He shall have dominion over the nations.
 All the fat ones of the earth have eaten and have adored: all they that go down to the earth shall fall before Him.
 My soul also shall live unto Him: and my seed shall serve Him.
 The generation to come shall tell it unto the Lord: and the heavens shall declare His righteousness unto a people that shall be born, whom the Lord hath made.
Ant. They parted my garments amongst them, and upon my vesture they cast lots.
My God, my God, look upon me: why hast Thou forsaken me? Far from my salvation are the words of my sins.
_
_
!!Washing of the feet
!  At a convenient hour, after the unclothing of the altars, a signal is given with the clapper, and the clergy assemble for the Maundy or the Washing of the feet. The prelate or superior wears a purple stole and cope over his amice and alb, and with the deacon and subdeacon vested in white dalmatics, comes to the appointed place. The celebrant puts incense into the thurible, served by the deacon. Then the deacon, holding the Gospel book before his breast, kneels before the celebrant and asks a blessing; having received it, while two acolytes stand by with lighted candles, and the subdeacon holds the book, he makes the Sign of the Cross over teh book and incenses it and sings, in the usual way, the Gospel of the Mass: Before the festival-day, as above. After this the subdeacon takes the Gospel book open to the officiating priest to kiss, and the deacon incenses him as usual. Then the celebrant removes his cope, girds himself with a cloth, and assisted by his ministers, begins the washing of the feet of twelve clerks or twelve poor people chosen for the ceremony. While the subdeacon holds the right foot of each of those whose feet are to be washed, the celebrant, kneeling before him, washes the foot, wipes and kisses it, the deacon handing him a towel for the wiping. Meanwhile the following chants are sung by the choir.
_
_
!Antiphona
!Ps 13:34.
v. A new commandment I give unto you: That you love one another, as I have loved you, saith the Lord.
!Ps 118:1
Blessed are the undefiled in the way: who walk in the law of the Lord.
v. A new commandment I give unto you: That you love one another, as I have loved you, saith the Lord.

_
_
!Antiphona
!Ps 13:4, 5, 15.
v. After our Lord was risen from supper, He put water into a basin, and began to wash the feet of His disciples: to whom He gave this example. 
!Ps. 47:2
Great is the Lord, and exceedingly to be praised in the city of our God, in His holy mountain.
v. After our Lord was risen from supper, He put water into a basin, and began to wash the feet of His disciples: to whom He gave this example. 
_
_
!Antiphona
!John 13:12, 13 et 15.
v. Our Lord Jesus, after He had supped with His disciples, washed their feet, and said to them: Know you what I your Lord and Master have done to you? I have given you an example, that ye also may do likewise.
!Ps. 84:2
Thou hast blessed, O Lord, Thy land; Thou hast turned away the captivity of Jacob.
v. Our Lord Jesus, after He had supped with His disciples, washed their feet, and said to them: Know you what I your Lord and Master have done to you? I have given you an example, that ye also may do likewise.
_
_
!Antiphona
!Ps 13:6-7, 8
v. Lord, dost Thou wash my feet? Jesus answered and said to them: If I shall not wash thy feet, thou shalt have no part with Me. 
V. He came to Simon Peter, and Peter said to Him: Lord, dost Thou wash my feet? Jesus answered and said to them: If I shall not wash thy feet, thou shalt have no part with Me. 
V. What I do, thou knowest not now; but thou shalt know hereafter.
v. Lord, dost Thou wash my feet? Jesus answered and said to them: If I shall not wash thy feet, thou shalt have no part with Me. 
_
_
!Antiphona.
v. If I your Lord and Master, have washed your feet, how much more ought you to wash one another's feet? 
!Ps. 48:2
Hear these things, all ye nations: give ear, ye that inhabit the world.
v. If I your Lord and Master, have washed your feet, how much more ought you to wash one another's feet? 
_
_
!Antiphona
!Ps 13:35.
v.  By this shall all men know that you are My disciples, if you have love one for another. 
V. Said Jesus to His disciples.
v.  By this shall all men know that you are My disciples, if you have love one for another. 
_
_
!Antiphona
!Ps 13:13
v. Let these three, faith, hope, and charity, remain in you; but the greatest of these is charity. 
V. And now there remain faith, hope and charity, these three; but the greatest of these is charity.
v. Let these three, faith, hope, and charity, remain in you; but the greatest of these is charity. 
_
_
!Antiphona.
v. Blessed be the Holy Trinity and undivided Unity: We will give praise to Him, for unto us He hath shown His mercy.
V. Let us bless the Father, and the Son, and the Holy Ghost.
!Ps. 83: 2-3.
How lovely are Thy tabernacles O lord of Hosts: my soul longeth and fainteth for the courts of the Lord.
v. Blessed be the Holy Trinity and undivided Unity: We will give praise to Him, for unto us He hath shown His mercy.
_
_
!Antiphona.
!1 Joann. 2; 3; 4.
v.  Where charity and love are, there is God. 
The love of Christ has gathered us together. 
Let us rejoice in Him and be glad. 
Let us fear and love the living God. 
And let us love one another with a sincere heart. 
v. Where charity and love are, there is God. 
When, therefore, we are assembled together. 
Let us take heed, that we be not divided in mind. 
Let malicious quarrels and contentions cease. 
And let Christ our God dwell among us. 
v. Where charity and love are, there is God. 
Let us also with the blessed see. 
Thy face in glory, O Christ our God. 
There to possess immeasurable and happy joy. 
For infinite ages of ages. Amen.
_
_
!   After the Maundy, the officiating priest washes his hands. Then returning to the place whence he came he puts on maniple and chasuble and standing with his head uncovered, he says:
_
$Pater noster
V. Thou hast commanded Thy commandments, O Lord. 	
R. To be exactly observed. 	
V. Thou hast washed the feet of Thy disciples. 	
R. Despise not the work of Thy hands. 	
V. O Lord, hear my prayer. 	
R. And let my cry come unto Thee. 	
V. The Lord be with you. 	
R. And with thy spirit. 	
v. Let us pray
Be present, O Lord, we beseech Thee, at the performance of our service: and since Thou didst vouchsafe to wash the feet of Thy disciples, despise not the work of Thy hands, which Thou hast commanded us to retain: that as here the outward stains are washed away by us and from us, so the inward sins of us all may be blotted out by Thee. Which do Thou vouchsafe to grant, who livest and reignest God for ever and ever. 
R. Amen. 	
