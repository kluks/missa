[Rank]
Feria Sexta infra Hebdomadam II in Quadragesima;;Feria major;;2

[Rule]
no Gloria
Suffragium=Sanctorum;Vivis;;
Super popul

[Introitus]
!Ps 16:15
v. But I in justice shall behold Your face; I shall be content when Your glory shall appear.
!Ps 16:1
Hear, O Lord, a just suit; attend to my outcry.
&Gloria
v. But I in justice shall behold Your face; I shall be content when Your glory shall appear.

[Oratio]
Grant, we You, almighty God, that with the sacred fast to purify us, we may with sincere hearts reach the coming feast.
$Per Dominum

[Lectio]
Olvasm�ny M�zes els� k�nyv�b�l
!Gen 37:6-22
v. �gy sz�lt hozz�juk: "Hallgassatok ide, milyen �lmot l�ttam.  K�v�t k�t�tt�nk a mez�n, s �me, az �n k�v�m f�legyenesedett �s �llva maradt, a ti k�v�itek pedig k�r�lvett�k �s meghajoltak az �n k�v�m el�tt."  Testv�rei �gy v�laszoltak: "No, tal�n kir�lyunk akarsz lenni �s f�l�tt�nk uralkodni?" �lma �s elbesz�l�se miatt m�g ink�bb meggy�l�lt�k.  M�s alkalommal �jra �lmot l�tott, s azt is elmondta b�tyjainak. �gy sz�lt: "L�tj�tok, ism�t �lmom volt. A nap, a hold �s a csillagok meghajoltak el�ttem."  Amikor elbesz�lte apj�nak �s testv�reinek, apja megfeddte, mondv�n: "Mit akar jelenteni ez az �lom, amit l�tt�l? Nekem, any�dnak �s b�ty�idnak, nek�nk a f�ldig kell hajolnunk el�tted?"  B�tyjai f�lt�kenyek lettek r�, apja ellenben megjegyezte mag�nak a dolgot.  Amikor b�tyjai elmentek, �s apjuk juhait Szichemben legeltett�k,  Izrael ezt mondta J�zsefnek: "B�ty�id Szichemn�l legeltetnek, gyere, hadd k�ldjelek el hozz�juk." "K�szen vagyok" - felelte.  Akkor ezt mondta neki: "Menj, �s n�zz ut�na, hogy b�ty�id �s a ny�jak j�l vannak-e, s hozz nekem h�rt r�luk." �gy elk�ldte Hebron v�lgy�b�l, s � Szichembe �rkezett.  Amikor a mez�n bolyongott, egy ember tal�lkozott vele, s megk�rdezte: "Mit keresel?"  "B�ty�imat keresem - felelte -, mondd meg, hol legeltetnek?"  A f�rfi �gy v�laszolt: "Elmentek inn�t, hallottam, amikor mondt�k: menj�nk Dotainba." J�zsef teh�t elment testv�rei ut�n, �s Dotainban megtal�lta �ket.  Amikor azok messzir�l megl�tt�k, m�g miel�tt k�zel�kbe �rt, azt ind�tv�nyozt�k, hogy �lj�k meg.  �gy sz�ltak egym�shoz: "N�zz�tek, ott j�n az �loml�t�.  Rajta, �lj�k meg, dobjuk egy cisztern�ba �s mondjuk azt, hogy vad�llat ette meg. Akkor majd megl�tjuk, mi lesz az �lmaib�l."  Mikor Ruben ezt meghallotta, igyekezett kimenteni kez�kb�l, �s �gy sz�lt: "Ne vegy�k el az �let�t."  Ruben m�g azt mondta nekik: "Ne ontsatok v�rt, dobj�tok be a cisztern�ba, ott a puszt�ban, de kezeteket ne emelj�tek r�." Ezt mondta, mert ki akarta menteni kez�kb�l �s vissza akarta k�ldeni apj�hoz. 

[Graduale]
!Ps 119:1-2
In my distress I called to the Lord, and He answered me.
V. O Lord, deliver me from lying lip, from treacherous tongue.
_
!Tractus
!Ps 102:10
O Lord, deal with us not according to our sins, nor requite us according to our crimes.
V. Ps 78: 8-9. O Lord, remember not against us the iniquities of the past; may Your compassion quickly come to us, for we are brought very low. (kneel)
V. Help us, O God, our Savior, because of the glory of Your Name, O Lord; deliver us and pardon our sins for Your Name�s sake.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt. 21:33-46
v. "Halljatok egy m�sik p�ldabesz�det. Volt egy gazda, aki sz�l�t telep�tett. Beker�tette s�v�nnyel, bel�l pedig sajt�t �sott �s �rtornyot �p�tett. Azt�n b�rbe adta a sz�l�t m�veseknek, �s elment idegenbe.  Amikor elj�tt a sz�ret ideje, elk�ldte szolg�it a sz�l�m�vesekhez, hogy szedj�k be a term�st.  �m a sz�l�m�vesek nekiestek a szolg�knak. Az egyiket megvert�k, a m�sikat meg�lt�k, a harmadikat megk�vezt�k.  Erre m�s szolg�kat k�ld�tt, t�bbet, mint el�sz�r, de ezekkel is �gy b�ntak.  V�g�l a fi�t k�ldte el hozz�juk, mert azt gondolta: A fiamat csak megbecs�lik.  De amikor a sz�l�m�vesek megl�tt�k a fi�t, �gy biztatt�k egym�st: Itt az �r�k�s! Gyertek, �lj�k meg, �s mienk lesz �r�ks�ge.  Nekiestek, kidobt�k a sz�l�b�l �s meg�lt�k.  Amikor majd megj�n a sz�l� ura, vajon mit tesz a sz�l�m�vesekkel?"  "A gonoszokat a gonoszok sors�ra juttatja, sz�lej�t pedig m�s b�rl�knek adja ki b�rbe, akik idej�ben beszolg�ltatj�k a term�st."  Erre J�zus azt k�rdezte: "Sose olvast�tok az �r�sban: A k�, amelyet az �p�t�k elvetettek, szegletk�v� lett. Az �r tette azz�, s ez csod�latos a szem�nkben.  Ez�rt mondom nektek, hogy elveszik t�letek az Isten orsz�g�t, s olyan n�p kapja meg, amely megtermi gy�m�lcs�t.  Aki erre a k�re esik, �sszez�zza mag�t, s akire r�zuhan, azt sz�tmorzsolja."  E hasonlatokat hallva a farizeusok �s az �r�stud�k meg�rtett�k, hogy r�luk besz�lt.  El akart�k fogni, de f�ltek a n�pt�l, mert pr�f�t�nak tartott�k. 

[Offertorium]
!Ps 39:14-15
Deign, O Lord, to rescue me; let all be put to shame and confusion who seek to snatch away my life. Deign, O Lord, to rescue me.

[Secreta]
O God, may this sacrifice which is offered, stay in us and accomplish lasting effects.
$Per Dominum

[Communio]
!Ps 11:8
You, O Lord, will keep us and preserve us always from this generation.

[Postcommunio]
Grant, we beseech You, O Lord, that we who have received the pledge of everlasting salvation, may properly direct our course so that we may be able to attain our goal.
$Per Dominum

[Super populum]
!Prayer over the people
v. Let us pray.
v. Bow your heads to God.
v. Grant unto Your people, we beseech You, O Lord, health of mind and body, that, by persevering in good works, we may be worthy to be protected by Your mighty power.
$Per Dominum
