[Rank]
Feria Tertia infra Hebdomadam Passionis;;Feria major;;2

[Rule]
no Gloria
Suffragium=Ecclesiae,Papa;;
Prefatio=Quad5
Super popul

[Introitus]
!Ps 26:14
v. Wait for the Lord with courage; be stouthearted, and wait for the Lord.
!Ps 26:1
The Lord is my light and my salvation; whom should I fear?
v. Wait for the Lord with courage; be stouthearted, and wait for the Lord.

[Oratio]
May our fasting be acceptable to You, we beseech You, O Lord; may it be atonement for our sins, make us worthy of Your grace, and lead us to the everlasting remedy of the life to come.
$Per Dominum

[Lectio]
Olvasm�ny D�niel pr�f�ta k�nyv�b�l
!Dan 14:27-42
v. Ekkor D�niel szurkot, h�jat �s sz�rt vett, �sszef�zte �ket, pog�cs�kat csin�lt bel�l�k �s beadogatta �ket a k�gy� sz�j�ba. A k�gy� megette �s sz�tpukkadt. Erre azt mondta: "N�zz�tek, mit im�dtatok!"  Amikor a b�beliek ezt meghallott�k, nagy felh�borod�s t�madt k�zt�k. �sszecs�d�ltek a kir�ly ellen, �s azzal v�dolt�k: "A kir�ly zsid�v� lett. B�lt �sszet�rette, a k�gy�t meg�lte, a papokat meggyilkolta."  Elmentek a kir�lyhoz �s k�vetelt�k: "Add ki nek�nk D�nielt, mert k�l�nben t�ged �l�nk meg h�zad n�p�vel egy�tt."  A kir�ly l�tta, hogy hevesen t�madj�k, k�nyszerhelyzet�ben kiadta h�t nekik D�nielt.  Erre az oroszl�nok barlangj�ba vetett�k hat napig ott volt.  A barlangban h�t oroszl�n volt. Mindennap k�t tetemet �s k�t b�r�nyt szoktak nekik adni. Most azonban �heztett�k �ket, hogy felfalj�k D�nielt.  Habakuk pr�f�ta ebben az id�ben �lt J�de�ban. �ppen megf�zte az �telt, a kenyeret beleapr�totta a t�lba �s elindult a mez�re, hogy kivigye az arat�knak.  Ekkor az �r angyala felsz�l�totta Habakukot: "Vidd el ezt az eb�det B�belbe, D�nielnek, az oroszl�nok barlangj�ba!"  Habakuk azt felelte: "Uram, B�belt sose l�ttam, az oroszl�nok barlangj�r�l meg sejtelmem sincs."  Erre az �r angyala megragadta az �st�k�n�l, s haj�n�l fogva elvitte lelk�nek erej�vel B�belbe �s letette a barlang mell�.  Habakuk leki�ltott: "D�niel, D�niel, itt az eb�d, amelyet az Isten k�ld�tt neked."  D�niel erre felfoh�szkodott: "Megeml�kezt�l r�lam, Istenem, mert te nem hagyod el azokat, akik szeretnek."  Ezut�n D�niel f�lkelt �s megeb�delt. Habakukot meg az �r angyala azon nyomban visszavitte lak�hely�re.  A hetedik napon a kir�ly elment, hogy meggy�szolja D�nielt. Amikor a barlanghoz �rt, ben�zett, s l�m, D�niel ott �ld�g�lt.  Hangosan felki�ltott: "Nagy vagy, Uram, D�niel Istene, �s rajtad k�v�l nincsen m�s!"  Kih�zatta D�nielt �s azokat dobatta a barlangba, akik el akart�k puszt�tani, �s ott a szeme l�tt�ra felfalt�k �ket az oroszl�nok. 

[Graduale]
!Ps 42:1, 3.
Fight my fight, O Lord; from the deceitful and impious man rescue me.
V. Send forth Your light and Your fidelity; they shall lead me on and bring me to Your holy mountain.

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 7:1-13
v. J�zus ezut�n bej�rja Galile�t, J�de�ba nem akart menni, mert a zsid�k az �let�re t�rtek.  K�zeledett a zsid�k �nnepe, a s�toros �nnep.  Ez�rt testv�rei �gy sz�ltak hozz�: "Kelj �tra, �s menj el J�de�ba, hogy tan�tv�nyaid is l�ss�k tetteidet.  Hisz senki sem m�k�dik titokban, ha azt akarja, hogy tudom�st szerezzenek r�la. Ha k�pes vagy ilyenekre, akkor l�pj a vil�g sz�ne el�."  Testv�rei sem hittek ugyanis benne.  J�zus ezt v�laszolta nekik: "Nekem m�g nem �rkezett el az id�m, de nektek mindig megfelel� az id�.  Titeket nem gy�l�lhet a vil�g, de engem gy�l�l, mert bizony�tom, hogy gonoszak a tetteik.  Menjetek h�t f�l az �nnepre, �n azonban nem megyek f�l erre az �nnepre, mert m�g nem telt be az id�m."  Ezt mondta nekik, s ott maradt Galile�ban.  De azut�n, hogy testv�rei f�lmentek az �nnepre, �maga is f�lment, csak nem nyilv�nosan, hanem titokban.  A zsid�k kerest�k az �nnepen. "Hol van?" - k�rdezgett�k,  s a n�p k�r�ben sokat emlegett�k. N�melyek azt mondt�k, hogy j� ember, m�sok meg azt, hogy f�lrevezeti a n�pet.  De a zsid�kt�l val� f�lelm�ben senki sem besz�lt ny�ltan r�la. 

[Offertorium]
!Ps 9:11-13
They trust in You who cherish your Name, O Lord, for You forsake not those who seek You. Sing praise to the Lord enthroned in Sion, for He has not forgotten the cry of the afflicted.

[Secreta]
O Lord, we bring You the gifts for sacrifice, as tokens of consolation in this life, so that we may not despair of Your eternal promises.
$Per Dominum

[Communio]
!Ps 24:22
Redeem me, O God of Israel, from all my distress.

[Postcommunio]
Grant us, we beseech You, almighty God that by constantly performing these holy acts, we may be worthy to come closer to the gifts of heaven.
$Per Dominum

[Super populum]
!Prayer over the people
v. Let us pray.
v. Bow your heads to God.
v. O Lord, we beseech You, make us always obedient servants of Your will, so that in our days Your faithful people may increase both in merit and in number.
$Per Dominum
