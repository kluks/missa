[Name]
Sancta Maria Sabbato ex Purificatione ad Paschae; Salve Sancta parens

[Rule]
Gloria
Suffr=Spiritu;;Ecclesiae,Papa
Prefatio=Maria=Feast;

[Introitus]
!  
v. Hail, holy Mother, who in childbirth brought forth the King Who rules heaven~
and earth world without end.
!Ps 44:2
My heart overflows with a goodly theme; as I sing my ode to the King.
&Gloria
v. Hail, holy Mother, who in childbirth brought forth the King Who rules heaven~
and earth world without end.

[Oratio]
Grant us, Your servants, O Lord God, we beseech You, to enjoy lasting health of~
mind and body; and by the intercession of glorious and blessed Mary, ever virgin,~
may we be delivered from present sorrow and partake to the full of eternal~
happiness.
$Per Dominum

[Lectio]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Sir 24:14-16
v. Kezdett�l �s az id�k el�tt teremtettem, �s mind az �r�kk�val�s�gig meg nem sz�n�k; �s a szent lakhelyen szolg�ltam el�tte. �s �gy Sionban meger�s�ttettem, �s a megszentelt v�rosban hasonl�k�pen megnyugodtam, �s Jerusalemben az �n hatalmam. �s meggy�kereztem a tiszteletre m�lt� n�p k�z�tt, �s az �n Istenem r�sz�ben, mely az � �r�ks�ge; �s a szentek teljes gy�lekezet�ben az �n tart�zkod�som.

[Graduale]
!
Blessed and venerable are you, O Virgin Mary, who, with unsullied virginity,~
were found to be the Mother of the Savior.
V. O Virgin, Mother of God, He Whom the whole world does not contain, becoming~
man, shut Himself in your womb. Alleluia, alleluia.
!Num 17:8
The rod of Jesse has blossomed: a Virgin has brought forth God and man: God has~
given peace, reconciling in Himself the lowest with the highest. Alleluia.

[Tractus]
!
Rejoice, O Virgin Mary; alone you have put an end to all heresies.
V. You who believed the words of the Archangel Gabriel.
V. Still a virgin, you brought forth God and man; and after childbirth you remained an inviolate virgin.
V. O Mother of God, intercede for us.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 11:27-28
v. M�g besz�lt, amikor egy asszony a t�megb�l felki�ltott: "Boldog a m�h, amely~
kihordott, �s az eml�, amelyet szopt�l!" De � ezt mondta: "H�t m�g azok milyen~
boldogok, akik hallgatj�k az Isten szav�t, �s meg is tartj�k!"

[Offertorium]
!
You are happy, O holy Virgin Mary, and most worthy of all praise; since out of~
you has risen the sun of justice, Christ our God.

[Secreta]

Through Your mercy, O Lord, and by the intercession of blessed Mary, ever virgin,~
the Mother of Your only-begotten Son, may this offering profit us for prosperity~
and peace, now and forevermore.
$Per Dominum

[Communio]
!
Blessed is the womb of the Virgin Mary, which bore the Son of the eternal~
Father.

[Postcommunio]
Having received the aids conducive to our salvation, O Lord, we beseech You,~
grant that we may everywhere be protected by the patronage of blessed Mary, ever~
virgin, in veneration of whom we have made these offerings to Your Majesty.
$Per Dominum
