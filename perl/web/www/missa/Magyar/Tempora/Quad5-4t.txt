[Rank]
Feria Quinta infra Hebdomadam Passionis;;Feria major;;2

[Rule]
no Gloria
Suffragium=Ecclesiae,Papa;;
Prefatio=Quad5
Super popul

[Introitus]
!Dan 3:31
v. All that You have done to us, O Lord, You have done in true judgment: because we have sinned against You, and have not obeyed Your commandments: but give glory to Your Name, and deal with us according to the multitude of Your mercy.
!Ps 118:1
Happy are they whose way is blameless, who walk in the law of the Lord.
v. All that You have done to us, O Lord, You have done in true judgment: because we have sinned against You, and have not obeyed Your commandments: but give glory to Your Name, and deal with us according to the multitude of Your mercy.

[Oratio]
Grant, we beseech You, almighty God, that the dignity of human nature, weakened by excessive self-indulgence, may be restored by the earnest practice of healing self-denial.
$Per Dominum

[Lectio]
Olvasm�ny D�niel pr�f�ta k�nyv�b�l
!Dan 3:25; 3:34-45.
v. Erre �gy folytatta: "�n n�gy f�rfi�t l�tok, akik szabadon j�rnak-kelnek a l�ngok k�zepette an�lk�l, hogy valami b�nt�d�suk volna. A negyediknek pedig olyan alakja van, mint egynek az istenek fiai k�z�l." 

[Graduale]
!Ps 95:8-9
Bring gifts and enter His courts; worship the Lord in His holy court.
V. Ps 28:9 The Lord strips the forests, and in His temple all say, Glory!

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 7:36-50
v. Egy farizeus megh�vta, hogy egy�k n�la. Bet�rt h�t a farizeus h�z�ba, �s asztalhoz telepedett.  �lt a v�rosban egy b�n�s n�. Amikor megtudta, hogy a farizeus h�z�ban van vend�gs�gben, alab�strom ed�nyben illatos olajat hozott.  Meg�llt h�tul a l�b�n�l, �s s�rva fakadt. K�nnyeit J�zus l�b�ra hullatta, majd haj�val megt�r�lte, el�rasztotta cs�kjaival, �s megkente illatos olajjal.  Mikor ezt a farizeus h�zigazda l�tta, �gy sz�lt mag�ban: "Ha pr�f�ta volna, tudn�, hogy ki �s mif�le az, aki �rinti: hogy b�n�s n�."  J�zus akkor hozz� fordult: "Simon, mondan�k neked valamit." Az k�rte: "Mester! H�t mondd el!"  "Egy hitelez�nek k�t ad�sa volt. Az egyik �tsz�z d�n�rral tartozott neki, a m�sik �tvennel.  Nem volt mib�l fizetni�k, h�t elengedte mind a kett�nek. Melyik�k szereti most jobban?"  "�gy gondolom az, akinek t�bbet elengedett" - felelte Simon. "Helyesen felelt�l" - mondta neki.  Majd az asszony fel� fordulva �gy sz�lt Simonhoz: "L�tod ezt az asszonyt? Bet�rtem h�zadba, s nem adt�l vizet a l�bamra. Ez a k�nnyeivel �ztatta l�bamat, �s a haj�val t�r�lte meg.  Cs�kot sem adt�l nekem, ez meg egyfolyt�ban cs�kolgatja a l�bam, ami�ta csak bej�tt.  Azt�n a fejemet sem kented meg olajjal. Ez meg a l�bamat keni illatos olaj�val.  Azt mondom h�t neked, sok b�ne bocs�natot nyer, mert nagyon szeretett. Akinek kev�s b�n�t bocs�tj�k meg, az csak kev�ss� szeret."  Azt�n �gy sz�lt az asszonyhoz: "B�neid bocs�natot nyernek."  A vend�gek �sszes�gtak: "Ki ez, hogy m�g a b�n�ket is megbocs�tja?"  De � ism�t az asszonyhoz fordult: "A hited megmentett. Menj b�k�vel!" 

[Offertorium]
!Ps 136:1
By the streams of Babylon we sat and wept when we remembered Sion.

[Secreta]
O Lord, our God, Who have especially commanded that these created things, which You have fashioned for the support of our weakness, should also be used as offerings dedicated to Your Name, grant we beseech You, that they may provide for us help in the present life and may be a pledge of life eternal.
$Per Dominum

[Communio]
!Ps 118:49-50
Remember Your word to Your servant, O Lord, since You have given me hope. This is my comfort in my affliction.

[Postcommunio]
May we receive with pure mind, O Lord, what we have taken by mouth, and as a gift in time, may it become for us a remedy for eternity.
$Per Dominum

[Super populum]
!Prayer over the people
v. Let us pray.
v. Bow your heads to God.
v. Be merciful to Your people, we beseech You, O Lord, that as they reject whatever displeases You, they may the more delight in Your commandments.
$Per Dominum
