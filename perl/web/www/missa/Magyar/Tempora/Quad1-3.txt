[Rank]
Feria Quarta Quatuor Temporum;;Feria major;;2

[Rank1960]
Feria Quarta Quatuor Temporum;;Feria major;;3

[RankNewcal]
Feria IV. infra Hebdomadam I in Quadragesima;;Feria major;;2

[Rule]
no Gloria
Suffragium=Sanctorum;Vivis;;
Super popul
LectioL1

[Introitus]
!Ps 24:6, 3, 22.
v. Remember that Your compassion, O Lord, and Your kindness are from of old; let not our enemies exult over us; deliver us, O God of Israel, from all our tribulations.
!Ps 24:1-2
To You I lift up my soul, O Lord; in You, O my God, I trust; let me not be put to shame.
&Gloria
v. Remember that Your compassion, O Lord, and Your kindness are from of old; let not our enemies exult over us; deliver us, O God of Israel, from all our tribulations.

[Oratio]
Arise. Mercifully hear our prayers, we beseech You, O Lord; and stretch forth the right hand of Your Majesty against all things hostile to us.
$Per Dominum

[LectioL1]
!Exod 24:12-18
v. Az �r �gy sz�lt M�zeshez: "J�jj fel hozz�m a hegyre, �s maradj itt. �tadom neked a k�t�bl�kat a t�rv�nnyel �s a parancsokkal, amelyeket oktat�sukra �rtam."  M�zes elindult szolg�j�val, J�zsu�val, �s f�lment Isten hegy�re.  A v�neknek ezt mondta: "V�rjatok meg itt, am�g visszaj�v�nk. �ron �s Hur veletek vannak. Ha valakinek peres �gye van, forduljon hozz�juk."  Azut�n M�zes f�lment a hegyre. A felh� betakarta a hegyet,  �s az �r dics�s�ge leereszkedett a S�nai-hegyre. A felh� hat napig takarta be. A hetedik napon a felh�b�l sz�lt M�zeshez.  Az izraelit�k szem�ben Isten dics�s�ge olyannak mutatkozott, mint az em�szt� t�z a hegy cs�cs�n.  M�zes bel�pett a felh�be �s f�lment a hegyre. M�zes negyven nap �s negyven �jjel maradt a hegyen. 

[GradualeL1]
!Ps 24:17-18
Relieve the troubles of my heart, and bring me out of distress, O Lord.
V. Put an end to my affliction and my suffering, and take away all my sins.

[OratioL1]
We beseech You, O Lord, look graciously upon the fervor of Your people, who mortify themselves in the flesh through abstinence: that they may be refreshed in spirit by the fruit of these good works.
$Per Dominum

[Lectio]
Olvasm�ny a Kir�lyok els� k�nyv�b�l
!1 Kgs 19:3-8
v. Ill�s megrettent, �tra kelt �s elment, hogy megmentse �let�t. Amikor a J�d�hoz tartoz� Beerseb�ba �rt, otthagyta szolg�j�t,  maga pedig beh�z�dott egynapi j�r�snyira a puszt�ba. Amikor oda�rt, le�lt egy bor�kabokor al� �s a hal�l�t k�v�nta. Azt mondta: "Most m�r el�g, Uram! Vedd magadhoz lelkemet! �n sem vagyok k�l�nb aty�imn�l."  Ezzel lefek�dt �s elaludt. Egyszer csak angyal �rintette meg, �s �gy sz�lt hozz�: "Kelj f�l �s egy�l!"  Ahogy odapillantott, l�m, a fej�n�l egy s�lt cip� meg egy kors� v�z volt.  Evett is, ivott is, de azt�n �jra lefek�dt aludni. �m az �r angyala m�sodszor is megjelent, meg�rintette �s azt mondta: "Kelj f�l �s egy�l! K�l�nben t�l hossz� lesz neked az �t."  F�lkelt, evett, ivott, azt�n negyven nap �s negyven �jjel v�ndorolt ennek az �telnek az erej�b�l, eg�szen az Isten hegy�ig, a H�rebig. 

[Graduale]
!Ps 24:17-18, 1-4.
Bring me out of distress, O Lord; put an end to my affliction and my suffering, and take away all my sins.
V. To You, I lift up my soul, O Lord. In You, O my God, I trust; let me not be put to shame, let not my enemies exult over me.
V. No one who waits for You shall be put to shame; those shall be put to shame who heedlessly break faith.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt:12:38-50
At that time, certain of the Scribes and Pharisees answered Him, saying, Master, we would see a sign from You. But He answered and said to them, An evil and adulterous generation demands a sign, and no sign shall be given it but the sign of Jonah the prophet. For even as Jonah was in the belly of the fish three days and three nights, so will the Son of Man be three days and three nights in the heart of the earth. The men of Ninive will rise up in the judgment with this generation and will condemn it; for they repented at the preaching of Jonah, and behold, a greater than Jonah is here. The queen of the South will rise up in the judgment with this generation and will condemn it; for she came from the ends of the earth to hear the wisdom of Solomon, and behold, a greater than Solomon is here. But when the unclean spirit has gone out of a man, he roams through dry places in search of rest, and finds none. Then he says, �I will return to my house which I left�; and when he has come to it, he finds the place unoccupied, swept and decorated. Then he goes and takes with him seven other spirits more evil than himself, and they enter in and dwell there; and the last state of that man becomes worse than the first. So shall it be with this evil generation also. While He was still speaking to the crowds, His mother and His brethren were standing outside, seeking to speak to Him. And someone said to Him, Behold, Your mother and Your brethren are standing outside, seeking You. But He answered and said to him who told Him, Who is My mother and who are My brethren? And stretching forth His hand toward His disciples, He said, Behold My mother and My brethren! For whoever does the will of My Father in heaven, he is My brother and sister and mother.

[Offertorium]
!Ps 118:47-48
I will delight in Your commands, which I love exceedingly. And I will life up my hands to Your commands which I love.

[Secreta]
We offer You, O Lord, this sacrifice of atonement, that You would mercifully absolve our sins and direct our faltering hearts.
$Per Dominum

[Communio]
!Ps 5:2-4
Attend to my sighing, heed my call for help, my King and my God! To You, I pray, O Lord.

[Postcommunio]
By receiving Your sacrament, O Lord, may we be cleansed of out secret sins and delivered from the snares of our enemies.
$Per Dominum

[Super populum]
!Prayer over the people
v. Let us pray.
v. Bow your heads to God.
v. O Lord, we beseech You, shed light upon our minds by the brightness of Your glory, so that we may see what we must do and have strength to do what is right.
$Per Dominum
