[Rank]
Dominica I in Quadragesima;;I classis Semiduplex;;6

[Rule]
no Gloria
Credo
Prefatio=Quadragesima
Suffragium=Sanctorum;Vivis;;

[Introitus]
!Ps 90:15-16
v. He shall call upon Me, and I will answer him; I will deliver him and glorify him; with length of days I will gratify him.
!Ps 90:1
You who dwell in the shelter of the Most High, shall abide in the shadow of the Almighty.
&Gloria
v. He shall call upon Me, and I will answer him; I will deliver him and glorify him; with length of days I will gratify him.

[Oratio]
O God, You Who purify Your Church by the yearly Lenten observance, grant to Your household that what they strive to obtain from You by abstinence, they may achieve by good works.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol Korintusiakhoz irott m�sodik level�b�l
!2 Cor. 6:1-10
v. Az � munkat�rsak�nt figyelmeztet�nk titeket is: ne vegy�tek hi�ba az Isten kegyelm�t.  Azt mondja ugyanis: A kell� id�ben meghallgatlak, s az �dv�ss�g napj�n seg�tek rajtad. Nos, most van a kell� id�, most van az �dv�ss�g napja!  Senkinek sem okozunk semmif�le botr�nyt, hogy ne becsm�relhess�k szolg�latunkat.  Ehelyett mindenben �gy viselked�nk, mint Isten szolg�ja: Nagy t�relmet tan�s�tva a megpr�b�ltat�sban, a sz�ks�gben, a szorongattat�sban;  ha vernek, fogs�gba vetnek, fell�zadnak, ha elf�radunk, virrasztunk, b�jt�l�nk;  tisztas�gban, meg�rt�n, b�ket�r�n �s j�s�gosan, a Szentl�lekben �s az �szinte szeretetben megmaradva;  az igazs�g hirdet�s�ben kitartva, az Isten erej�vel, az igazs�g t�mad� �s v�d� fegyverzet�ben,  ak�r dics�s�g, ak�r megsz�gyen�l�s, ak�r gyal�zat, ak�r j�h�rn�v jut oszt�lyr�sz�l; mint csal�k, m�gis igazat mond�k,  mint ismeretlenek, m�gis j�l ismertek, mint akiket hal�lra sz�ntak, m�gis �lnek, mint megk�nzottak, de bel� nem haltak;  mint szomor�ak, m�gis mindig vid�mak, mint sz�k�lk�d�k, m�gis sokakat gazdag�t�k, mint akiknek semmij�k sincs, m�gis minden�k megvan. 

[Graduale]
!Ps 90:11-12
To His angels God has given command about you, that they guard you in all your ways.
V. Upon their hands they shall bear you up, lest you dash your foot against a stone.
_
!Tractus
!Ps 90:1-7, 11-16.
You who dwell in the shelter of the Most High, shall abide in the shadow of the Almighty.
V. Say to the Lord, My refuge and my fortress, my God, in Whom I trust.
V. For He will rescue you from the snare of the fowler, from the destroying pestilence.
V. With His pinions He will cover you, and under His wings you shall take refuge.
V. His faithfulness is a buckler and a shield; you shall not fear the terror of the night.
V. Nor the arrow that flies by day; nor the pestilence that roams in darkness; nor the devastating plague at noon.
V. Though a thousand fall at your side, ten thousand at your right side, near you it shall not come.
V. For to His angels He has given command about you, that they may guard you in all your ways.
V. Upon their hands they shall bear you up, lest you dash your foot against a stone.
V. You shall tread upon the asp and the viper; you shall trample down the lion and the dragon.
V. Because he clings to Me, I will deliver him; I will set him on high because he acknowledges My name.
V. He shall call upon Me, and I will answer him; I will be with him in distress.
V. I will deliver him and glorify him; with length of days I will gratify him and will show him My salvation.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt. 4:1-11
v. Akkor a L�lek a puszt�ba vitte J�zust, hogy a s�t�n megk�s�rtse.  Negyven nap �s negyven �jjel b�jt�lt, v�g�l meg�hezett.  Odal�pett hozz� a k�s�rt�, �s �gy sz�lt: "Ha Isten Fia vagy, mondd, hogy ezek a k�vek v�ltozzanak keny�rr�."  Azt felelte: "Meg van �rva: Nemcsak keny�rrel �l az ember, hanem minden tan�t�ssal is, amely az Isten sz�j�b�l sz�rmazik."  Most a szent v�rosba vitte a s�t�n, �s a templom p�rk�ny�ra �ll�totta.  "Ha Isten Fia vagy - mondta -, vesd le magad, hiszen �rva van: Parancsot adott angyalainak, a kez�k�n hordoznak majd, nehogy k�be �sd a l�bad."  J�zus �gy v�laszolt: "Az is meg van �rva: Ne k�s�rtsd Uradat, Istenedet."  V�g�l egy igen magas hegyre vitte a s�t�n, s felvonultatta szeme el�tt a vil�g minden orsz�g�t �s dics�s�g�ket.  "Ezt mind neked adom - mondta -, ha leborulva im�dsz engem."  J�zus elutas�totta: "T�vozz s�t�n! Meg van �rva: Uradat, Istenedet im�dd, s csak neki szolg�lj!"  Erre otthagyta a s�t�n �s angyalok j�ttek a szolg�lat�ra. 

[Offertorium]
!Ps 90:4-5
With His pinions the Lord will cover you, and under His wings you shall take refuge; His faithfulness is a buckler and a shield.

[Secreta]
We offer these sacrificial gifts at the beginning of Lent, praying You, O Lord, that while we practice restraint in the use of bodily food, we may also refrain from harmful pleasures.
$Per Dominum

[Communio]
!Ps 90:4-5
With His pinions the Lord will cover you, and under His wings you shall take refuge; His faithfulness is a buckler and a shield.

[Postcommunio]
May the holy offering of Your sacrament renew us, O Lord, and cause us to be purified from our old ways and come to the fellowship of this saving mystery.
$Per Dominum
