[Rank]
Feria Sexta Quattuor Temporum Septembris;;Feria major;;2

[Rank1960]
Feria Sexta Quattuor Temporum Septembris;;Feria major;;4

[Rule]

[Introitus]
!Ps 104:3-4
v. Rejoice, O hearts that seek the Lord! Look to the Lord, and be strengthened; seek His face evermore.
!Ps 104:1
Give thanks to the Lord, invoke His name; make known among the nations His deeds.
&Gloria
v. Rejoice, O hearts that seek the Lord! Look to the Lord, and be strengthened; seek His face evermore.

[Oratio]
Grant, we beseech You, almighty God, that we who devoutly keep the sacred observances year by year may be pleasing unto You both in body and soul.
$Per Dominum

[Lectio]
Olvasm�ny Oze�s pr�f�ta k�nyv�b�l
!Hos 14:2-10
v. Izrael, t�rj vissza az �rhoz, a te Istenedhez, hisz b�n�d miatt bukt�l el!  Hozzatok szavakat magatokkal, �s t�rjetek vissza az �rhoz! Mondj�tok neki: "V�gy el minden gonoszs�got, hogy elnyerj�k a j�t, �s ajkunk gy�m�lcs�t hozhassuk �ldozatul.  Assz�ria nem seg�t rajtunk, l�ra sem sz�llunk t�bb�. Nem mondjuk kez�nk alkot�s�nak ezut�n: Te vagy a mi Isten�nk! Mert az �rva csak n�lad tal�l irgalmat."  Meggy�gy�tom h�tlens�g�ket, s sz�vemb�l szeretni fogom �ket, elfordul t�l�k haragom.  Izraelhez olyan leszek, mint a harmat; virul majd, mint a liliom, gy�keret ereszt, mint a ny�rfa.  Hajt�sai messze �gaznak, pomp�s lesz, mint az olajfa, s illatos, mint a Libanon.  Visszat�rnek, hogy �rny�komban lakjanak, b�z�t termelnek, �s gondozz�k a sz�l�t, amelynek olyan h�re lesz, mint a helboni bornak.  Mi k�ze Efraimnak ezent�l a b�lv�nyokhoz? �n hallgatom meg, �n viselek r� gondot. Olyan leszek, mint a z�ldell� ciprus; t�lem sz�rmazik gy�m�lcs�d.  Aki b�lcs, �rtse meg ezeket! Aki �rtelmes, l�ssa be ezt mind! Mert egyenesek az �r �tjai, azokon j�rnak az igazak, de a gonoszok elbuknak rajtuk. 


[Graduale]
!Ps 89:13, 1.
Return, O Lord! How long? Have pity on Your servants.
V. O Lord, You have been our refute through all generations.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 7:36-50
v. Egy farizeus megh�vta, hogy egy�k n�la. Bet�rt h�t a farizeus h�z�ba, �s asztalhoz telepedett.  �lt a v�rosban egy b�n�s n�. Amikor megtudta, hogy a farizeus h�z�ban van vend�gs�gben, alab�strom ed�nyben illatos olajat hozott.  Meg�llt h�tul a l�b�n�l, �s s�rva fakadt. K�nnyeit J�zus l�b�ra hullatta, majd haj�val megt�r�lte, el�rasztotta cs�kjaival, �s megkente illatos olajjal.  Mikor ezt a farizeus h�zigazda l�tta, �gy sz�lt mag�ban: "Ha pr�f�ta volna, tudn�, hogy ki �s mif�le az, aki �rinti: hogy b�n�s n�."  J�zus akkor hozz� fordult: "Simon, mondan�k neked valamit." Az k�rte: "Mester! H�t mondd el!"  "Egy hitelez�nek k�t ad�sa volt. Az egyik �tsz�z d�n�rral tartozott neki, a m�sik �tvennel.  Nem volt mib�l fizetni�k, h�t elengedte mind a kett�nek. Melyik�k szereti most jobban?"  "�gy gondolom az, akinek t�bbet elengedett" - felelte Simon. "Helyesen felelt�l" - mondta neki.  Majd az asszony fel� fordulva �gy sz�lt Simonhoz: "L�tod ezt az asszonyt? Bet�rtem h�zadba, s nem adt�l vizet a l�bamra. Ez a k�nnyeivel �ztatta l�bamat, �s a haj�val t�r�lte meg.  Cs�kot sem adt�l nekem, ez meg egyfolyt�ban cs�kolgatja a l�bam, ami�ta csak bej�tt.  Azt�n a fejemet sem kented meg olajjal. Ez meg a l�bamat keni illatos olaj�val.  Azt mondom h�t neked, sok b�ne bocs�natot nyer, mert nagyon szeretett. Akinek kev�s b�n�t bocs�tj�k meg, az csak kev�ss� szeret."  Azt�n �gy sz�lt az asszonyhoz: "B�neid bocs�natot nyernek."  A vend�gek �sszes�gtak: "Ki ez, hogy m�g a b�n�ket is megbocs�tja?"  De � ism�t az asszonyhoz fordult: "A hited megmentett. Menj b�k�vel!" 

[Offertorium]
!Ps 102:2, 5.
Bless the Lord, O my soul, and forget not all His benefits; and your youth shall be renewed like the eagle�s.

[Secreta]
May the gift of our fasting, we beseech You, O Lord, be acceptable to You, and by its purifying power make us worthy of Your grace and bring us to the eternal bliss You have promised us.
$Per Dominum

[Communio]
!Ps 118:22, 24.
Take away from me reproach and contempt, for I observe Your decrees, O Lord. Your decrees are my delight.

[Postcommunio]
We beseech You, O almighty God, that, showing gratitude for the gifts we have received, we may obtain yet greater benefits.
$Per Dominum
