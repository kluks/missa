[Rank]
Feria Sexta infra Hebdomadam IV in Quadragesima;;Feria major;;2

[Rule]
no Gloria
Suffragium=Sanctorum;Vivis;;
Super popul

[Introitus]
!Ps 18:15
v. Let the thought of my heart find favor before You, O Lord, my Rock and my Redeemer.
!Ps 18:2
The heavens declare the glory of God, and the firmament proclaims His handiwork.
&Gloria
v. Let the thought of my heart find favor before You, O Lord, my Rock and my Redeemer.

[Oratio]
O God, Who by your wondrous sacraments renew the world, grant that Your church may benefit from Your eternal decrees and not be deprived of temporal help.
$Per Dominum

[Lectio]
Olvasm�ny a Kir�lyok els� k�nyv�b�l
!1 Kgs 17:17-24
v. Ezek ut�n az esem�nyek ut�n t�rt�nt, hogy az asszony fia, aki� a h�z volt, megbetegedett. Betegs�ge annyira elhatalmasodott rajta, hogy nem maradt benne �let.  Akkor �gy sz�lt Ill�shez az asszony: "Mi dolgom veled, Isten embere? Csak az�rt j�tt�l, hogy eml�kezetembe id�zd v�tkeimet �s meg�ld a fiamat?"  � azt felelte neki: "Add ide a fiadat!" Elvette �l�b�l �s f�lvitte a fenti szob�ba, ahol lakott �s lefektette �gy�ra.  Azt�n az �rhoz fordult: "Uram, �n Istenem, h�t csakugyan szerencs�tlens�gbe sodrod ezt az �zvegyet, akin�l lakom, s meg�l�d a fi�t?"  Majd h�romszor a fi�ra borult �s seg�ts�g�l h�vta az Urat: "Uram �s Istenem, engedd, hogy visszat�rjen a l�lek ebbe a fi�ba!"  S az �r meghallgatta Ill�s k�ny�rg�s�t, a l�lek visszat�rt a fi�ba, �gyhogy �jra �letre kelt.  Akkor Ill�s fogta a fi�t, levitte a fenti szob�b�l a h�zba, �tadta anyj�nak �s azt mondta: "N�zd, �l a fiad!"  Az asszony erre �gy sz�lt Ill�shez: "Most m�r tudom, hogy csakugyan Isten embere vagy, s hogy az �r szava igazs�g ajkadon." 

[Graduale]
!Ps 117:8-9
It is better to take refuge in the Lord rather than to trust in man.
V. It is better to take refuge in the Lord rather than to trust in princes.
_
!Tractus
!Ps 102:10
O Lord, deal with us not according to our sins, nor requite us according to our crimes.
!Ps 78:8-9 
V. O Lord, remember not against us the iniquities of the past; may Your compassion quickly come to us, for we are brought very low. (kneel)
V. Help us, O God, our Savior, because of the glory of Your Name, O Lord; deliver us and pardon our sins for Your Name�s sake.

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 11:1-45
v. Bet�ni�ban, M�ri�nak �s n�v�r�nek, M�rt�nak a faluj�ban volt egy beteg, L�z�r.  Ez a M�ria kente meg az Urat olajjal �s t�r�lte meg a l�b�t a haj�val. Az � testv�re, L�z�r volt a beteg.  A n�v�rek meg�zent�k neki: "Uram, akit szeretsz, beteg."  Ennek hallat�ra J�zus azt mondta: "Ez a betegs�g nem okozza hal�l�t, hanem Isten dics�s�g�re lesz, hogy megdics��lj�n �ltala az Isten Fia."  J�zus szerette M�rt�t, a n�v�r�t M�ri�t �s L�z�rt.  Amikor meghallotta, hogy beteg, k�t napig m�g ott maradt, ahol volt,  s akkor sz�lt a tan�tv�nyoknak: "Menj�nk vissza J�de�ba!"  "Mester - felelt�k a tan�tv�nyok -, most akartak ott megk�vezni a zsid�k �s �jra odam�sz?"  J�zus �gy v�laszolt: "Nem tizenk�t �r�ja van a napnak? Aki nappal j�r, nem botlik meg, mert l�tja a vil�g vil�goss�g�t.  Aki azonban �jszaka j�r, megbotlik, mert nincs vil�goss�ga."  Azt�n �gy folytatta: "Bar�tunk, L�z�r elaludt, de elmegyek �s f�l�bresztem."  "Uram, ha alszik, akkor meggy�gyul" - felelt�k a tan�tv�nyok.  J�zus L�z�r hal�l�r�l besz�lt, de �k azt hitt�k, hogy alv�s�r�l besz�lt.  Ez�rt J�zus vil�gosan megmondta nekik: "L�z�r meghalt.  Miattatok �r�l�k, hogy nem voltam ott, hogy higgyetek. De most menj�nk el hozz�!"  Tam�s, akit mell�knev�n Didimusznak h�vtak, �gy sz�lt a t�bbi tan�tv�nyhoz: "Menj�nk mi is, haljunk meg vele egy�tt!"  Amikor J�zus meg�rkezett, L�z�r m�r n�gy napja a s�rban volt.  Bet�nia Jeruzs�lem k�zel�ben fek�dt, mintegy tizen�t st�diumnyira.  Ez�rt a zsid�k k�z�l sokan elmentek M�rt�hoz �s M�ri�hoz, hogy testv�r�k miatt vigasztalj�k �ket.  Amikor M�rta meghallotta, hogy J�zus k�zeledik, el�je sietett, M�ria pedig otthon maradt.  "Uram - sz�l�totta meg M�rta J�zust -, ha itt lett�l volna, nem halt volna meg testv�rem.  De most is tudom, hogy b�rmit k�rsz az Istent�l, megadja neked."  J�zus megnyugtatta: "Felt�mad testv�red."  "Tudom, hogy felt�mad - mondta M�rta -, majd a felt�mad�skor, az utols� napon."  J�zus �gy folytatta: "�n vagyok a felt�mad�s �s az �let. Aki hisz bennem, m�g ha meghal is, �lni fog.  Az, aki �gy �l, hogy hisz bennem, nem hal meg �r�kre. Hiszed ezt?"  "Igen, Uram - felelte -, hiszem, hogy Te vagy a Messi�s, az Isten Fia, aki a vil�gba j�n."  E szavakkal elment �s h�vta n�v�r�t, M�ri�t. Halkan sz�lt neki: "Itt a Mester �s h�vat."  Ennek hallat�ra (M�ria) gyorsan f�lkelt �s odasietett hozz�.  Mert J�zus m�g nem �rt le a faluba, hanem ott volt, ahol M�ria tal�lkozott vele.  Amikor a zsid�k, akik ott maradtak vele a h�zban �s vigasztalt�k, l�tt�k, hogy M�ria gyorsan fel�ll �s elsiet, ut�namentek. Azt gondolt�k, hogy a s�rhoz megy s�rni.  Amikor M�ria oda�rt, ahol J�zus volt, �s megl�tta, e szavakkal borult a l�ba el�: "Uram, ha itt lett�l volna, nem halt volna meg testv�rem."  Amikor l�tta, hogy s�rnak, J�zus lelke m�ly�ig megrend�lt.  "Hova tett�tek?" - k�rdezte megindultan. "Gyere, Uram - felelt�k -, �s n�zd meg!"  J�zus k�nnyekre fakadt.  Erre a zsid�k megjegyezt�k: "N�zz�tek, mennyire szerette!"  N�melyek azonban �gy v�lekedtek: "Ha a vaknak vissza tudta adni a szeme vil�g�t, azt nem tudta volna megakad�lyozni, hogy ne haljon meg?"  J�zus sz�ve m�ly�ig megrend�lt, s odament a s�rhoz, amely egy k�vel eltorlaszolt barlang volt.  "Henger�ts�tek el a k�vet!" - sz�lt J�zus. De M�rta, az elhunyt n�v�re tiltakozott: "Uram, m�r szaga van, hiszen negyednapos."  J�zus �gy felelt: "Nemde azt mondtam: ha hiszel, megl�tod Isten dics�s�g�t?"  Erre elhenger�tett�k a k�vet, J�zus pedig az �gre emelte tekintet�t �s �gy im�dkozott: "Aty�m, h�l�t adok neked, hogy meghallgatt�l.  Tudom, hogy mindig meghallgatsz. Csak a k�r�ttem �ll� n�p miatt mondtam, hogy higgy�k: te k�ldt�l engem."  E szavak ut�n hangosan besz�lt: "L�z�r, j�jj ki!"  S a halott kij�tt. L�ba �s keze be volt p�ly�zva, az arc�t meg kend� f�dte. J�zus sz�lt nekik: "Oldj�tok fel, hogy tudjon j�rni."  A zsid�k k�z�l, akik f�lkerest�k M�ri�t, sokan l�tt�k, amit J�zus v�gbevitt, �s hittek benne. 

[Offertorium]
!Ps 17:28, 32.
Lowly people You save, O Lord, but haughty eyes You bring low; for who is God except You, O Lord?

[Secreta]
May the gifts we offer, O Lord, cleanse us, we beseech You, that You may be always merciful to us.
$Per Dominum

[Communio]
!Jn 11:33, 35, 43-44, 39.
The Lord, seeing the sisters of Lazarus weeping at the tomb, wept before the Jews, and cried out: Lazarus, come forth�: and he who had been dead four days came forth, bound hands and feet.

[Postcommunio]
May the reception of this sacrament, we beseech You, O Lord, always free us from sin and protect us from all that works against us.
$Per Dominum

[Super populum]
!Prayer over the people
v. Let us pray.
v. Bow your heads to God.
v. Grant us, we beseech You, almighty God, that, conscious of our weakness, we may place our trust in Your strength, and rejoice forever in Your fatherly love.
$Per Dominum
