[Rank]
Sabbato Quattuor Temporum Pentecostes;;Semiduplex I classis;;5;;ex Pasc7-0

[Rank]
Sabbato Quattuor Temporum Pentecostes;;Semiduplex I classis;;6;;ex Pasc7-0

[RankNewcal]
Sabbato post Pentecostes;;Feria;;1;;

[Rule]
Gloria
(rubrica 1960 et missa brevior) LectioL1
(rubrica 1960) celebranda aut forma longior aut forma brevior
LectioL5
Sequentia
Credo
Prefatio=Spiritu

[Introitus]
!Rom 5:5.
v. The charity of God is poured forth in our hearts, alleluia: by His Spirit~
dwelling in us, alleluia, alleluia.
!Ps 102:1
Bless the Lord, O my soul; and, all my being, bless His holy Name.
&Gloria
v. The charity of God is poured forth in our hearts, alleluia: by His Spirit~
dwelling in us, alleluia, alleluia.

[Oratio]
Graciously pour into our minds, we beseech You, O Lord, the Holy Spirit, Whose~
wisdom created us and Whose providence governs us.
$Per Dominum eiusdem

[LectioL1]
Olvasm�ny Joel pr�f�ta k�nyv�b�l
!Joel 2:28-32
�s leszen ezekut�n: Ki�nt�m az �n lelkemet minden testre, �s pr�f�t�lnak majd fiaitok �s le�nyaitok; a ti v�neitek �lmokat �lmodnak, �s ifjaitok l�tom�sokat l�tnak; so"t szolg�imra �s szolg�l�imra is ki�nt�m ama napokban az �n lelkemet. �s csod�kat teszek az �gen �s f�ld�n, v�r, tu"z �s f�stgo"z �ltal. A nap s�t�ts�gg� v�ltozik, �s a hold v�rszinu"v�, miel&#337;tt eljo" az �r nagy �s rettenetes napja. �s leszen: hogy mindaz, ki seg�ts�gu"l h�ja az �r nev�t, �dv�zu"l; mert Sion hegy�n �s Jerusalemben szabad�l�s leszen, mint az �r mondotta; �s a maradv�nyokban, kiket az �r h�vand.
$Deo gratias

[GradualeL1]
Alleluia.
!John 6:64
!It is the Spirit that gives life; but the flesh profits nothing.

[OratioL1]
May the Holy Spirit, we beseech You, O Lord, inflame us with that fire which our~
Lord Jesus Christ cast upon the earth and desired that it be fanned into flame.
$Qui tecum eiusdem

[LectioL2]
Olvasm�ny M�zes harmadik k�nyv�b�l
!Lev. 23:9-11; 23:15-17; 23:21.
v. Az �r �gy sz�lt M�zeshez: "Sz�lj Izrael fiaihoz �s k�z�ld vel�k: Amikor majd eljuttok arra a f�ldre, amelyet adok nektek, s amikor el�rkezik az arat�s, akkor arat�stok els� k�v�j�t vigy�tek a papnak. � felaj�nlja az �rnak a bemutat�s szertart�s�val, hogy kedvess� tegyen titeket. A pap a szombat ut�ni napon mutassa be az �ldozatot, A szombat ut�ni napt�l, amelyen bemutatt�tok az �ldozati k�v�ket, sz�moljatok h�t teljes hetet. Sz�moljatok �tven napot a hetedik szombat ut�ni napig, �s akkor mutass�tok be az �rnak az �j keny�r �ldozat�t. Hozzatok a lak�sotokb�l kenyeret az �rnak sz�nt �ldozathoz, k�t �j kenyeret, amelyet k�t tized efa kov�szos lisztl�ngb�l s�t�ttek. Ezen a napon gy�ljetek �ssze, legyen ez szent �sszej�vetel sz�motokra, �s ne v�gezzetek semmif�le szolgai munk�t. �r�k t�rv�ny ez ut�daitok sz�m�ra ott, ahol lakni fogtok.
$Deo Gratias

[GradualeL2]
Alleluia.
!Job 26:13
V. His Spirit has adorned the heavens.

[OratioL2]
O God, Who for the healing of our souls has commanded that our bodies should be~
discipline by devout fasting, mercifully grant us to be ever devoted to You both~
in mind and in body.
$Per Dominum

[LectioL3]
Olvasm�ny M�zes �t�dik k�nyv�b�l
!Deut. 26:1-3; 26:7-11
v. Ha arra a f�ldre �rkezel, amelyet az �r, a te Istened ad neked �r�ks�g�l, elfoglalod �s letelepedsz rajta, mindenb�l, amit azon a f�ld�n, amelyet az �r, a te Istened ad majd neked, betakar�tasz, vedd az els� term�s egy r�sz�t, tedd be egy kos�rba, s menj el arra a helyre, amelyet az �r, a te Istened neve kiv�laszt. Menj oda a paphoz, aki akkor �pp ell�tja a szolg�latot, s mondd neki: "Kijelentem ma az �rnak, az �n Istenemnek, hogy meg�rkeztem arra a f�ldre, amelyet az �r esk�vel �g�rt aty�inknak!" akkor az �rhoz, aty�ink Isten�hez ki�ltottunk, s az �r meghallgatta k�ny�rg�s�nket, megl�tta nyomor�s�gunkat, elnyomat�sunkat �s szorongatotts�gunkat. Az �r er�s k�zzel �s kiny�jtott karral kivezetett Egyiptomb�l, nagy r�m�letet t�masztva, jelek �s csod�k k�s�ret�ben. Erre a f�ldre hozott, �s nek�nk adta ezt a tejjel-m�zzel foly� orsz�got. Itt hozom annak a f�ldnek els� term�s�t, amelyet te, az �r, nekem adt�l." Ezzel hagyd ott a kosarat az �r, a te Istened sz�ne el�tt, majd borulj le az �r, a te Istened el�tt, �s �r�lj eg�sz csal�doddal, a levit�val �s a k�r�dben �l� idegennel egy�tt, mindannak a j�nak, amit az �r, a te Istened adott neked.
$Deo Gratias

[GradualeL3]
Alleluia.
!Act 2:1
V. When the days of Pentecost were accomplished, they were all sitting together.

[OratioL3]
Grant, we beseech You, almighty God, that we, disciplined by wholesome fasting~
and refraining from all wrong-doing, may the more readily obtain Your~
forgiveness. $Per Dominum

[LectioL4]
Olvasm�ny M�zes harmadik k�nyv�b�l
!Lev 26:3-12
v. Ha t�rv�nyeim szerint �ltek, ha megtartj�tok parancsaimat �s teljes�titek �ket, akkor a kell� id�ben es�t adok nektek, ahogyan sz�ks�getek van r�, a f�ld meghozza term�s�t, s a mez� f�ja a gy�m�lcs�t. Cs�pelni fogtok sz�retig, �s sz�retelni fogtok vet�sig. J�llak�sig eszitek kenyereteket, �s biztons�gban laktok f�ldeteken. B�k�t szerzek orsz�gotoknak, s alhattok, mert senki sem fenyeget. Kiirtom f�ldetekr�l az �rtalmas vadat, kard nem vesz�lyezteti orsz�gotokat. Megfutam�tj�tok ellens�geiteket, s elhullanak kardotok el�tt. �ten k�z�letek megfutam�tanak sz�zat, �s sz�zan megfutam�tanak t�zezret, s ellens�geitek elhullanak kardotok el�tt. Fel�tek fordulok, naggy� teszlek �s megsokas�tlak benneteket, s megtartom veletek k�t�tt sz�vets�gemet. Azut�n, hogy �ltek a r�gi arat�sb�l, m�g mindig ki kell hordanotok majd a r�gi gabon�t, hogy az �jnak helyet k�sz�tsetek. Lak�helyet v�lasztok k�ztetek, �s nem vetlek el benneteket. K�z�ttetek fogok �lni, Istenetek leszek, ti meg n�pem lesztek.
$Deo Gratias

[GradualeL4]
Alleluia.
V. (Kneel.) Come, O Holy Spirit, fill the hearts of Your faithful; and kindle in~
them the fire of Your love.

[OratioL4]
Grant, we beseech You, almighty God, that we may so fast from bodily food as to~
abstain also from the sins that beset us.
$Per Dominum

[LectioL5]
Olvasm�ny D�niel pr�f�ta k�nyv�b�l
!Dan 3:49-51
Az �r angyala pedig lesz�lla Azari�ssal �s t�rsaival a kemencz�be, �s a t&#369;z l�ngj�t k�tfel� �t� a kemencz�ben, �s olyann� tev� a kemencze k�zep�t, mint harmaton a szell&#337; lengedez�s�t; �s a t&#369;z teljess�ggel nem illet� &#337;ket, semmi f�jdalmokra �s semmi b�nt�sokra sem l&#337;n. Ekkor azok h�rman mind egy sz�jjal * dics�r�k, dics&#337;it�k �s �ld�k Istent a kemencz�ben, mondv�n:
$Deo Gratias

[GradualeL5]
Alleluia.
!Dan 3:52
Blessed are You, O Lord, the God of our fathers, and worthy to be praised~
forever.

[OratioL5]
O God, You Who tempered the flames of fire for the three young men, mercifully~
grant that the flames of sin may not burn us, Your servants.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol R�maiakhoz irott level�b�l
!Rom 5:1-5
v. Mivel teh�t a hit r�v�n megigazultunk, b�k�ben �l�nk az Istennel, Urunk, J�zus Krisztus �ltal. �ltala jutottunk hozz� a hitben a kegyelemhez, amelyben �l�nk, �s dicseksz�nk a rem�nys�ggel, hogy az isteni dics�s�g r�szesei lehet�nk. De nemcsak ezzel, hanem m�g szenved�seinkkel is dicseksz�nk, mert tudjuk, hogy a szenved�sb�l t�relem fakad, a t�relemb�l kipr�b�lt er�ny, a kipr�b�lt er�nyb�l rem�nys�g. A rem�ny pedig nem csal meg, mert a nek�nk aj�nd�kozott Szentl�lekkel ki�radt sz�v�nkbe az Isten szeretete.

[Graduale]
Ps 116:1-2 Praise the Lord, all you nations; glorify Him, all you peoples! For~
steadfast is His kindness toward us, and the fidelity of the Lord endures~
forever.

[Sequentia]
@Tempora/Pasc7-0:Sequentia

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 4:38-44
v. J�zus a zsinag�g�b�l kij�ve, Simon h�z�ba ment. Simon any�sa magas l�zban~
fek�dt. Sz�ltak neki miatta. F�l� hajolt, parancsolt a l�znak, �s az nyomban~
elhagyta. Mindj�rt fel is kelt �s kiszolg�lta. Napnyugta ut�n hozz�tartoz�ik~
elvitt�k hozz� a k�l�nf�le bajokban szenved� betegeket. Egyenk�nt r�juk tette~
kez�t, s meggy�gy�totta �ket. Sokakb�l gonosz l�lek ment ki, �gy ki�ltozva: "Te~
az Isten Fia vagy!" De � r�juk sz�lt, s nem engedte sz�hoz jutni �ket, mert hisz~
tudt�k, hogy � a Krisztus. Amikor megvirradt, kiment egy elhagyatott helyre. A~
n�p kereste, m�g meg nem tal�lta. Marasztalt�k, hogy ne hagyja ott �ket. De �~
azt felelte: "M�s v�rosokban is kell hirdetnem az Isten orsz�g�t, hiszen ez a~
k�ldet�sem." �s hirdette J�dea zsinag�g�iban.

[Offertorium]
!Ps 87:2-3
O Lord, the God of my salvation, by day I cry out, at night I clamor in Your~
presence. Let my prayer come before You, O Lord, alleluia.

[Secreta]
Grant us, we beseech You, O Lord, that we may offer You a heart cleansed by this~
sacred rite so that our fast may be acceptable to You.
$Per Dominum

[Communio]
!John 3:8.
The Spirit breaths where He will, and you hear His voice, alleluia, alleluia;~
but do not know whence He comes or where He goes, alleluia, alleluia, alleluia.

[Postcommunio]
May Your holy things, O Lord, fill us with divine eagerness to enjoy their~
celebration together with their effects.
$Per Dominum
