[Rank]
Die III infra octavam Paschae;;Duplex I classis;;7;;ex Pasc0-0

[Rule]
ex Pasc0-0;
Credo
Sequentia

[Introitus]
!Sir 15:3-4
v.  She give them the water of wholesome wisdom to drink, alleluia; and she shall be made strong in him, and he shall not be moved: And she shall hold him fast, and he shall not be confounded: and she shall exalt them, allel�ja, allel�ja.
!Ps 104:1
Give glory to the Lord, and call upon his name: declare his deeds among the Gentiles. 
&Gloria
v.  She give them the water of wholesome wisdom to drink, alleluia; and she shall be made strong in him, and he shall not be moved: And she shall hold him fast, and he shall not be confounded: and she shall exalt them, allel�ja, allel�ja.

[Oratio]
O God, Who art ever multiplying the Children of thy Church, grant unto the same~
thy servants that they may lead the rest of their lives according to this~
beginning wherein Thou hast given them faith to receive the Sacrament of the New~
Birth. 
$Per Dominum

[Lectio]
Olvasm�ny az Apostolok Cselekedeib�l
!Act 13:16; 13:26-33
v. Ekkor P�l sz�l�sra emelkedett, kez�vel csendre intett, �s besz�lni kezdett: "Izraelita f�rfiak �s ti istenf�l�k, figyeljetek!  Testv�rek! �brah�m nemzets�g�nek fiai �s ti istenf�l�k! Ennek az �dv�ss�gnek a h�re nek�nk sz�lt.  De Jeruzs�lem lak�i �s vezet�i nem ismert�k el, hanem �t�let�kkel beteljes�tett�k a pr�f�t�k szav�t, amelyet szombatonk�nt mindig felolvasnak.  B�r semmi hal�lt �rdeml� b�nt nem tudtak r�la, m�gis hal�l�t k�rt�k Pil�tust�l.  Miut�n �gy minden beteljesedett, amit r�la �rtak, levett�k a keresztf�r�l �s s�rba tett�k.  De Isten felt�masztotta a hal�lb�l,  s � t�bb napon �t megjelent azoknak, akik vele j�ttek fel Galile�b�l Jeruzs�lembe. Ezek most tan�skodnak mellette a n�p el�tt.  �gy mi azt az �g�retet hirdetj�k nektek, melyet az Isten az aty�knak tett,  amint azt � ut�daiknak teljes�tette, amikor felt�masztotta J�zust. Ez van a m�sodik zsolt�rban meg�rva: Fiam vagy, ma nemzettelek. 

[Graduale]
Alleluia, alleluia
!Ps 117:24;
v.  This is the day which the Lord hath made: let us be glad and rejoice therein. 
!Ps 106:2
V.  Let them say so that have been redeemed by the Lord, whom he hath redeemed from the hand of the enemy: and gathered out of the countries. Allel�ja, allel�ja.
V. The Lord is risen from the sepulcher, He, who hangeth on the tree

[Sequentia]
@Tempora/Pasc0-0:Sequentia

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 24:36-47
v. M�g ezekr�l besz�lgettek, egyszer csak megjelent k�zt�k �s k�sz�nt�tte �ket: "B�kess�g nektek!"  Ijedt�kben �s f�lelm�kben azt v�lt�k, hogy szellemet l�tnak.  De � �gy sz�lt hozz�juk: "Mi�rt ijedtetek meg, s mi�rt t�mad k�tely sz�vetekben?  N�zz�tek meg a kezem �s a l�bam! �n vagyok. Tapogassatok meg �s l�ssatok! A szellemnek nincs h�sa �s csontja, de mint l�tj�tok, nekem van."  Ezut�n megmutatta nekik kez�t �s l�b�t.  De �r�m�kben m�g mindig nem mertek hinni �s csod�lkoztak. Ez�rt �gy sz�lt hozz�juk: "Van itt valami ennival�tok?"  Adtak neki egy darab s�lt halat.  Fogta �s a szem�k l�tt�ra evett bel�le.  Azt�n �gy sz�lt hozz�juk: "Ezeket mondtam nektek, amikor m�g veletek voltam, hogy be kell teljesednie mindannak, amit r�lam M�zes t�rv�ny�ben, a pr�f�t�kban �s a zsolt�rokban �rtak."  Ekkor megnyitotta �rtelm�ket, hogy meg�rts�k az �r�sokat, s �gy folytatta:  "Meg van �rva, hogy a Messi�snak szenvednie kell, �s harmadnap fel kell t�madnia a hal�lb�l.  Nev�ben megt�r�st �s b�nbocs�natot kell hirdetni minden n�pnek Jeruzs�lemt�l kezdve. 

[Offertorium]
!Ps 17:14; 17:16
v.  And the Lord thundered from heaven, and the highest gave his voice:  Then the fountains of waters appeared, allel�ja.

[Secreta]
Accept, O Lord, the prayers and the sacrifice of thy faithful; that by these services of hearthy devotion, we may come to heavenly glory.
$Per Dominum

[Communio]
!Col 3:1-2
v.  If you be risen with Christ, seek the things that are above; where Christ is sitting at the right hand of God: alleluia. Mind the things that are above, allel�ja.

[Postcommunio]
Grant, we beseech thee, Almighty God, that the grace of paschal sacrament which we have received may ever abide in our souls.
$Per Dominum