[Rank]
Feria VI Quattuor temporum in Adventu;;Feria major;;2

[Rank1960]
Feria VI Quattuor temporum in Adventu;;Feria major;;3

[RankNewcal]
Feria VI infra Hebdomadam III Adventus;;Feria major;;2

[Rule]
Oratio Dominica
no Gloria
no Credo
Suffr=Maria1;Ecclesiae,Papa;;

[Introitus]
!Ps 118:151-152
v. You, O Lord, are near, and all Your ways are truth. Of old I know from Your~
decrees that You are forever.
!Ps 118:1
Happy are they whose way is blameless, who walk in the law of the Lord.
&Gloria
v. You, O Lord, are near, and all Your ways are truth. Of old I know from Your~
decrees that You are forever.

[Oratio]
Put forth Your power, O Lord, we beseech You, and come; that those who trust in~
Your goodness may soon be delivered from all harm.
$Qui vivis

[Lectio]
Olvasm�ny Izi�s pr�f�ta k�nyv�b�l
!Isa 11:1-5
v. Vessz� k�l majd Iz�j t�rzs�k�b�l, hajt�s sarjad gy�ker�b�l.  Az �r lelke nyugszik rajta: a b�lcsess�g �s az �rtelem lelke; a tan�cs �s az er�ss�g lelke; a tud�s �s az �r f�lelm�nek lelke,  s az �r f�lelm�ben telik �r�me. Nem aszerint �t�l majd, amit a szem l�t, s nem aszerint �t�lkezik, amit a f�l hall,  hanem igazs�got szolg�ltat az alacsony sor�aknak, �s m�lt�nyos �t�letet hoz a f�ld szeg�nyeinek. Sz�ja vesszej�vel megveri az er�szakost, s ajka lehelet�vel meg�li a gonoszt.  Az igazs�goss�g lesz derek�n az �v, s a h�s�g cs�p�j�n a k�t�. 

[Graduale]
!Ps 84:8, 2.
Show us, O Lord, Your kindness, and grant us Your salvation.
V. You have favored, O Lord, Your land; You have restored the well-being of~
Jacob.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 1:39-47
v. M�ria m�g ezekben a napokban �tnak indult, �s a hegyekbe sietett, J�da v�ros�ba.  Zakari�s h�z�ba t�rt be �s �dv�z�lte Erzs�betet.  Amikor Erzs�bet meghallotta M�ria k�sz�nt�s�t, �r�m�ben megmozdult m�h�ben a gyermek, maga Erzs�bet pedig eltelt Szentl�lekkel.  Nagy sz�val felki�ltott: "�ldott vagy az asszonyok k�z�tt, �s �ldott a te m�hed gy�m�lcse!  Hogy lehet az, hogy Uramnak anyja j�n hozz�m?  L�sd, mihelyt meghallottam k�sz�nt�sed szav�t, az �r�mt�l megmozdult m�hemben a gyermek.  Boldog, aki hitt annak a beteljesed�s�ben, amit az �r mondott neki!"  M�ria �gy sz�lt: "Lelkem magasztalja az Urat,  �s sz�vem ujjong megv�lt� Istenemben, 

[Offertorium]
!Ps 84: 7-8.
Will You not, O God, give us life; and shall not Your people rejoice in You?~
Show us, O Lord, Your kindness, and grant us Your salvation.

[Secreta]
Accept our offering and prayers, we beseech You, O Lord; cleanse us by this~
heavenly rite, and in Your mercy hear us.
$Per Dominum

[Communio]
!Zach 14:5-6
Behold, the Lord shall come, and all His holy ones with Him: and there shall be~
in that day a great light.

[Postcommunio]
May the devout reception of Your sacrament, O Lord, give us new strength, purge~
us of our old selves and let us come to share in the mystery of salvation.
$Per Dominum
