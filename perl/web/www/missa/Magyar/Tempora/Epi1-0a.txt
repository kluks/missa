[Rank]
Dominica infra Octavam Epiphaniae;;Semiduplex;;5;;ex Sancti/01-06

[Rule]
ex Sancti/01-06
Gloria
Credo
Prefatio=Epi

[Introitus]
!
v. Upon a high throne I saw a man sitting, Whom a multitude of angels adore,~
singing in unison: Behold Him, the name of Whose empire is forever.
!Ps 99:1
Sing joyfully to God, all you lands; serve the Lord with gladness.
&Gloria
v. Upon a high throne I saw a man sitting, Whom a multitude of angels adore,~
singing in unison: Behold Him, the name of Whose empire is forever.

[Oratio]
O Lord, we beseech You, in Your heavenly goodness, hear Your people who pray to~
You; that they may perceive what they ought to do, and have the strength to~
fulfill what they have seen.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol R�maiakhoz irott level�b�l
!Rom 12:1-5
v. Testv�rek, Isten irgalm�ra k�rlek benneteket: Adj�tok testeteket �l�, szent, Istennek tetsz� �ldozatul. Ez legyen szellemi h�dolatotok.  Ne hasonuljatok a vil�ghoz, hanem gondolkod�stokban meg�julva alakuljatok �t, hogy felismerj�tek, mi az Isten akarata, mi a helyes, mi a kedves el�tte �s mi a t�k�letes.  A nekem adott kegyelem seg�ts�g�vel azt mondom mindegyiteknek: Senki ne becs�lje mag�t a kellet�n�l t�bbre, hanem j�zanul gondolkodjatok, mindenki az Istent�l neki juttatott hit m�rt�ke szerint.  Mert ahogy egy testben t�bb tagunk van, s minden tagnak m�s a szerepe,  sokan egy test vagyunk Krisztusban, egyenk�nt azonban tagjai vagyunk egym�snak, 

[Graduale]
!Ps 71:18, 3.
Blessed be the Lord, the God of Israel, Who alone does wondrous deeds.
V. The mountains shall yield peace for the people, and the hills justice.~
Alleluia, alleluia.
!Ps 99:1
Sing joyfully to God, all you lands; serve the Lord with gladness. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 2:42-52
v. Amikor tizenk�t �ves lett, szint�n f�lmentek, az �nnepi szok�s szerint.  Az �nnepnapok eltelt�vel hazafel� indultak. A gyermek J�zus azonban Jeruzs�lemben maradt an�lk�l, hogy sz�lei tudt�k volna.  Abban a hitben, hogy az �ti t�rsas�gban van, mentek egy napig, �s kerest�k a rokonok �s ismer�s�k k�z�tt.  Amikor nem tal�lt�k, visszafordultak Jeruzs�lembe, hogy keress�k.  H�rom nap m�lva akadtak r� a templomban, ott �lt a tan�t�k k�zt, hallgatta �s k�rdezgette �ket.  Akik csak hallgatt�k, mind csod�lkoztak okoss�g�n �s feleletein.  Amikor megl�tt�k, csod�lkoztak. Anyja �gy sz�lt hozz�: "Gyermekem, mi�rt tetted ezt vel�nk? �me, aty�d �s �n szomor�an kerest�nk."  Ezt felelte: "De mi�rt kerestetek? Nem tudt�tok, hogy nekem Aty�m dolgaiban kell lennem?"  �m �k nem �rtett�k meg ezeket a hozz�juk int�zett szavakat.  Vel�k ment h�t, lement N�z�retbe, �s engedelmeskedett nekik. Szavait anyja mind meg�rizte sz�v�ben.  J�zus meg gyarapodott b�lcsess�gben, korban, s kedvess�gben Isten �s az emberek el�tt. 

[Offertorium]
!Ps 99:1-2
Sing joyfully to God, all you lands; serve the Lord with gladness; come before~
Him with joyful song. Know that the Lord is God.

[Secreta]
May this sacrifice that we offer You, O Lord, ever give us new life and~
protection.
$Per Dominum

[Communio]
!Luke 2:48-49
Son, why have You done so to us? In sorrow Your father and I have been seeking~
You.� How is it that you sought Me? Did you not know that I must be about My~
Father�s business?

[Postcommunio]
O Almighty God, grant, we humbly beseech You, that those whom You refresh with~
Your sacrament may also worthily serve You in a way that is well pleasing to~
You.
$Per Dominum
