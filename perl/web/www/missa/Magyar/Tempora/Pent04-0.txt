[Rank]
Dominica IV Post Pentecosten;;Semiduplex;;5

[Rule]
Gloria
Credo
Prefatio=Trinitate
Suffr=Sanctorum;Maria3,Ecclesiae,Papa;;

[Introitus]
!Ps. 26:1-2.
v. The Lord is my light and my salvation; whom should I fear? The Lord is my~
life�s refuge; of whom should I be afraid? My enemies that trouble me,~
themselves stumble and fall.
!Ps 26:3
Though an army encamp against me, my heart will not fear.
&Gloria
v. The Lord is my light and my salvation; whom should I fear? The Lord is my~
life�s refuge; of whom should I be afraid? My enemies that trouble me,~
themselves stumble and fall.

[Oratio]
Grant us, we beseech You, O Lord, that the course of the world may be directed~
according to Your rule in peace and that Your Church may have the joy of serving~
You undisturbed.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol R�maiakhoz irott level�b�l
!Rom. 8:18-23.
v. De ennek az �letnek a szenved�sei v�lem�nyem szerint nem m�rhet�k az elj�vend� dics�s�ghez, amely majd megnyilv�nul rajtunk.  Maga a term�szet s�v�rogva v�rja Isten fiainak megnyilv�nul�s�t.  A term�szet ugyanis muland�s�gnak van al�vetve, nem mert akarja, hanem amiatt, aki abban a rem�nyben vetette al�,  hogy a muland�s�g szolgai �llapot�b�l majd felszabadul az Isten fiainak dics�s�ges szabads�g�ra.  Tudjuk ugyanis, hogy az eg�sz term�szet egy�tt s�hajtozik �s vaj�dik mindm�ig.  De nemcsak az, hanem mi magunk is, akik bens�nkben hordozzuk a L�lek zseng�j�t, s�hajtozunk, �s v�rjuk a fogadott fi�s�got, test�nk megv�lt�s�t. 

[Graduale]
!Ps. 78:9-10.
Pardon our sins, O Lord; why should the nations say, Where is their God?
V. Help us, O God our Savior; because of the glory of Your name, O Lord, deliver~
us. Alleluia, alleluia. Ps. 9:5; 9:10 O God, seated on Your throne, judging~
justly: be a stronghold for the oppressed in times of distress. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 5:1-11.
v. Amikor egyszer a Genez�ret tav�n�l �llt, nagy t�meg sereglett oda hozz�, hogy hallgassa az Isten szav�t.  L�tta, hogy a t� partj�n k�t b�rka vesztegel. A hal�szok kisz�lltak, �s a h�l�t most�k.  Besz�llt h�t az egyik b�rk�ba, Simon�ba, s megk�rte, hogy l�kje egy kicsit beljebb a partt�l. Azt�n le�lt, �s a b�rk�b�l tan�totta a n�pet.  Amikor befejezte a tan�t�st, �gy sz�lt Simonhoz: "Evezz a m�lyre, �s vess�tek ki a h�l�tokat halfog�sra!"  "Mester, eg�sz �jszaka f�radoztunk, s nem fogtunk semmit, de a te szavadra, kivetem a h�l�t" - v�laszolta Simon -,  �s �gy is tett, s annyi halat fogtak, hogy szakadozni kezdett a h�l�.  Intettek a m�sik b�rk�ban lev� t�rsaiknak, hogy menjenek seg�teni. Mentek is, �s �gy telerakt�k mind a k�t b�rk�t, hogy majdnem els�llyedt.  Ennek l�tt�n Simon P�ter J�zus l�b�hoz borult, �s e szavakra fakadt: "Uram, menj el t�lem, mert b�n�s ember vagyok!"  Mert a szerencs�s halfog�s l�tt�n t�rsaival egy�tt f�lelem t�lt�tte el.  Hasonl�k�ppen Jakabot �s J�nost is, Zebedeus fiait, Simon t�rsait. De J�zus �gy sz�lt Simonhoz: "Ne f�lj! Ezent�l emberhal�sz leszel."  Kivont�k a haj�kat a partra, �s minden�ket elhagyva k�vett�k. 

[Offertorium]
!Ps. 12:4-5.
Give light to my eyes that I may never sleep in death, lest my enemy say, I have~
overcome him.

[Secreta]
Be appeased, we beseech You, O Lord, by accepting our offerings, and in Your~
kindness make even our rebellious wills turn to You.
$Per Dominum

[Communio]
!Ps. 17:3
O Lord, my rock, my fortress, my deliverer: my God, my rock of refuge!

[Postcommunio]
May the sacrament we have received cleanse us, we beseech You, O Lord, and by~
its grace protect us.
$Per Dominum
