[Name]
Commune Doctore; In medio

[Rule]
Gloria

[Introitus]
!Sir 15:5
v.  And in the midst of the church she shall open his mouth, and shall fill him with the spirit of wisdom and understanding, and shall clothe him with a robe of glory. 
!Ps 91:2 
v.  It is good to give praise to the Lord: and to sing to thy name, O most High.
&Gloria
v.  And in the midst of the church she shall open his mouth, and shall fill him with the spirit of wisdom and understanding, and shall clothe him with a robe of glory. 

[Oratio]
O God, who didst give blessed N. to be thy people minister in eternal~
salvation grant we pray, that that we who have him for teacher of life here on~
earth, may also deserve now that he is in heaven, to have him for an advocate
$Per Dominum.

[Lectio]
Olvasm�ny szent P�l apostol Tim�teushoz irott m�sodik level�b�l
!2 Tim 4:1-8
v. K�rve-k�rlek az Istenre �s Krisztus J�zusra, aki �t�lkezni fog �l�k �s holtak f�l�tt, az � elj�vetel�re �s orsz�g�ra:  hirdesd az evang�liumot, �llj vele el�, ak�r alkalmas, ak�r alkalmatlan. �rvelj, ints, buzd�ts nagy t�relemmel �s hozz��rt�ssel.  Mert j�n id�, amikor az eg�szs�ges tan�t�st nem hallgatj�k sz�vesen, hanem saj�t �zl�s�k szerint szereznek maguknak tan�t�kat, hogy f�l�ket csiklandoztass�k.  Az igazs�got nem hallgatj�k meg, de a mes�ket elfogadj�k.  Te azonban maradj mindenben meggondolt, viseld el a bajokat, teljes�tsd az evang�lium hirdet�j�nek feladat�t, t�ltsd be szolg�latodat.  Az �n v�remet ugyanis nemsok�ra kiontj�k �ldozatul, elt�voz�som ideje k�zel van.  A j� harcot megharcolam, a p�ly�t v�gigfutottam, hitemet megtartottam.  K�szen v�r az igazs�g gy�zelmi koszor�ja, amelyet azon a napon megad nekem az �r, az igazs�gos b�r�, de nemcsak nekem, hanem mindenkinek, aki �r�mmel v�rja elj�vetel�t. 

[Graduale]
!Ps 36:30-31
v.  The mouth of the just shall meditate wisdom : 
V. and his tongue shall speak judgment. The law of his God is in his heart, and his steps shall not be supplanted. Allel�ja, allel�ja
!Sir 45:9
v.  And he girded him about with a glorious girdle, and clothed him with a robe of glory, Allel�ja.

[Tractus]
!Ps 36:30-31
v.  The mouth of the just shall meditate wisdom : 
V. and his tongue shall speak judgment. The law of his God is in his heart, and his steps shall not be supplanted.
!Ps 36:30-31
 Os justi medit�bitur sapi�ntiam, et lingua ejus loqu�tur jud�cium.
V. Lex Dei ejus in corde ips�us: et non supplantab�ntur gressus ejus.
!Ps 111:1-3
v.  Blessed is the man that feareth the Lord: he shall delight exceedingly in his commandments. 
V. His seed shall be mighty upon earth: the generation of the righteous shall be blessed. 
V. Glory and wealth shall be in his house: and his justice remaineth for ever and ever.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 5:13-19
v. Ti vagytok a f�ld s�ja. Ha a s� �z�t veszti, ugyan mivel s�zz�k meg? Nem val� egy�bre, mint hogy kidobj�k, s az emberek eltaposs�k.  Ti vagytok a vil�g vil�goss�ga. A hegyen �p�lt v�rost nem lehet elrejteni.  S ha vil�got gy�jtan�nak, nem rejtik a v�ka al�, hanem a tart�ra teszik, hogy mindenkinek vil�g�tson a h�zban.  Ugyan�gy a ti vil�goss�gotok is vil�g�tson az embereknek, hogy j�tetteiteket l�tva dics��ts�k mennyei Aty�tokat!  Ne gondolj�tok, hogy megsz�ntetni j�ttem a t�rv�nyt vagy a pr�f�t�kat. Nem megsz�ntetni j�ttem, hanem teljess� tenni.  Bizony mondom nektek, m�g �g �s f�ld el nem m�lik, egy i bet� vagy egy vessz�cske sem v�sz el a t�rv�nyb�l, hanem minden beteljesedik.  Aki teh�t csak egyet is elt�r�l e legkisebb parancsok k�z�l, �s �gy tan�tja az embereket, azt igen kicsinek fogj�k h�vni a mennyek orsz�g�ban. Aki viszont megtartja �s tan�tja �ket, az nagy lesz a mennyek orsz�g�ban. 

[Offertorium]
!Ps 91:13
v.  The just shall flourish like the palm tree: he shall grow up like the cedar of Libanus. 

[Secreta] 
My the pious prayer of holy N. thy Bishop and illustrious Doctor be not wanting to us, O Lord, but make our offerings acceptable to thee and ever win for us thy mercy.
$Per Dominum.

[Communio]
!Luke 12:42
v. The faithful and wise steward, whom his lord setteth over his family, to give them their measure of wheat in due season.

[Postcommunio] Pro Doctore Pontifice.
My blessed N. thy Bishop and illustrious Doctor intercede for us, O Lord, that this thy sacrifice may obtain for us salvation,
$Per Dominum
