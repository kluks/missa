[Name]
Commune Confessoris non pontificis;In medio

[Rule]
Gloria

[Introitus]
!Sir 15:5
v. And in the midst of the church she shall open his mouth, and shall fill him with the spirit of wisdom and understanding, and shall clothe him with a robe of glory. 
!Ps 91:2
v. It is good to give praise to the Lord: and to sing to thy name, O most High.
&Gloria
v. And in the midst of the church she shall open his mouth, and shall fill him with the spirit of wisdom and understanding, and shall clothe him with a robe of glory. 

[Oratio]
O God, who didst give blessed N. to be thy people minister in eternal~
salvation grant we pray, that that we who have him for teacher of life here on~
earth, may also deserve now that he is in heaven, to have him for an advocate
$Per Dominum.

[Graduale]
!Ps 36:30-31
v. The mouth of the just shall meditate wisdom :
V. and his tongue shall speak judgment. The law of his God is in his heart, and his steps shall not be supplanted. Allel�ja, allel�ja
!Sir 45:9
v. And he girded him about with a glorious girdle, and clothed him with a robe of glory, Allel�ja.

[Tractus]
!Ps 36:30-31
v. The mouth of the just shall meditate wisdom : and his tongue shall speak judgment. The law of his God is in his heart, and his steps shall not be supplanted.
!Ps 111:1-3
v. Blessed is the man that feareth the Lord: he shall delight exceedingly in his commandments.
V. His seed shall be mighty upon earth: the generation of the righteous shall be blessed.
V. Glory and wealth shall be in his house: and his justice remaineth for ever and ever.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 5:13-19
v. Ti vagytok a f�ld s�ja. Ha a s� �z�t veszti, ugyan mivel s�zz�k meg? Nem val� egy�bre, mint hogy kidobj�k, s az emberek eltaposs�k.  Ti vagytok a vil�g vil�goss�ga. A hegyen �p�lt v�rost nem lehet elrejteni.  S ha vil�got gy�jtan�nak, nem rejtik a v�ka al�, hanem a tart�ra teszik, hogy mindenkinek vil�g�tson a h�zban.  Ugyan�gy a ti vil�goss�gotok is vil�g�tson az embereknek, hogy j�tetteiteket l�tva dics��ts�k mennyei Aty�tokat!  Ne gondolj�tok, hogy megsz�ntetni j�ttem a t�rv�nyt vagy a pr�f�t�kat. Nem megsz�ntetni j�ttem, hanem teljess� tenni.  Bizony mondom nektek, m�g �g �s f�ld el nem m�lik, egy i bet� vagy egy vessz�cske sem v�sz el a t�rv�nyb�l, hanem minden beteljesedik.  Aki teh�t csak egyet is elt�r�l e legkisebb parancsok k�z�l, �s �gy tan�tja az embereket, azt igen kicsinek fogj�k h�vni a mennyek orsz�g�ban. Aki viszont megtartja �s tan�tja �ket, az nagy lesz a mennyek orsz�g�ban. 

[Offertorium]
!Ps 91:13
v. The just shall flourish like the palm tree: he shall grow up like the cedar of Libanus.

[Secreta] !Pro Doctore pontifice
My the pious prayer of holy N. thy Bishop and illustrious Doctor be not wanting to us, O Lord, but make our offerings acceptable to thee and ever win for us thy mercy.
$Per Dominum.

[Secreta1] Pro Doctore non Pontifice.
My the pious prayer of holy N. thy Confessor and illustrious Doctor be not wanting to us, O Lord, but make our offerings acceptable to thee and ever win for us thy mercy.
$Per Dominum.

[Communio]
!Luke 12:42
v. The faithful and wise steward, whom his lord setteth over his family, to give them their measure of wheat in due season.

[Lectio1]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Sir 39:6-14
v. Sziv�t arra adja, hogy kor�n �bredjen az �rhoz, ki �t teremtette, �s a F�ls�gesnek szine el�tt k�ny�r�g.  Megnyitja sz�j�t im�ds�gra, �s v�tkei�rt k�ny�r�g.  Mert ha a nagy �r akarja, az �rtelem lelk�vel bet�lti �t.  �s �, mint a z�pores�t, �gy �rasztja b�lcses�ge besz�deit, �s az im�ds�gban h�l�t ad az �rnak.  A ki majd igazgatja sz�nd�k�t �s tudom�ny�t, �s a titkos dolgokban tan�csot ad.  � kinyilatkoztatja tudom�ny�nak oktat�s�t, �s az �r sz�vets�g�nek t�rv�ny�ben leli dics�s�g�t.  Sokan dics�rik az � b�lcses�g�t, s ez soha sem fog megsz�nni.  Nem eny�szik el eml�kezete, �s neve ism�teltetik nemzed�kr�l nemzed�kre.  B�lcses�g�t hirdetni fogj�k a nemzetek, �s dics�ret�t besz�li a gy�lekezet. 

