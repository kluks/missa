[Name]
Commune Unius Martyris Pontificis, Statuit

[Rule]

[Introitus]
!Ecclus 45:30
v. The Lord made a covenant of friendship with him, and made him a prince; that he should possess the dignity of priesthood forever.
!Ps 131:1
Remember, O Lord, David and all his meekness.
&Gloria
v. The Lord made a covenant of friendship with him, and made him a prince; that he should possess the dignity of priesthood forever.

[Oratio]
Be mindful of our weakness, almighty God, and because the burden of our sins weighs heavily upon us, may the glorious intercession of blessed N., Your Martyr and Bishop, sustain us.
$Per Dominum

[Lectio]
Olvasm�ny szent Jakab Apostol level�b�l
!James 1:12 - 18.
Boldog az az ember, aki a k�s�rt�sben helyt�ll, mert miut�n ki�llja a pr�b�t, megkapja az �let koszor�j�t, amelyet az �r azoknak �g�rt, akik �t szeretik. Senki se mondja, aki k�s�rt�st szenved: "Az Isten k�s�rt", mert az Istent nem lehet rosszra cs�b�tani, �s � sem cs�b�t rosszra senkit. Minden embert a saj�t k�v�ns�ga vonzza �s cs�b�tja, �gy esik t�rbe. Azt�n a k�v�ns�g, ha m�r megfogant, b�nt sz�l, a b�n meg, ha elk�vetik, hal�lt sz�l. Ne legyetek h�t t�ved�sben, szeretett testv�reim. Minden j� adom�ny �s minden t�k�letes aj�nd�k fel�lr�l van, a vil�goss�g Atyj�t�l sz�ll al�, akiben nincs v�ltoz�s, m�g �rny�ka sem a v�ltoz�snak. Akarattal h�vott minket �letre az igazs�g szav�val, hogy mintegy zseng�je legy�nk teremtm�nyeinek.

[Graduale]
!Ps 88:21-23
I have found David, My servant, with My holy oil I have anointed him, that My hand may be always with him, and that My arm may make him strong.
V. No enemy shall deceive him, nor shall the wicked afflict him. Alleluia, alleluia.
!Ps 109:4
V. You are a priest forever, according to the order of Melchisedec. Alleluia.

[Tractus]
!Ps 20:3-4  
You have granted him his heart�s desire; You refused not the wish of his lips.
V. For You welcomed him with goodly blessings.
V. You placed on his head a crown of pure gold.

[Evangelium]
Evang�lium szent Luk�cs k�nyv�b�l
!Luke 14:26 - 33
Ha valaki k�vetni akar, de nem gy�l�li apj�t, anyj�t, feles�g�t, gyermekeit, fiv�reit �s n�v�reit, s�t m�g saj�t mag�t is, nem lehet a tan�tv�nyom. Aki nem veszi fel keresztj�t �s nem k�vet, nem lehet a tan�tv�nyom. Aki tornyot akar �p�teni, nem �l-e le el�bb, hogy kisz�m�tsa a k�lts�geket, vajon futja-e p�nz�b�l, hogy fel is �p�tse? Nehogy azut�n, hogy az alapokat lerakta, de befejezni nem tudta, mindenki, aki csak l�tja, kics�folja: Ez az ember �p�tkez�sbe fogott, de nem tudta befejezni. Vagy melyik kir�ly nem �l le, miel�tt hadba vonulna egy m�sik kir�ly ellen, sz�mot vetni, vajon a maga t�zezernyi katon�j�val szembe tud-e sz�llni azzal, aki h�szezerrel j�n ellene? Mert ha nem, k�vets�get k�ld hozz� m�g akkor, amikor messze van, �s b�k�t k�r. �gy h�t aki k�z�letek nem mond le minden�r�l, amije csak van, nem lehet a tan�tv�nyom.

[Offertorium]
!Ps 88:25
May faithfulness and My kindness shall be with him, and through My name shall his horn be exalted.

[Secreta]
O Lord, graciously accept, through the merits of Your blessed Martyr and Bishop N., the sacrificial gifts dedicated to You, and grant they may prove to be for us a lasting help.
$Per Dominum

[Communio]
!Ps 88:36 - 38.
Once, by My holiness, have I sworn; his posterity shall continue forever; and his throne shall be like the sun before Me; like the moon, which remains forever - a faithful witness in the sky.

[Postcommunio]
Refreshed by partaking of the sacred gift, we beseech You, O Lord our God, that we may enjoy the benefits of the rite we perform through the intercession of blessed N., Your Martyr and Bishop.
$Per Dominum
