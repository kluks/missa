[Name]
Commune plurimorum martirum extra Tempus Paschale; Salus autem

[Introitus]
!Ps 36:39
v. The salvation of the just is from the Lord; He is their refuge in time of~
distress.
!Ps 36:1
Be not vexed over evildoers, nor jealous of those who do wrong.
&Gloria
v. The salvation of the just is from the Lord; He is their refuge in time of~
distress.

[Oratio]: Of Martyrs not Bishops
O God, Who gladden us by the annual festival of Your Martyrs N. and N., grant~
that we may be inspired by the example of those in whose merits we rejoice.
$Per Dominum

[Oratio1]: Of Martyr-Bishops
May the feast of the blessed Martyrs and Bishops N. and N., protect us, O Lord,~
we beseech You, and may their holy prayer recommend us unto You.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol Zsid�khoz irott level�b�l
!Heb 10:32 - 38.
v. Eml�kezzetek vissza az elm�lt id�kre, amikor megvil�gosod�stok ut�n annyi szenved�sben �s k�zdelemben helyt�lltatok.  R�szint szidalmaz�snak �s szorongat�snak voltatok kit�ve, r�szint k�z�ss�get v�llaltatok azokkal, akik hasonl�k�ppen szenvedtek.  Egy�tt szenvedtetek a foglyokkal, �s vagyonotok elkobz�s�t is �r�mmel elviselt�k, abban a tudatban, hogy �rt�kesebb �s maradand�bb javakkal rendelkeztek.  Ne vesz�ts�tek h�t el bizalmatokat, hiszen nagy jutalom j�r �rte.  �llhatatosnak kell lennetek, hogy az Isten akarat�t teljes�ts�tek, �s az �g�retet elnyerj�tek:  M�r csak r�vid, nagyon r�vid id�, azt�n elj�n az elj�vend� �s nem k�sik.  Az igaz ember a hitb�l �l, de ha elp�rtol, nem telik benne kedvem. 

[Graduale]
!Ps 39:18-19
When the just cry out the Lord hears them, and from all their distress He~
rescues them.
V. The Lord is close to those who are contrite of heart; and those who are~
humble of spirit He saves.
!Ps 39:18-19
When the just cry out the Lord hears them, and from all their distress He~
rescues them.
V. The Lord is close to those who are contrite of heart; and those who are~
humble of spirit He saves. Alleluia, alleluia.
V. The white-robed army of Martyrs praises You, O Lord. Alleluia.

[Tractus]
!Ps 125:5-6
Those that sow in tears shall reap rejoicing. V} Although they go forth weeping, carrying the seed to be sown.
V. They shall come back rejoicing, carrying their sheaves.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 12:1 - 8.
v. K�zben t�zezres t�meg ver�d�tt �ssze k�r�l�tte, �gyhogy egym�st tiport�k, �s el�sz�r csak tan�tv�nyaihoz kezdett besz�lni: "�vakodjatok a farizeusok kov�sz�t�l, vagyis a k�pmutat�st�l.  Nincs semmi elrejtve, ami nyilv�noss�gra ne ker�lne, s titok, ami ki ne tud�dna.  Amit s�t�tben mondtok, vil�gos nappal hallj�k majd, �s amit a z�rt falak k�zt f�lbe s�gtok, azt a h�ztet�kr�l fogj�k hirdetni.  Nektek, bar�taimnak, mondom: Ne f�ljetek azokt�l, akik a testet ugyan meg�lik, de azt�n semmi t�bbet nem tehetnek.  Megmondom �n nektek, kit�l f�ljetek: Att�l f�ljetek, akinek, miut�n meg�lt, ahhoz is van hatalma, hogy a k�rhozatba tasz�tson benneteket. Igen, mondom nektek, t�le f�ljetek.  Ugye k�t fill�r�rt �t verebet is adnak. M�gis az Isten nem feledkezik meg egyetlen egyr�l sem.  S�t m�g a fejeteken a hajsz�lakat is mind sz�mon tartja. Ne f�ljetek h�t! Sokkal t�bbet �rtek ti, mint a verebek.  Mondom nektek, hogy aki tan�s�got tesz mellettem az emberek el�tt, azt majd az Emberfia is mag��nak vallja az Isten angyalai el�tt. 

[Secreta]: Of Martyrs not Bishops
Be appeased by the gifts we offer You, O Lord, and through the intercession of~
Your holy Martyrs N. and N., safeguard us from all dangers.
$Per Dominum

[Secreta1]: Of Martyr-Bishops
Look with favor, O Lord, upon our humble prayers which we are offering in~
commemoration of Your Saints, so that we, who have no confidence in our own~
righteousness, may be helped by the merits of those who have been pleasing to~
You.
$Per Dominum

[Communio]
!Matt 10:27
What I tell you in darkness, speak it in the light, says the Lord; and what you~
hear whispered, preach it on the housetops.

[Postcommunio]: Of Martyrs not Bishops
May this Communion, O Lord, cleanse us of sin, and, through the intercession of~
Your holy Martyrs N. and N., make us sharers of heavenly salvation.
$Per Dominum

[Postcommunio1]: Of Martyr-Bishops
Filled with the sacrament of salvation, we beseech You, O Lord, that we may be~
helped by the prayers of those whose feast-day we celebrate.
$Per Dominum
