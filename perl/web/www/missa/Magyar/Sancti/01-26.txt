[Rank]
S. Polycarpi Episcopi et Martyris;;Duplex;;3;;vide C2

[Rank1570]
S. Polycarpi Episcopi et Martyris;;Simplex;;1.1;;vide C2

[Rule]
vide C2;
Gloria

[Introitus]
!Dan 3:84, 87.
v. O ye priests of the Lord, bless the Lord: O ye holy and humble of heart,~
praise God.
!Dan. 3:57
All ye works of the Lord, bless the Lord; praise and exalt Him above all for~
ever.
&Gloria
v. O ye priests of the Lord, bless the Lord: O ye holy and humble of heart,~
praise God.

[Oratio]
O God, who givest us joy by the annual solemnity of blessed Polycarp, Thy martyr~
and bishop, mercifully grant that we may rejoice in his protection, whose~
birthday we celebrate.
$Per Dominum

[Lectio]
Olvasm�ny szent J�nos apostol els� level�b�l
!1 John 3:10-16
v. Err�l ismerhet�k fel az Isten gyermekei �s a s�t�n fiai. Aki nem az igazs�goss�got teszi, az nem Istent�l val�. Ugyan�gy az sem, aki nem szereti testv�r�t.  Mert ezt az �zenetet halljuk kezdett�l fogva: Szeress�k egym�st!  Ne tegy�nk �gy, mint K�in, aki a gonoszt�l val� volt, �s meg�lte testv�r�t. Mi�rt �lte meg? Mert az � tettei gonoszak voltak, a testv�re tettei ellenben igazak.  Ne csod�lkozzatok azon, testv�rek, hogy a vil�g gy�l�l benneteket.  Mi tudjuk, hogy a hal�lb�l �tment�nk az �letbe, mert szeretj�k testv�reinket. Aki nem szeret, a hal�lban marad.  Aki gy�l�li testv�r�t, gyilkos, m�rpedig tudj�tok, hogy egy gyilkosnak sem maradand� birtoka az �r�k �let.  A szeretetet arr�l ismerj�k fel, hogy � �let�t adta �rt�nk. Nek�nk is k�teless�g�nk �let�nket adni testv�reink�rt. 

[Graduale]
!Ps 8:6-7
Thou hast crowned him with glory and honor.
V. And hast set him over the works of Thy hands, O Lord. Alleluia, alleluia.
V. This is the priest whom the Lord hath crowned. Alleluia.

[Tractus]
!Ps 111:1-3
Blessed is the man that feareth the Lord; he delighteth exceedingly in His commandments.
V. His seed shall be mighty upon the earth: the generation of the righteous shall be blessed.
V. Glory and wealth shall be in his house, and his justice remaineth for ever and ever.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 10:26-32
v. Ne f�ljetek h�t t�l�k! Hiszen semmi sincs elrejtve, ami nyilv�noss�gra ne ker�lne, s olyan titok sincs, ami ki ne tud�dn�k.  Amit s�t�tben mondok nektek, azt mondj�tok el f�nyes nappal, �s amit a f�letekbe s�gnak, azt hirdess�tek a h�ztet�kr�l!  Ne f�ljetek azokt�l, akik a testet meg�lik, a lelket azonban nem tudj�k meg�lni! Ink�bb att�l f�ljetek, aki a k�rhozatba vetve a testet is, a lelket is el tudja puszt�tani!  Ugye k�t verebet adnak egy fill�ren? S Aty�tok tudta n�lk�l egy sem esik le a f�ldre.  Nektek minden sz�l hajatok sz�mon tartj�k.  Ne f�ljetek h�t! Sokkal t�bbet �rtek a verebekn�l.  Azokat, akik megvallanak engem az emberek el�tt, �n is megvallom majd mennyei Aty�m el�tt. 

[Offertorium]
!Ps 88:21-22
I have found David My servant, with My holy oil I have anointed him; for My hand~
shall help him, and My arm shall strengthen him.

[Secreta]
Sanctify, O Lord, the offerings dedicated unto Thee, and appeased by the~
intercession of blessed Polycarp Thy Martyr and bishop, and also by this~
sacrifice, look mercifully upon us.
$Per Dominum

[Communio]
!Ps 20:4
Thou hast set on his head, O Lord, a crown of precious stones.

[Postcommunio]
May this communion, O Lord purify us from guilt, and by the intercession of~
blessed Polycarp Thy martyr and bishop, make us partakers of a heavenly remedy.
$Per Dominum
