[Rank]
S. Bonaventurae Episcopi Confessoris;;Duplex;;3;;vide C4a

[Rank1570]
S. Bonaventurae Episcopi Confessoris;;Semiduplex;;2;;vide C4a

[Rule]
vide C4a;
Gloria
CredoDA

[Introitus]
!Sir 15:5
v. In the midst of the assembly he opened his mouth; and the Lord filled him~
with the spirit of wisdom and understanding; He clothed him with a robe of~
glory.
!Ps 91:2
It is good to give thanks to the Lord, to sing praise to Your name, Most High.
&Gloria
v. In the midst of the assembly he opened his mouth; and the Lord filled him~
with the spirit of wisdom and understanding; He clothed him with a robe of~
glory.

[Oratio]
O God, Who gave to Your people, blessed Bonaventure, as a minister of salvation,~
grant, we beseech You, that we who cherished him on earth as a teacher of life,~
may be found worthy to have him as an intercessor in heaven.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol Tim�teushoz irott m�sodik level�b�l
!2 Tim. 4:1-8
v. K�rve-k�rlek az Istenre �s Krisztus J�zusra, aki �t�lkezni fog �l�k �s holtak f�l�tt, az � elj�vetel�re �s orsz�g�ra:  hirdesd az evang�liumot, �llj vele el�, ak�r alkalmas, ak�r alkalmatlan. �rvelj, ints, buzd�ts nagy t�relemmel �s hozz��rt�ssel.  Mert j�n id�, amikor az eg�szs�ges tan�t�st nem hallgatj�k sz�vesen, hanem saj�t �zl�s�k szerint szereznek maguknak tan�t�kat, hogy f�l�ket csiklandoztass�k.  Az igazs�got nem hallgatj�k meg, de a mes�ket elfogadj�k.  Te azonban maradj mindenben meggondolt, viseld el a bajokat, teljes�tsd az evang�lium hirdet�j�nek feladat�t, t�ltsd be szolg�latodat.  Az �n v�remet ugyanis nemsok�ra kiontj�k �ldozatul, elt�voz�som ideje k�zel van.  A j� harcot megharcolam, a p�ly�t v�gigfutottam, hitemet megtartottam.  K�szen v�r az igazs�g gy�zelmi koszor�ja, amelyet azon a napon megad nekem az �r, az igazs�gos b�r�, de nemcsak nekem, hanem mindenkinek, aki �r�mmel v�rja elj�vetel�t. 

[Graduale]
!Ps 36:30-31
The mouth of the just tells of wisdom and his tongue utters what is right.
V. The law of his God is in his heart, and his steps do not falter. Alleluia,~
alleluia.
!Ps 109:4
V. The Lord has sworn, and He will not repent: You are a priest forever,~
according to the order of Melchisedec. Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 5:13-19
v. Ti vagytok a f�ld s�ja. Ha a s� �z�t veszti, ugyan mivel s�zz�k meg? Nem val� egy�bre, mint hogy kidobj�k, s az emberek eltaposs�k.  Ti vagytok a vil�g vil�goss�ga. A hegyen �p�lt v�rost nem lehet elrejteni.  S ha vil�got gy�jtan�nak, nem rejtik a v�ka al�, hanem a tart�ra teszik, hogy mindenkinek vil�g�tson a h�zban.  Ugyan�gy a ti vil�goss�gotok is vil�g�tson az embereknek, hogy j�tetteiteket l�tva dics��ts�k mennyei Aty�tokat!  Ne gondolj�tok, hogy megsz�ntetni j�ttem a t�rv�nyt vagy a pr�f�t�kat. Nem megsz�ntetni j�ttem, hanem teljess� tenni.  Bizony mondom nektek, m�g �g �s f�ld el nem m�lik, egy i bet� vagy egy vessz�cske sem v�sz el a t�rv�nyb�l, hanem minden beteljesedik.  Aki teh�t csak egyet is elt�r�l e legkisebb parancsok k�z�l, �s �gy tan�tja az embereket, azt igen kicsinek fogj�k h�vni a mennyek orsz�g�ban. Aki viszont megtartja �s tan�tja �ket, az nagy lesz a mennyek orsz�g�ban. 

[Offertorium]
!Ps 91:13
The just shall flourish like the palm tree; like a cedar of Libanus shall he~
grow.

[Secreta]
May the loving prayer of blessed Bonaventure, Your Confessor and Doctor, fail us~
never, O Lord; may it commend our offerings and ever secure for us Your~
forgiveness.
$Per Dominum

[Communio]
!Luke 12:42
The faithful and prudent servant whom the master will set over his household to~
give them their rations of grain in due time.

[Postcommunio]
So that your sacrificial rites may grant us salvation, we pray you, O Lord, that~
blessed Bonaventure, Your Bishop and illustrious Doctor, may draw nigh as our~
intercessor.
$Per Dominum
