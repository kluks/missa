[Rank]
Die VI infra Octavam Nativitatis;;Semiduplex;;2;;ex Sancti/12-25m3

[Rank1960]
Die sexta post Nativitatem;;Duplex II class;;5;;ex Sancti/12-25m3

[Rule]
ex Sancti/12-25m3;
Gloria
Credo
Prefatio=Nat

[Introitus]
!Isa 9:6
v. A Child is born to us, a Son is given to us; upon His shoulder dominion rests;~
and His name shall be called the Angel of great counsel.
!Ps 97:1
Sing to the Lord a new song, for He has done wondrous deeds.
&Gloria
v. A Child is born to us, a Son is given to us; upon His shoulder dominion rests;~
and His name shall be called the Angel of great counsel.

[Oratio]
Grant, we beseech You, almighty God, that the new birth, in the flesh, of Your~
only-begotten Son may deliver us whom the bondage of old keeps under the yoke of~
sin.
$Per eundem

[Lectio]
Olvasm�ny szent P�l apostol Tituszhoz irott level�b�l
!Tit 3:4-7
v. Amikor azonban �dv�z�t� Isten�nk kinyilv�n�totta j�s�g�t �s emberszeretet�t, megmentett minket.  Nem az�rt, mert igazak voltak tetteink, hanem irgalmass�gb�l, s a Szentl�lekben val� �jj�sz�let�s �s meg�jul�s f�rd�j�ben,  akit �dv�z�t�nk, J�zus Krisztus �ltal b�ven �rasztott r�nk,  hogy kegyelm�vel megigazuljunk, s az �r�k �let rem�nybeli �r�k�seiv� v�ljunk. 

[Graduale]
!Ps 97:3-4, 2.
All the ends of the earth have seen the salvation by our God. Sing joyfully to~
God, all you lands.
V. The Lord has made His salvation known: in the sight of the nations He has~
revealed His justice. Alleluia, alleluia.
V. A sanctified day has shone upon us; come, you nations, and adore the Lord:~
for this day a great light has descended upon the earth. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 2:15-20
v. Mihelyt az angyalok visszat�rtek a mennybe, a p�sztorok �gy sz�ltak egym�shoz: "Menj�nk el Betlehembe, hadd l�ssuk a val�ra v�lt besz�det, amit az �r tudtunkra adott!"  Gyorsan �tra keltek, �s megtal�lt�k M�ri�t, J�zsefet �s a j�szolban fekv� gyermeket.  Miut�n l�tt�k, az ezen gyermekr�l nekik mondottak alapj�n ismert�k fel.  Aki csak hallotta, csod�lkozott a p�sztorok besz�d�n.  M�ria meg mind eml�kezet�be v�ste szavaikat �s sz�v�ben egyeztette.  A p�sztorok hazat�rtek, dics��tett�k �s magasztalt�k az Istent minden�rt, amit csak hallottak, �s �gy l�ttak, ahogy tudtul adt�k nekik. 

[Offertorium]
!Ps 88:12, 15.
Yours are the heavens, and Your is the earth; the world and its fullness You~
have founded. Justice and judgment are the foundation of Your throne.

[Secreta]
Make holy the sacrificial gifts we offer, O Lord, and by the new birth of Your~
only-begotten Son cleanse us from the stains of our sins.
$Per eumdem

[Communio]
!Ps 97:3
All the ends of the earth have seen the salvation by our God.

[Postcommunio]
Grant, we beseech You, almighty God, that the Savior of the world, born this day,~
Who is the author of our birth in godliness, may bestow on us immortal life.
$Qui tecum
