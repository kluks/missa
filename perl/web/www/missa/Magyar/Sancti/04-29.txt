[Rank]
S. Petri Martyris;;Duplex;;3;;vide C2

[Rule]
vide C2;

[Oratio]
Grant us grace, we beseech thee, O Almighty God, to follow with zeal conformable~
thereto after the pattern of that great ensample of faith, thy blessed Martyr~
Peter, who, for the spreading of the same faith, did so run as to obtain the~
palm of martyrdom. 
$Per Dominum.

[Lectio]
Olvasm�ny szent P�l apostol Tim�teushoz irott m�sodik level�b�l
!2 Tim 2:8-10; 3:10-12
v. Ne felejtsd, hogy J�zus Krisztus, D�vid sarja felt�madt a hal�lb�l, ahogy evang�liumom hirdeti, amely�rt meghurcoltak,  s�t mint valami gonosztev�t, m�g bilincsbe is vertek, de az Isten szava nincs megbilincselve.  A v�lasztottak�rt teh�t mindent elt�r�k, hogy az �r�k dics�s�gben �k is elnyerj�k az �dv�ss�get Krisztus J�zusban.  Te k�vett�l a tan�t�sban, �letm�dom �s �letc�lom tekintet�ben, a hitben, a kitart�sban, a szeretben, a t�relemben,  az �ld�ztet�sekben �s a szenved�sekben, amik Anti�chi�ban, Ikoniumban �s Lisztr�ban �rtek. Mi minden �ld�ztet�st magamra v�llaltam! De az �r mindig kimentett.  S az �ld�z�sb�l mindenkinek kijut, aki buzg�n akar �lni Krisztus J�zusban. 

[Secreta]
Graciously regard, O Lord, the prayers, we offer thee, through the intercession of blessed peter, thy Martyr; and keep ever under thy protection the champions of the faith.
$Per Dominum

[Postcommunio]
May the Sacrament, O Lord, which we, the faithful people have received, hold us in safe keeping, and through the intercession of thy blessed Martyr Peter defend us from all assaults of the enemy.
$Per Dominum
