[Rank]
Ss. Tryphon, Respicius, and Nympha martyrs;;Simplex;;1.1;;vide C3

[Rule]
vide C3;

[Introitus]
!Ps 33:18
v. When the just cry out, the Lord hears them, and from all their distress He~
rescues them.
!Ps 33:2
I will bless the Lord at all times; His praise shall be ever in my mouth.
&Gloria
v. When the just cry out, the Lord hears them, and from all their distress He~
rescues them.

[Oratio]
Make us, we beseech You, O Lord, ever faithful to observe the feast-day of Your~
holy Martyrs, Tryphon, Respicius and Nympha, that through their prayers, we may~
enjoy the bounty of Your protection.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol R�maiakhoz irott level�b�l
!Rom 8:18 - 23.
v. De ennek az �letnek a szenved�sei v�lem�nyem szerint nem m�rhet�k az elj�vend� dics�s�ghez, amely majd megnyilv�nul rajtunk.  Maga a term�szet s�v�rogva v�rja Isten fiainak megnyilv�nul�s�t.  A term�szet ugyanis muland�s�gnak van al�vetve, nem mert akarja, hanem amiatt, aki abban a rem�nyben vetette al�,  hogy a muland�s�g szolgai �llapot�b�l majd felszabadul az Isten fiainak dics�s�ges szabads�g�ra.  Tudjuk ugyanis, hogy az eg�sz term�szet egy�tt s�hajtozik �s vaj�dik mindm�ig.  De nemcsak az, hanem mi magunk is, akik bens�nkben hordozzuk a L�lek zseng�j�t, s�hajtozunk, �s v�rjuk a fogadott fi�s�got, test�nk megv�lt�s�t. 

[Graduale]
!Ps 78:10, 2.
Avenge, O Lord, the shedding of Your servants� blood.
V. They have given the corpses of Your servants as food to the birds of heaven,~
the flesh of Your faithful ones to the beasts of the earth. Alleluia, alleluia.
!Ps 115:15
V. Precious in the eyes of the Lord is the death of His faithful ones. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 14:12:1 - 8.
v. Amikor J�zus szombaton egy vezet� farizeus h�z�ba ment, hogy n�la �tkezz�k, figyelt�k.  �me, el�tte �llt egy v�zk�ros ember.  J�zus megk�rdezte a t�rv�nytud�kat �s a farizeusokat: "Szabad szombaton gy�gy�tani, vagy nem szabad?"  Azok hallgattak. Erre meg�rintette kez�vel, meggy�gy�totta, �s �tnak bocs�totta.  Azt�n hozz�juk fordult: "Ha valamelyiteknek a fia vagy az �kre k�tba esik, nem h�zza-e ki r�gt�n, ak�r szombaton is?"  Erre nem tudtak mit felelni.  A megh�vottaknak egy p�ldabesz�det mondott, mert �szrevette, hogyan v�logatj�k az els� helyeket.  "Amikor lakodalomba h�vnak - kezdte -, ne telepedj�l le a f�helyre, mert akadhat a hivatalosak k�zt n�lad el�kel�bb is. 

[Offertorium]
!Ps 31:11
Be glad in the Lord, and rejoice, you just; exult, all you upright of heart.

[Secreta]
We offer You, O Lord, the gifts of our homage; may they be made pleasing to You~
for the honor of all Your just ones, and, by Your mercy, salutary for us.
$Per Dominum

[Communio]
!Matt 12:50
Whoever does the will of My Father in heaven, he is My brother and sister and~
mother, says the Lord.

[Postcommunio]
Grant, we beseech You, O Lord, by the intercession of blessed Tryphon, Respicius~
and Nympha, Your Martyrs, that what we taste with our lips we may consume with a~
pure heart.
$Per Dominum
