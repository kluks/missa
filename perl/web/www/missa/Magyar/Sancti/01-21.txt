[Rank]
S. Agnetis Virginis et Martyris;;Duplex;;3.1;;vide C6

[Rule]
vide C6;
Gloria

[Introitus]
!Ps 118:95-96
v. Sinners wait to destroy me, but I pay heed to Your decrees, O Lord. I see~
that all fulfillment has its limits; broad indeed is Your command.
!Ps 118:1
Happy are they whose way is blameless, who walk in the law of Lord.
&Gloria
v. Sinners wait to destroy me, but I pay heed to Your decrees, O Lord. I see~
that all fulfillment has its limits; broad indeed is Your command.

[Oratio]
Almighty, eternal God, You Who choose the weak things of the world to confound~
the strong, mercifully grant that we who are celebrating the feast of blessed~
Agnes, Your Virgin and Martyr, may reap the benefit of her patronal intercession~
with You.
$Per Dominum

[Lectio]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Ecclus 51:1-8; 51:12
v. J�zusnak, Sir�k fi�nak im�ds�ga. H�l�t adok neked, Uram Kir�lyom! �s dics�rlek t�ged, szabad�t� Istenemet.  H�l�t adok a te nevednek, mert seg�t�m �s oltalmaz�m lett�l.  Megszabad�tottad testemet a roml�st�l, a rosz nyelvek t�r�t�l, �s a hazuds�got kohol�k ajkait�l, �s az ellenem �ll�k szine el�tt segit�m lett�l.  �s megszabad�tott�l engem a te neved irgalm�nak sokas�ga szerint az elnyel�semre k�sz�lt ord�t�kt�l,  az �n lelkemet keres�k kezeib�l, �s a h�bor�s�g kapuib�l, mely k�r�lvett engem,  a l�ng �get�s�t�l, mely k�r�lvett engem, �s a t�z k�zepett nem �gtem meg,  a pokol gyomr�nak m�lys�g�b�l, �s a megfert�z�tt nyelvt�l, �s a hazug ig�t�l, az igazs�gtalan kir�lyt�l �s az �lnok nyelvt�l.  Mindhal�lig dics�ri lelkem az Urat;  hogy kimented, Uram, a te r�d v�rakoz�kat, �s kiszabad�tod �ket a pog�nyok kezeib�l. 

[Graduale]
!Ps 44:8
Grace is poured out upon your lips; thus God has blessed you forever.
V. In the cause of truth and mercy and for the sake of justice; may your right~
hand show you wondrous deeds. Alleluia, alleluia.
!Matt. 25:4; 25:6
The five wise virgins took oil in their vessels with the lamps; and at midnight a~
cry arose, Behold the bridegroom is coming, go forth to meet Christ our Lord.~
Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 25:1-13
v. A mennyek orsz�ga hasonl�t a t�z sz�zh�z, akik fogt�k l�mp�sukat, �s a v�leg�ny el� mentek.  �t balga volt k�z�l�k, �t pedig okos.  A balg�k l�mp�sukat ugyan elvitt�k, de olajat nem vittek bele.  Az okosak l�mp�sukkal egy�tt olajat is vittek kors�jukban.  A v�leg�ny k�sett, �gy valamennyien el�lmosodtak �s elaludtak.  �jf�lkor ki�lt�s hallatszott: Itt a v�leg�ny, menjetek ki el�be! -  Erre a sz�zek mind f�lkeltek, �s rendbe hozt�k l�mp�sukat.  A balg�k k�rt�k az okosakat: Adjatok egy kis olajat! L�mp�sunk kialv�ban van. -  Nem adunk - felelt�k az okosak -, mert akkor nem lesz el�g se nek�nk, se nektek. Menjetek ink�bb az �rusokhoz �s vegyetek magatoknak.  M�g odavoltak v�s�rolni, meg�rkezett a v�leg�ny, �s akik k�szen voltak, bevonultak vele a menyegz�re. Ezzel az ajt� bez�rult.  K�s�bb meg�rkezett a t�bbi sz�z is. Besz�ltak: Uram, Uram, nyiss ki nek�nk!  De � �gy v�laszolt: Bizony mondom nektek, nem ismerlek benneteket.  Legyetek h�t �berek, mert nem tudj�tok sem a napot, sem az �r�t. 

[Offertorium]
!Ps 44:15-16
Behind her the virgins of her train are brought to the King. They are borne in~
to You with gladness and joy; they enter the palace of the Lord, the King.

[Secreta]
Graciously accept, O Lord, the sacrificial gifts we offer You and by the~
intercession of blessed Agnes, Your Virgin and Martyr, loose our sins.
$Per Dominum

[Communio]
!Matt 25:4, 6.
The five wise virgins took oil in their vessels with the lamps; and at midnight a~
cry arose, Behold the bridegroom is coming, go forth to meet Christ our Lord.

[Postcommunio]
Filled with heavenly food and drink, we humbly pray You, our God, that we may be~
helped by the prayers of her in whose memory we have received Your sacrament.
$Per Dominum
