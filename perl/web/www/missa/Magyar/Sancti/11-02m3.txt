[Rank]
Commemoratio Omnium Fidelium Defunctorum Ad tertiam Missam;;Duplex;;3;;ex C9

[RankNewcal]
Commemoratio Omnium Fidelium Defunctorum Ad tertiam Missam;;Duplex Solemnity;;6;;ex C9

[Rank1960]
Commemoratio Omnium Fidelium Defunctorum Ad tertiam Missam;;I. classis;;6;;ex C9

[Rule]
ex C9
no Gloria
no Credo
Prefatio=Defunctorum
multiple3
Sequentia
Requiem gloria

[Oratio]
O God, Who forgives sins and desire to save mankind, we beseech Your mercy to~
grant, by the intercession of Blessed Mary, ever Virgin, and that of all Your~
Saints, that the souls of Your servants and handmaids who have departed this~
life may enter upon the enjoyment of unending happiness.
$Per Dominum

[Lectio]
Olvasm�ny a Jelen�sek k�nyv�b�l
!Apoc 14:13
v. Erre sz�zatot hallottam az �gb�l: "Jegyezd fel: Mostant�l fogva boldogok a halottak, akik az �rban haltak meg. Igen, mondja a L�lek, hadd pihenj�k ki f�radalmaikat, mert tetteik elk�s�rik �ket." 

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 6:51-55
v. �n vagyok a mennyb�l al�sz�llott �l� keny�r. Aki e keny�rb�l eszik, �r�kk� �l. A keny�r, amelyet adok, a testem a vil�g �let��rt."  Erre vita t�madt a zsid�k k�zt: "Hogy adhatja ez a test�t eledel�l?"  J�zus ezt mondta r�: "Bizony, bizony, mondom nektek: Ha nem eszitek az Emberfia test�t �s nem issz�tok a v�r�t, nem lesz �let bennetek.  De aki eszi az �n testemet, �s issza az �n v�remet, annak �r�k �lete van, s felt�masztom az utols� napon.  A testem ugyanis val�s�gos �tel, s a v�rem val�s�gos ital. 

[Secreta]
O God, Whose mercy is boundless, graciously accept our humble prayers, and,~
through this sacrament of salvation, grant to the souls of all the faithful~
departed, to whom You gave the grace of believing in You, the remission of all~
their sins.
$Per Dominum

[Postcommunio]
Grant, we beseech You, O almighty and merciful God, that the souls of Your~
servants and handmaids for whom we have offered this sacrifice of praise to Your~
Majesty, cleansed of all sins by the virtue of this sacrament, may, by Your~
mercy, receive the joy of perpetual light.
$Per Dominum
