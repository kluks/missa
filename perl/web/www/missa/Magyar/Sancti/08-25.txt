[Rank]
S. Ludovici Confessoris;;Semiduplex;;2;;vide C5

[Rank1570]
S. Ludovici Confessoris;;Simplex;;1.1;;vide C5

[RankTrident]
S. Ludovici Confessoris;;Duplex;;3;;vide C5

[Rank1960]
S. Ludovici Confessoris;;Duplex;;3;;vide C5

[RankNewcal]
S. Ludovioci Confessoris;;Duplex optional;;2;;vide C5

[Rule]
vide C5;
Gloria

[Introitus]
!Ps 36:30-31
v. The mouth of the just man tells of wisdom, and his tongue utters what is~
right. The law of his God is in his heart.
!Ps 36:1
Be not vexed over evildoers, nor jealous of those who do wrong.
&Gloria
v. The mouth of the just man tells of wisdom, and his tongue utters what is~
right. The law of his God is in his heart.

[Oratio]
O God, Who transported Your blessed Confessor Louis from an earthly throne to~
the glory of the heavenly kingdom, by his merits and intercession we beseech You~
to make us of the company of the King of kings, Jesus Christ Your Son.
$Qui tecum

[Lectio]
Olvasm�ny a B�lcsess�g k�nyv�b�l
!Wis 10:10:14
v. Lement vele a b�rt�nbe, �s nem hagyta el a bilincsekben sem, m�g nem hozta neki az orsz�g jogar�t �s a hatalmat elnyom�i f�l�tt, m�g meg nem hazudtolta r�galmaz�it �s �r�k h�rnevet nem szerzett neki. 

[Graduale]
!Ps 91:13-14
The just man shall flourish like the palm tree, like a cedar of Lebanon shall he~
grow in the house of the Lord.
!Ps 91:3
V. To proclaim Your kindness at dawn and Your faithfulness throughout the night.~
Alleluia, alleluia.
!James 1:12
V. Blessed is the man who endures temptation; for when he has been tried, he~
will receive the crown of life. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 19:12-26
v. "Egy f�ember - kezdte - messze f�ldre indult, hogy kir�lys�got szerezzen mag�nak, s azt�n visszat�rjen.  Mag�hoz h�vatta t�z szolg�j�t, adott nekik t�z min�t �s �gy sz�lt hozz�juk: Kamatoztass�tok, m�g vissza nem t�rek.  Polg�rt�rsai gy�l�lt�k, ez�rt k�vets�get k�ldtek ut�na �s tiltakoztak: Nem akarjuk, hogy kir�lyunk legyen.  M�gis megszerezte a kir�lys�got �s visszat�rt. H�vatta szolg�it, akiknek a p�nzt adta, hogy megtudja, ki hogyan forgatta.  J�tt az els�, s �gy sz�lt: Uram, min�d t�z min�t hozott.  J�l van, der�k szolga! - felelte neki. - Mivel a kicsiben h� volt�l, hatalmad lesz t�z v�ros felett.  J�tt a m�sodik is: Uram - mondta -, min�d �t min�t j�vedelmezett.  Ennek ezt v�laszolta: T�ged �t v�ros f�l� rendellek.  V�g�l j�tt a harmadik �s �gy besz�lt: Uram, itt a min�d! Kend�be k�t�ttem �s eldugtam,  f�ltem ugyanis t�led, mert szigor� ember vagy. F�lveszed, amit nem te tett�l le, �s learatod, amit nem te vetett�l. -  A magad sz�j�b�l �t�llek meg - mondta neki -, te mihaszna szolga. Tudtad, hogy szigor� ember vagyok. F�lveszem, amit nem �n tettem le �s learatom, amit nem �n vetettem.  Mi�rt nem adtad h�t oda p�nzemet a p�nzv�lt�knak, hogy megj�vet kamatostul kaptam volna vissza?  Ezzel a k�r�l�ll�khoz fordult: Vegy�tek el t�le a min�t, �s adj�tok oda annak, aki t�z min�t kapott.  Azok megjegyezt�k: Uram, neki m�r t�z min�ja van. -  Mondom nektek, hogy akinek van, az kap, akinek meg nincs, att�l azt is elveszik, amije van. 

[Offertorium]
!Ps 88:25
My faithfulness and My kindness shall be with him, and through My name shall his~
horn be exalted. ( Alleluia.)

[Secreta]
Grant, we beseech You, almighty God, that as blessed Louis, Your Confessor,~
spurning the delights of the world, labored earnestly to please Christ, the only~
King, so may his prayer make us pleasing to You.
$Per Dominum

[Communio]
!Matt 24:46-47
Blessed is that servant whom his master, when he comes, shall find watching.~
Amen I say to you, he will set him over all his goods. ( Alleluia.)

[Postcommunio]
O God, Who made Your blessed Confessor, Louis, wonderful on earth and glorious~
in heaven, make him, we beseech You, the defender of Your Church.
$Per Dominum
