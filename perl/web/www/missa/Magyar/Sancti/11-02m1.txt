[Rank]
Commemoratio Omnium Fidelium Defunctorum Ad primam Missam;;Duplex;;3;;ex C9

[RankNewcal]
Commemoratio Omnium Fidelium Defunctorum Ad primam Missam;;Duplex Solemnity;;6;;ex C9

[Rank1960]
Commemoratio Omnium Fidelium Defunctorum Ad primam Missam;;I. classis;;6;;ex C9

[Rule]
ex C9
no Gloria
no Credo
Prefatioo=Defunctorum
multiple3
Sequentia
Requiem gloria

[Oratio]
O God, Creator and Redeemer of all the faithful, grant to the souls of Your~
servants and handmaids the remission of all their sins, that they may obtain by~
our loving prayers the forgiveness which they have always desired.
$Qui vivis

[Lectio]
Olvasm�ny szent P�l apostol Korintusiakhoz irott els� level�b�l
!1 Cor. 15:51-57
v. Nos, titkot k�zl�k veletek: Nem halunk ugyan meg mindny�jan, de mindny�jan elv�ltozunk, hirtelen�l,  egy szempillant�s alatt, a v�gs� harsonasz�ra. Amikor az megsz�lal, a halottak felt�madnak a romlatlans�gra, mi pedig elv�ltozunk.  Ennek a romland� testnek fel kell �ltenie a romlatlans�got, ennek a haland�nak a halhatatlans�got.  Amikor a romland� mag�ra �lti a romlatlans�got, a haland� a halhatatlans�got, akkor teljesedik az �r�s szava: A gy�zelem elnyelte a hal�lt.  Hal�l, hol a te gy�zelmed? Hal�l, hol a te full�nkod?  A hal�l full�nkja a b�n, a b�n ereje pedig a t�rv�ny.  De legyen h�la Istennek, mert � gy�zelemre seg�t minket, J�zus Krisztus, a mi Urunk �ltal. 

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 5:25-29
v. Bizony, bizony, mondom nektek: El�rkezik az �ra, s m�r itt is van, amikor a halottak meghallj�k az Isten Fia szav�t. S akik meghallj�k, azok �lni fognak.  Amint ugyanis az Aty�nak �lete van �nmag�ban, a Fi�nak is megadta, hogy �lete legyen �nmag�ban,  s hatalmat adott neki, hogy �t�letet tartson, mert hiszen � az Emberfia.  Ne csod�lkozzatok rajta! Mert el�rkezik az �ra, amikor a s�rokban mindny�jan meghallj�k az Isten Fia szav�t, �s el�j�nnek.  Akik j�t tettek, az�rt, hogy felt�madjanak az �letre, akik gonoszat tettek, az�rt, hogy felt�madjanak a k�rhozatra. 

[Secreta]
Look with mercy, we beseech You, O Lord, upon the sacrificial gifts we offer for~
the souls of Your servants and handmaids, so that, to those upon whom You~
conferred the merits of Christian faith, You will also grant its reward.
$Per Dominum

[Postcommunio]
May the prayer of Your suppliants, O Lord, benefit the souls of Your servants~
and handmaids, that You may deliver them from all sins and make them sharers in~
Your redemption.
$Qui vivis

