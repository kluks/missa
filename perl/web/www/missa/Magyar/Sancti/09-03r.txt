[Rank]
S. Pius X Papae Confessoris;;Duplex;;3;;vide C4b

[Rule]
vide C4b
Gloria
CredoDA

[Introitus]
!Ps 88:20-22
v. I have raised up the chosen one from the people; with My holy oil I have~
anointed him, that My hand may be always with him, and that My arm may make him~
strong.
!Ps 88:2
The favors of the Lord I will sing forever; through all generations my mouth~
shall proclaim Your faithfulness.
&Gloria
v. I have raised up the chosen one from the people; with My holy oil I have~
anointed him, that My hand may be always with him, and that My arm may make him~
strong.

[Oratio]
O God Who, for the defense of the Catholic faith and the restoration of all~
things in Christ, filled St. Pius, the Supreme Pontiff, with heavenly wisdom and~
apostolic fearlessness, mercifully grant that, by following his teachings and~
examples, we may receive Your eternal rewards.
$Per eundem

[Lectio]
Olvasm�ny szent P�l apostol Tesszalonikaiakhoz irott els� level�b�l
!1 Thess. 2:2-8
v. J�llehet el�z�leg Filippiben szenved�s �s b�ntalom �rt benn�nket, amint tudj�tok, m�gis - b�zva Isten�nkben - v�llalni mert�k, hogy hirdetj�k nektek az Isten evang�lium�t, a sok neh�zs�g ellen�re is.  Buzd�t�sunk nem megt�veszt�sb�l, hamis sz�nd�kb�l vagy �lnoks�gb�l fakad,  hanem �gy besz�l�nk, mint akiket Isten alkalmasnak �t�lt az evang�lium hirdet�s�re. Nem is az embereknek igyeksz�nk tetszeni, hanem az Istennek, aki sz�v�nket meg�t�li.  Mint tudj�tok, nem volt szok�sunk h�zelegni, �s kapzsi sz�nd�k sem vezetett soha. Isten a tan�nk r�!  Emberi elismer�sre nem t�rekedt�nk, sem a ti�tekre, sem a m�sok�ra.  B�r mint Krisztus apostolai k�vetelm�nyekkel �llhattunk volna el�, m�gis olyan szel�den viselkedt�nk k�r�t�kben, mint a gyermekeit dajk�l� anya.  Annyira k�zel �lltatok sz�v�nkh�z, hogy nemcsak Isten evang�lium�t, hanem �let�nket is nektek akartuk adni. Ennyire megszerett�nk benneteket! 

[Graduale]
!Ps 39:10-11
I announced Your justice in the vast assembly; I did not restrain my lips as You, O~
Lord, know.
V. Your justice I kept not hid within my heart; Your faithfulness and Your~
salvation I have spoken of. Alleluia, alleluia.
!Ps 22:5-6 
V. You spread the table before me; You anoint my head with oil; my cup~
overflows. Alleluia.

[Tractus]
!Ps 131:16-18
Her priests I will clothe with salvation, and her faithful ones shall shout merrily for joy.
V. In her will I make a horn to sprout forth for David; I will place a lamp for My anointed.
V. His enemies I will clothe with shame, but upon him My crown shall shine.

[GradualeP]
Alleluia, alleluia.
!Ps 22:5-6
V. You spread the table before me; You anoint my head with oil; my cup~
overflows. Alleluia.
!Ps 25:8 O
V. Lord, I love the house in which You dwell, the tenting-place of Your glory.~
Alleluia.

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 21:15-17
v. Miut�n ettek, J�zus megk�rdezte Simon P�tert�l: "Simon, J�nos fia, jobban szeretsz engem, mint ezek?" "Igen, Uram - felelte -, tudod, hogy szeretlek." Erre �gy sz�lt hozz�: "Legeltesd b�r�nyaimat!"  Azt�n m�sodszor is megk�rdezte t�le: "Simon, J�nos fia, szeretsz engem?" "Igen, Uram - v�laszolta -, tudod, hogy szeretlek." Erre azt mondta neki: "Legeltesd juhaimat!"  Majd harmadszor is megk�rdezte t�le: "Simon, J�nos fia, szeretsz?" P�ter elszomorodott, hogy harmadszor is megk�rdezte: "Szeretsz engem?" S �gy v�laszolt: "Uram, te mindent tudsz, azt is tudod, hogy szeretlek." J�zus ism�t azt mondta: "Legeltesd juhaimat!" 

[Offertorium]
!Ps 33:12
Come, children, hear me; I will teach you the fear of the Lord.

[Secreta]
Having graciously accepted our offerings, we beseech You, O Lord, that we may~
ever treat this divine sacrament with sincere veneration and receive it with~
faithful heart, through the prayers of St. Pius, Your Supreme Pontiff.
$Per Dominum

[Communio]
!John 6:56-57
My Flesh is food indeed, and My Blood is drink indeed. He who eats My Flesh and~
drinks My Blood, abides in Me and I in him.

[Postcommunio]
We, who have been filled with strength at the heavenly table, beseech You, O~
Lord, our God, that by the intercession of St. Pius, Supreme Pontiff, we may be~
made firm in faith and of one mind in Your love.
$Per Dominum
