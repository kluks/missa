[Rank]
S. Joannis Cantii Confessoris;;Duplex;;3;;vide C5

[RankNewcal]
S. Joannis Cantii Confessoris;;Duplex optional;;2;;vide C5

[Rule]
vide C5;
Gloria

[Introitus]
!Sir 18:12-13
v. Man may be merciful to his fellow man, but God�s mercy reaches all flesh. He~
has mercy, teaches and guides, as a shepherd does his flock.
!Ps 1:1
Happy the man who follows not the counsel of the wicked nor walks in the way of~
sinners, nor sits in the company of the insolent.
&Gloria
v. Man may be merciful to his fellow man, but God�s mercy reaches all flesh. He~
has mercy, teaches and guides, as a shepherd does his flock.

[Oratio]
Grant, we beseech You, almighty God, that by following the example of blessed~
John, Your Confessor, we may advance in a knowledge of holiness and, by showing~
pity for others, obtain Your forgiveness through his merits
$Per Dominum

[Lectio]
Olvasm�ny szent Jakab apostol level�b�l
!Jas 2:12-17
v. �gy besz�ljetek �s �gy cselekedjetek, mint akik f�l�tt majd a szabads�g t�rv�nye szerint �t�lkeznek.  Mert az �t�let irgalmatlanul les�jt arra, aki nem irgalmas. �m az irgalom gy�zelmet arat az �t�let f�l�tt.  Testv�reim, mit haszn�l, ha valaki azt �ll�tja, hogy van hite, tettei azonban nincsenek? �dv�z�theti a hite?  Ha valamelyik testv�rnek nincs ruh�ja �s nincs meg a mindennapi t�pl�l�ka,  �s egyiketek �gy sz�lna hozz�: "Menj b�k�ben, melegedj, �s lakj�l j�l!", de nem adn�tok meg neki, amire test�nek sz�ks�ge van, mit haszn�lna?  Ugyan�gy a hit is, ha tettei nincsenek, mag�ban holt dolog. 

[Graduale]
!Ps 106:8-9
Let them give thanks to the Lord for His kindness and His wondrous deeds to the~
children of men.
V. Because He satisfied the longing soul and filled the hungry soul with good~
things. Alleluia, alleluia.
!Prov 31:20
V. He extends his arms to the needy, and reaches out his hands to the poor.~
Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 12:35-40
v. Cs�p�t�k legyen fel�vezve �s �gjen a l�mp�sotok.  Hasonl�tsatok azokhoz az emberekhez, akik urukra v�rnak, hogy mihelyt meg�rkezik a menyegz�r�l �s z�rget, r�gt�n ajt�t nyissanak neki.  Boldogok azok a szolg�k, akiket uruk meg�rkez�sekor �bren tal�l. Bizony mondom nektek, fel�vezi mag�t, asztalhoz �lteti �ket, �s megy, hogy kiszolg�lja �ket.  �s ha a m�sodik vagy a harmadik �rv�lt�skor �rkezve is �gy tal�lja �ket, boldogok azok a szolg�k.  Gondolj�tok meg: ha tudn� a h�zigazda, hogy melyik �r�ban j�n a tolvaj, nem engedn� bet�rni h�z�ba.  Legyetek ti is k�szen, mert olyan �r�ban, amikor nem is gondolj�tok, elj�n az Emberfia." 

[Offertorium]
!Job 29:14-16
I wore my honesty like a garment; justice was my robe and my turban. I was eyes~
to the blind, and feet to the lame was I; I was a father to the needy.

[Secreta]
Graciously accept these sacrificial gifts, we beseech You, O Lord, through the~
merits of blessed John, Your Confessor, and grant that, loving You above all~
things and all mankind because of You, we may be pleasing to You in heart and in~
deed.
$Per Dominum

[Communio]
!Luke 6:38
Give, and it shall be given to you; good measure, pressed down, shaken together,~
running over, shall they pour into your lap.

[Postcommunio]
Filled with the good food of Your precious Body and Blood, we humbly implore~
Your clemency, O Lord, that by the merits and example of blessed John, Your~
Confessor, we may imitate his charity and share in his glory.
$Qui vivis

