[Rank]
In Conceptione Immaculata Beatae Mariae Virginis;;Duplex I classis cum octava communi;;6.5;;ex C11

[Rule]
ex C11;
Gloria
Credo
Prefatio=Maria=Concepti�ne immacul�ta;

[Introitus]
!Isa 61:10
v. I will heartily rejoice in the Lord, in my God is the joy of my soul; for He~
has clothed me with a robe of salvation, and wrapped me in a mantle of justice,~
like a bride bedecked with her jewels.
!Ps 84:2
I will extol You, O Lord, for You drew me clear and did not let my enemies~
rejoice over me.
&Gloria
v. I will heartily rejoice in the Lord, in my God is the joy of my soul; for He~
has clothed me with a robe of salvation, and wrapped me in a mantle of justice,~
like a bride bedecked with her jewels.

[Oratio]
O God, Who by the Immaculate Conception of the Virgin, prepared a worthy~
dwelling for Your Son, and Who, by Your Son�s death, foreseen by You, preserved~
her from all taint, grant, we beseech You, through her intercession, that we too~
may come to You unstained by sin.
$Per Dominum

[Lectio]
Olvasm�ny a P�ldabesz�dek k�nyv�b�l
!Prov 8:22-35
v. "Alkot� munk�ja elej�n teremtett az �r, �sid�kt�l fogva, mint legels� m�v�t.  Az id�k el�tt alkotott, a kezdet kezdet�n, a f�ld sz�let�se el�tt.  Amikor l�trehozott, m�g �svizek sem voltak, �s a forr�sokb�l m�g nem t�rt el� v�z.  Miel�tt a hegyek keletkeztek volna, kor�bban h�vott l�tre, mint a halmokat,  amikor a f�ldet �s a mez�ket m�g nem alkotta meg, �s a f�ld els� r�g�t sem.  Ott voltam, amikor az eget teremtette, s az �sv�z sz�n�re a k�rt megvonta,  amikor a felh�ket f�ler�s�tette, s az �sforr�sok erej�t megszabta;  amikor kijel�lte a tenger hat�r�t - �s a vizek nem csaptak ki -, amikor megrajzolta a f�ld szil�rd r�sz�t.  Ott voltam mellette mint a kedvence, napr�l napra csak bennem gy�ny�rk�d�tt, mindig ott j�tszottam a sz�ne el�tt.  Ott j�tszottam az eg�sz f�ldkereks�gen, s �r�mmel voltam az emberek fiai k�z�tt."  "Nos h�t, fiaim, hallgassatok r�m! J�l j�rnak, akik megtartj�k �tjaimat.  Hallj�tok meg int� szavamat: legyetek b�lcsek, s ne tagadj�tok meg t�le a figyelmet!  Boldog ember, aki hallgat r�m, aki napr�l napra �rk�dik ajt�mn�l, s �rzi kapuim f�lf�it.  Aki megtal�l, az �letet tal�l, �s elnyeri az �rnak tetsz�s�t. 

[Graduale]
!Judith 13:23
Blessed are you, O Virgin Mary, by the Lord the most high God, above all women~
upon the earth.
!Judith 15:10 
V. You are the glory of Jerusalem, you are the joy of Israel, you~
are the honor of our people. Alleluia, alleluia.
!Cant. 4:7 
V. You are all-beautiful, O Mary, and there is in you no stain of~
original sin. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 1:26-28
v. A hatodik h�napban az Isten elk�ldte G�bor angyalt Galilea N�z�ret nev� v�ros�ba  egy sz�zh�z, aki egy D�vid h�z�b�l val� f�rfinak, J�zsefnek volt a jegyese, �s M�ri�nak h�vt�k.  Az angyal bel�pett hozz� �s megsz�l�totta: "�dv�zl�gy, kegyelemmel teljes! Veled van az �r! �ldottabb vagy minden asszonyn�l." 

[Offertorium]
!Luke 1:28
Hail Mary full of grace, the Lord is with you; blessed are you among women.~
Alleluia.

[Secreta]
Accept, O Lord, the sacrifice of salvation which we offer You on the feast of~
the Immaculate Conception of the blessed Virgin Mary; and grant that as we~
profess that she was kept from all taint of evil, by Your anticipating grace, so,~
through her intercession, may we be freed from all sin.
$Per Dominum

[Communio]
!  
Glorious things are said of you, O Mary, for He Who is mighty has done great~
things for you.

[Postcommunio]
May the sacrament we have received, O Lord our God, heal in us the wounds of~
that sin from which by a singular privilege, You kept immaculate the conception~
of blessed Mary.
$Per Dominum
