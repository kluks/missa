[Rank]
Beatae Mariae Virginis Guadalupae;;Duplex;;3;;vide C11

[RankNewcal]
Beatae Mariae Virginis Guadalupae;;Duplex optional;;2.5;;vide C11

[Rule]
vide C11;
Gloria
Prefatio=Mariae=Festivitate

[Introitus]
!Ps 91:13-14
v. Hail, holy Mother, who brought forth the King Who rules heaven and earth~
world without end.
!Ps 44:2
My heart overflows with a goodly theme; as I sing my ode to the King.
&Gloria
v. Hail, holy Mother, who brought forth the King Who rules heaven and earth~
world without end.

[Oratio]
O God, Who willed that, under the special patronage of the Blessed Virgin Mary,~
we be laden with perpetual favors, grant to Your suppliants that, as we this day~
rejoice in her commemoration on earth, we may enjoy the vision of her in heaven.
$Per Dominum

[Lectio]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Ecclus 24:23-31
v. �n mint a sz�ll�t� az illat gy�ny�r�s�g�t termettem, �s vir�gaimb�l d�szes �s becses gy�m�lcs lett.  �n a sz�p szeretet anyja vagyok, �s a f�lelem� �s az ismeret� �s a szent rem�nys�g�.  N�lam az �t �s igazs�g minden kegyelme, n�lam az �let �s er�ny minden rem�nys�ge.  J�jetek �t hozz�m mindny�jan, kik engem k�v�ntok, �s gy�m�lcseimb�l teljetek el.  Mert az �n lelkem �desebb a m�zn�l, �s �r�ks�gem jobb a sz�nm�zn�l �s a l�pes m�zn�l.  Az �n eml�kezetem �l minden id�k nemzed�keiben.  A kik engem esznek, m�gink�bb �heznek; �s a kik engem isznak, m�gink�bb szomjaznak.  A ki engem hallgat, nem sz�gyen�l meg, �s a kik �ltalam munk�lkodnak, nem v�tkeznek.  A kik f�nyt der�tenek r�m, �r�k �letet nyernek. 

[Graduale]
!Cant 6:9
Who is this that comes forth like the dawn, as beautiful as the moon, as~
resplendent as the sun?
!Ecclus 50:8
V. Like the rainbow appearing in the bright clouds, and as the flower of roses~
in springtime. Alleluia, alleluia.
!Cant 2:12
V. The flowers appear in our land, the time of pruning has come. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 1:39-47
v. M�ria m�g ezekben a napokban �tnak indult, �s a hegyekbe sietett, J�da v�ros�ba.  Zakari�s h�z�ba t�rt be �s �dv�z�lte Erzs�betet.  Amikor Erzs�bet meghallotta M�ria k�sz�nt�s�t, �r�m�ben megmozdult m�h�ben a gyermek, maga Erzs�bet pedig eltelt Szentl�lekkel.  Nagy sz�val felki�ltott: "�ldott vagy az asszonyok k�z�tt, �s �ldott a te m�hed gy�m�lcse!  Hogy lehet az, hogy Uramnak anyja j�n hozz�m?  L�sd, mihelyt meghallottam k�sz�nt�sed szav�t, az �r�mt�l megmozdult m�hemben a gyermek.  Boldog, aki hitt annak a beteljesed�s�ben, amit az �r mondott neki!"  M�ria �gy sz�lt: "Lelkem magasztalja az Urat,  �s sz�vem ujjong megv�lt� Istenemben, 

[Offertorium]
!2 Par. 7:16
I have chosen, and have sanctified this place, that My name may be there, and My~
eyes and My heart may remain there forevermore.

[Secreta]
By Your clemency, O Lord, and the intercession of blessed Mary, ever Virgin, may~
this oblation profit us unto eternal and also present well-being and peace.
$Per Dominum

[Communio]
!Ps 147:20
He has not done thus for any other nation; His ordinances he has not made known~
to them.

[Postcommunio]
Having received the aids of our everlasting salvation, O Lord, we beseech You,~
grant us to be everywhere protected by the patronage of blessed Mary, ever~
Virgin, in veneration of whom we have made these offerings to Your majesty.
$Per Dominum
