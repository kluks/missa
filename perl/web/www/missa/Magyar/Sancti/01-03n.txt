[Rank]
The Most Holy Name of Jesus;;Duplex optional;;2;;

[Rule]
Gloria
Credo
Prefatio=Nat

[Introitus]
!Phil 2:10-11
v. At the Name of Jesus every knee should bend of those in heaven, on earth, and~
under the earth, and every tongue should confess that the Lord Jesus Christ is~
in the glory of God the Father.
!Ps 8:2 O
Lord, our Lord, how glorious is Your Name over all the earth.
&Gloria
v. At the Name of Jesus every knee should bend of those in heaven, on earth, and~
under the earth, and every tongue should confess that the Lord Jesus Christ is~
in the glory of God the Father.

[Oratio]
O God, You Who appointed Your only-begotten Son to be the Savior of the human~
race, and commanded that He be called Jesus, mercifully grant that we may enjoy~
in heaven the vision of Him Whose holy Name we venerate on earth.
$Per eumdem

[Lectio]
Olvasm�ny az Apostolok Cselekedeib�l
!Acts 4:8 - 12.
v. P�ter a Szentl�lekt�l eltelve �gy v�laszolt: "N�p�nk el�lj�r�i �s ti v�nek!  Ha az�rt vallattok ma minket, ami�rt egy nyomor�kkal j�t tett�nk, hogy vajon hogyan is gy�gyult meg,  h�t tudj�tok meg mindannyian, ti �s Izrael eg�sz n�pe, hogy annak a n�z�reti J�zus Krisztusnak a nev�ben, akit ti keresztre fesz�tettetek, �s akit az Isten felt�masztott a hal�lb�l. Az � nev�ben �ll itt el�ttetek eg�szs�gesen.  � az a k�, amelyet ti, az �p�t�k elvetettetek, m�gis szegletk�v� lett.  Nincs �dv�ss�g senki m�sban. Mert nem adatott m�s n�v az �g alatt az embereknek, amelyben �dv�z�lhetn�nk." 

[Graduale]
!Ps 105:47
Save us, O Lord, our God, and gather us from among the nations, that we may give~
thanks to Your holy Name and glory in praising You.
!Isa. 63:16
You, O Lord, are our Father and our Redeemer, from everlasting is Your Name.~
Alleluia, alleluia.
!Ps 144:21
May my mouth speak the praise of the Lord, and may all flesh bless His holy~
Name. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 2:21
v. Amikor eltelt a nyolc nap �s k�r�lmet�lt�k, a J�zus nevet adt�k neki, ahogy az angyal nevezte, miel�tt m�g a m�hben megfogamzott volna. 

[Offertorium]
!Ps 85:12, 5.
I will give thanks to You, O Lord my God, with all my heart, and I will glorify~
Your Name forever. For You, O Lord, are good and forgiving, abounding in~
kindness to all who call upon You. Alleluia.

[Secreta]
May Your blessing, O most merciful God, which makes all creation flourish,~
sanctify this our sacrifice, which we offer You to the glory of the Name of Your~
Son, our Lord Jesus Christ, and may it be pleasing to Your majesty as an act of~
praise and be profitable to us for our salvation.
$Per eumdem

[Communio]
!Ps 85:9-10
All the nations You have made shall come and worship You, O Lord, and glorify~
Your Name. For You are great, and do wondrous deeds; You alone are God.~
Alleluia.

[Postcommunio]
Almighty, eternal God, You Who have created and redeemed us, graciously look~
upon our needs, and deign to receive with kind and favorable countenance the~
sacrifice of the Saving Victim, which we have offered to Your majesty, in honor~
of the Name of Your Son, our Lord Jesus Christ, that, Your grace being poured~
out upon us, we may have the joy of seeing our names written in heaven, under~
the glorious Name of Jesus, the sign of eternal predestination.
$Per eumdem
