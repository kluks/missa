[Rank]
Festum Immaculati Cordis Beatae Mariae Virginis;;Duplex II classis;;5;;ex C11

[Rule]
ex C11;
Gloria
Credo
Prefatio=Maria=Assumptione;

[Introitus]
!Heb 4:16
v. Let us draw near with confidence to the throne of grace, that we may obtain~
mercy and find grace to help in time of need.
!Ps 44:2
My Heart overflows with a goodly theme; as I sing my ode to the King.
&Gloria
v. Let us draw near with confidence to the throne of grace, that we may obtain~
mercy and find grace to help in time of need.

[Oratio]
Almighty, everlasting God, Who in the Heart of the Blessed Virgin Mary prepared a~
dwelling place worthy of the Holy Spirit; graciously grant that we, who are~
devoutly keeping the feast of her Immaculate Heart, may be able to live~
according to Your Heart.
$Per eundem

[Commemoratio Oratio]
!For ST. TIMOTHY and COMPANIONS
Mercifully give us Your help, we beseech You, O Lord, and by the intercession of~
Your blessed Martyrs, Timothy, Hippolytus, and Symphorian, stretch over us the~
right hand of Your mercy.
$Per Dominum

[Lectio]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Sir 24:23-31
v. �n mint a sz�ll�t� az illat gy�ny�r�s�g�t termettem, �s vir�gaimb�l d�szes �s becses gy�m�lcs lett.  �n a sz�p szeretet anyja vagyok, �s a f�lelem� �s az ismeret� �s a szent rem�nys�g�.  N�lam az �t �s igazs�g minden kegyelme, n�lam az �let �s er�ny minden rem�nys�ge.  J�jetek �t hozz�m mindny�jan, kik engem k�v�ntok, �s gy�m�lcseimb�l teljetek el.  Mert az �n lelkem �desebb a m�zn�l, �s �r�ks�gem jobb a sz�nm�zn�l �s a l�pes m�zn�l.  Az �n eml�kezetem �l minden id�k nemzed�keiben.  A kik engem esznek, m�gink�bb �heznek; �s a kik engem isznak, m�gink�bb szomjaznak.  A ki engem hallgat, nem sz�gyen�l meg, �s a kik �ltalam munk�lkodnak, nem v�tkeznek.  A kik f�nyt der�tenek r�m, �r�k �letet nyernek. 

[Graduale]
!Ps 12:6
Let my Heart rejoice in Your salvation; let me sing of the Lord, He has been~
good to me. Yes, I will sing to the name of the Lord, Most High.
!Ps 44:18
V. They shall remember Your name throughout all generations; therefore shall~
nations praise You forever and ever. Alleluia, alleluia.
!Luke 1:46-47
V. My soul magnifies the Lord, and my spirit rejoices in God my Savior.~
Alleluia.

[Tractus]
!Prov 8:32-35
So now, O children, listen to me; happy those who keep my ways. Hear instruction, and be wise, and refuse it not.
V. Happy the man who obeys me, watching daily at my gates, waiting at my doorposts.
V. For he who finds me finds life, and wins favor from the Lord.

[GradualeP]
Alleluia, alleluia.
!Luke 1:46-48
V. My soul magnifies the Lord, and my spirit rejoices in God my Savior,~
Alleluia.
V. All generations shall call me blessed, because God has regarded His humble~
handmaid. Alleluia.

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 19:25-27
v. J�zus keresztje alatt ott �llt anyja, anyj�nak n�v�re, M�ria, aki Kleof�s feles�ge volt �s M�ria Magdolna.  Amikor J�zus l�tta, hogy ott �ll az anyja �s szeretett tan�tv�nya, �gy sz�lt anyj�hoz: "Asszony, n�zd, a fiad!"  Azt�n a tan�tv�nyhoz fordult: "N�zd, az any�d!" Att�l az �r�t�l fogva h�z�ba fogadta a tan�tv�ny. 

[Offertorium]
!Luke 1:47, 49.
My spirit rejoices in God my Savior; because He Who is mighty has done great~
things for me, and holy is His name.

[Secreta]
Offering the spotless Lamb to Your Majesty, O Lord, we pray that our hearts may~
be set aflame by the fire that burned so wondrously in the Heart of the Blessed~
Virgin Mary.
$Per eundem

[Commemoratio Secreta]
!For ST. TIMOTHY and COMPANIONS
Accept, O Lord, the offering which Your consecrated people present to You in~
honor of Your Saints, whose merits have helped them in their trials, as well~
they know.
$Per Dominum

[Communio]
!John 19:27
Jesus said to His Mother, Woman, behold your son. Then He said to the disciple,~
Behold your mother. And from that hour the disciple took her into his home.

[Postcommunio]
Refreshed by divine gifts, we humbly beseech You, O Lord, that by the~
intercession of the Blessed Virgin Mary, the solemn feast of whose Immaculate~
Heart we have celebrated, we may be freed from present dangers and reach the~
happiness of life eternal.
$Per Dominum

[Commemoratio Postcommunio]
!For ST. TIMOTHY and COMPANIONS
Filled with the abundance of the divine sacrament, we beseech You, O Lord our~
God, that, by the prayers of Your holy Martyrs, Timothy, Hippolytus and~
Symphorian, we may ever partake of this gift during life.
$Per Dominum
