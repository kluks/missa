[Rank]
S. Hedwigis Viduae;;Semiduplex;;2;;vide C7a

[Rule]
vide C7a
Gloria

[Introitus]
!Ps 118:75; 118:120
v. I know, O Lord, that Your ordinances are just, and in Your faithfulness You~
have afflicted me. Pierce my flesh with Your fear; I fear Your ordinances.
!Ps 118:1
Happy are they whose way is blameless, who walk in the law of the Lord.
&Gloria
v. I know, O Lord, that Your ordinances are just, and in Your faithfulness You~
have afflicted me. Pierce my flesh with Your fear; I fear Your ordinances.

[Oratio]
O God, Who taught blessed Hedwig to forsake worldly vanities that she might with~
her whole heart humbly follow Your cross, grant that, through her merits and~
example, we may learn to renounce the perishable delights of the world and, by~
embracing Your cross, overcome all things opposed to us.
$Qui vivis

[Lectio]
Olvasm�ny a P�ldabesz�dek k�nyv�b�l
!Prov 31:10-31
v. Der�k asszonyt ki tal�l? �rt�ke a gy�ngy�t messze meghaladja.  F�rje eg�sz sz�v�vel r�hagyatkozik, �s nem j�r rosszul vele.  Mindig a jav�ra van, sose a k�r�ra, �let�nek minden napj�n.  El�teremti a gyapj�t �s a kendert, �s mindent beszerez, �gyes k�zzel.  Olyan, mint a kalm�rok haj�ja: messze f�ldr�l sz�ll�tja az �lelmet.  B�r m�g �jszaka van, m�r f�lkel, �s ell�tja a h�za n�p�t eledellel, szolg�l�l�nyait egy eg�sz napra val�val.  Sz�nt�f�ld ut�n n�z �s meg is szerzi, sz�l�t telep�t a keze munk�j�b�l.  J�l k�r�lk�ti derek�t az �v�vel, �s nekifesz�ti karj�t a munk�nak.  Tudja, hogy j� hasznot hajt, �jnek idej�n sem alszik ki l�mp�ja.  Kiny�jtja a karj�t a guzsaly ut�n, �s a kez�vel megfogja az ors�t.  Kiterjeszti kez�t az elnyomott f�l�, a sz�k�lk�d�nek meg a karj�t ny�jtja.  Nem kell f�ltenie h�za n�p�t a h�t�l: eg�sz h�za n�p�t ell�tta kett�s ruh�val,  �s takar�kat is csin�lt mag�nak. Ruh�ja b�rsonyos len �s b�bor gyapj�.  F�rj�t a kapukn�l igen nagyra tartj�k, hogyha tan�csban �l az orsz�g v�neivel.  Ingeket is k�sz�t �s eladja �ket, �veket is �rul a keresked�nek.  Az er� �rzete veszi k�r�l, vid�man n�z a j�vend� el�.  Ha kinyitja a sz�j�t, b�lcsen besz�l, j�s�gos tan�t�s �rad a nyelv�r�l.  Szemmel tartja h�za n�p�nek munk�j�t, a t�tlen lustas�g nem kenyere.  Ki�llnak fiai, s boldognak hirdetik, a f�rje is f�lkel �s �gy dics�ri:  "Sok asszony megmutatta, milyen der�k, de te fel�lm�lod �ket, mind valamennyit!"  Csalfa dolog a b�j, a sz�ps�g muland�, de az okos asszonynak kij�r a dics�ret.  Magasztalj�tok keze munk�j��rt, a v�roskapukn�l dics�rj�k tetteit! 

[Graduale]
!Ps 44:3, 5.
Grace is poured out upon your lips; thus God has blessed you forever.
V. In the cause of truth and mercy and for the sake of justice: may your right~
hand show you wondrous deeds. Alleluia, alleluia.
!Ps 44.5
V. In your splendor and your beauty ride on triumphant, and reign. Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 13:44-52
v. "A mennyek orsz�ga hasonl�t a sz�nt�f�ldben elrejtett kincshez. Amikor egy ember megtal�lta, �jra elrejtette, azt�n �r�m�ben elment, eladta minden�t, amije csak volt, �s megvette a sz�nt�f�ldet.  A mennyek orsz�ga hasonl�t a keresked�h�z is, aki igazgy�ngy�t keresett.  Amikor egy nagyon �rt�keset tal�lt, fogta mag�t, eladta minden�t, amije csak volt �s megvette.  V�g�l hasonl�t a mennyek orsz�ga a tengerbe vetett h�l�hoz, amely mindenf�le halat �sszefog.  Amikor megtelik, partra h�zz�k, neki�lnek, �s a jav�t ed�nyekbe v�logatj�k, a hitv�ny�t pedig kisz�rj�k.  �gy lesz a vil�g v�g�n is: elmennek az angyalok, azt�n kiv�logatj�k a gonoszokat az igazak k�z�l,  �s t�zes kemenc�be vetik �ket. Ott s�r�s �s fogcsikorgat�s lesz."  "Meg�rtett�tek mindezeket?" "Igen" - felelt�k.  Erre �gy folytatta: "Minden �r�stud�, aki j�rtas a mennyek orsz�g�nak tan�t�s�ban, hasonl�t a h�zigazd�hoz, aki kincseib�l �jat �s r�git hoz el�." 

[Offertorium]
!Ps 44:3
Grace is poured out upon your lips; thus God has blessed you forever, and for~
ages of ages.

[Secreta]
O Lord, may the offerings of Your devoted people be pleasing to You in honor of~
Your saints, through whose merits they know they have received help in time of~
trial.
$Per Dominum

[Communio]
!Ps 44:8
You love justice and hate wickedness; therefore God, your God, has anointed you~
with the oil of gladness above your fellows.

[Postcommunio]
You have filled Your people, O Lord, with sacred gifts; ever comfort us, we~
beseech You, by the intercession of her whose festival we are celebrating.
$Per Dominum
