[Rank]
Omnium Sanctorum;;Duplex I classis cum octava communi;;7;;

[Rule]
9 lectiones
Gloria
Credo

[Introitus]
!
v. Let us all rejoice in the Lord, celebrating a feast day in honor of all the~
Saints, on whose solemnity the angels rejoice, and join in praising the Son of~
God.
!Ps 32:1
Exult, you just, in the Lord; praise from the upright if fitting.
&Gloria
v. Let us all rejoice in the Lord, celebrating a feast day in honor of all the~
Saints, on whose solemnity the angels rejoice, and join in praising the Son of~
God.

[Oratio]
Almighty, eternal God, Who granted us to honor the merits of all Your Saints in a~
single solemn festival, bestow on us, we beseech You, through their manifold~
intercession, that abundance of Your mercy for which we yearn.
$Per Dominum

[Lectio]
Olvasm�ny a Jelen�sek k�nyv�b�l
!Apoc 7:2-12
v. Majd l�ttam, hogy napkeletr�l egy m�sik angyal sz�ll fel, az �l� Isten pecs�tje volt n�la. Nagy hangon ki�ltott a n�gy angyalnak, akiknek hatalmuk volt r�, hogy �rtsanak a f�ldnek �s a tengernek:  "Ne �rtsatok se a f�ldnek, se a tengernek, se a f�knak, m�g meg nem jel�lj�k homlokukon Isten�nk szolg�it!"  Ekkor hallottam a megjel�ltek sz�m�t: sz�znegyvenn�gyezren voltak Izrael fiainak minden t�rzs�b�l:  J�da t�rzs�b�l tizenk�tezer, Ruben t�rzs�b�l tizenk�tezer, G�d t�rzs�b�l tizenk�tezer,  �ser t�rzs�b�l tizenk�tezer, Naftali t�rzs�b�l tizenk�tezer, Manassze t�rzs�b�l tizenk�tezer,  Simeon t�rzs�b�l tizenk�tezer, L�vi t�rzs�b�l tizenk�tezer, Isszach�r t�rzs�b�l tizenk�tezer,  Zebulun t�rzs�b�l tizenk�tezer, J�zsef t�rzs�b�l tizenk�tezer, Benjamin t�rzs�b�l tizenk�tezer.  Ezut�n akkora sereget l�ttam, hogy meg sem lehetett sz�molni. Minden nemzetb�l, t�rzsb�l, n�pb�l �s nyelvb�l �lltak a tr�n �s a B�r�ny el�tt, feh�r ruh�ba �lt�zve, kez�kben p�lma�g.  Nagy sz�val ki�ltott�k �s mondt�k: "�dv Isten�nknek, aki a tr�non �l �s a B�r�nynak!"  Az angyalok mind a tr�n, a v�nek �s a n�gy �l�l�ny k�r�l �lltak, arcra borultak a tr�n el�tt, �s im�dt�k az Istent,  mondv�n: "Amen, �ld�s, dics�s�g, b�lcsess�g, h�la, tisztelet, hatalom �s er� a mi Isten�nknek �r�kk�n-�r�kk�! Amen." 

[Graduale]
!Ps 33:10-11
Fear the Lord, you His holy ones, for nought is lacking to those who fear Him.
V. But those who seek the Lord want for no good thing. Alleluia, alleluia.
!Matt. 11:28
Come to Me, all you who labor and are burdened, and I will give you rest.~
Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 5:1-12
v. A t�meg l�tt�ra f�lment a hegyre �s le�lt. Tan�tv�nyai k�r�je gy�ltek,  � pedig sz�l�sra nyitotta ajk�t. �gy tan�totta �ket:  "Boldogok a l�lekben szeg�nyek, mert �v�k a mennyek orsz�ga.  Boldogok, akik szomor�ak, mert majd megvigasztalj�k �ket.  Boldogok a szel�dek, mert �v�k lesz a f�ld.  Boldogok, akik �hezik �s szomjazz�k az igazs�got, mert majd eltelnek vele.  Boldogok az irgalmasok, mert majd nekik is irgalmaznak.  Boldogok a tiszta sz�v�ek, mert megl�tj�k az Istent.  Boldogok a b�kess�gben �l�k, mert Isten fiainak h�vj�k majd �ket.  Boldogok, akik �ld�z�st szenvednek az igazs�g�rt, mert �v�k a mennyek orsz�ga.  Boldogok vagytok, ha miattam gyal�znak �s �ld�znek benneteket �s hazudozva minden rosszat r�tok fognak �nmiattam.  �r�ljetek �s ujjongjatok, mert nagy lesz a mennyben a jutalmatok! �gy �ld�zt�k el�ttetek a pr�f�t�kat is. 

[Offertorium]
!Wis 3:1-3
The souls of the just are in the hand of God, and no torment shall touch them.~
They seemed, in the view of the foolish, to be dead: but they are in peace.~
Alleluia.

[Secreta]
We offer You, O Lord, the gifts of our service; may they be pleasing to You for~
the honor of Your just ones and, through Your mercy, bring us salvation.
$Per Dominum

[Communio]
!Matt 5:8-10
Blessed are the clean of heart, for they shall see God. Blessed are the~
peacemakers, for they shall be called children of God. Blessed are they who~
suffer persecution for justice� sake, for theirs is the kingdom of heaven.

[Postcommunio]
Grant Your faithful people, we beseech You, O Lord, ever to rejoice in the~
veneration of all the Saints, and to be protected by their unceasing prayers.
$Per Dominum
