[Rank]
St Joseph the Worker Confessoris.;;Duplex I classis;;6;;

[RankNewcal]
St Joseph the Worker Confessoris.;;Duplex optional classis;;2;;

[Rule]
proper
Gloria
Credo
Prefatio=Joseph

[Introitus]
!Ps 32:20-21
v. Our soul waiteth for the Lord: for he is our helper and protector. For in him our heart shall rejoice: and in his holy name we have trusted, allel�ja, allel�ja
!Ps 79:2
v. O thou that rulest Israel: thou that leadest Joseph like a sheep. Thou that sittest upon the cherubims, shine forth, allel�ja, allel�ja

[Oratio]
God, Who in thine unspeakable foreknowledge didst choose thy blessed servant~
Joseph to be the husband of thine Own most holy Mother; mercifully grant that~
now that he is in heaven with thee, we who on earth do reverence him for our~
Defender, may worthily be holpen by the succour of his prayers to thee on our~
behalf;
$Qui vivis

[Lectio]
Olvasm�ny M�zes els� k�nyv�b�l
!Gen 49:22-26
v. J�zsef fiatal gy�m�lcsfa a forr�s mellett, �gai felny�lnak a falak f�l�.  �j�szok keser�s�get okoztak neki, c�lbavett�k �s vesz�lybe sodort�k.  De egy er�s sz�tt�rte �jukat. J�kob er�s�nek keze, Izrael szikl�j�nak neve sz�tz�zta karjuk inait,  aty�id Istene, aki seg�ts�get k�ld neked, a mindenhat� Isten, aki meg�ld t�ged fel�lr�l, az �g �ld�s�nak b�s�g�vel �s az alant elter�l� m�lys�g �ld�s�nak b�s�g�vel. A kebel �s az anyam�h �ld�s�nak b�s�g�vel,  a kal�sz �s a vir�g �ld�s�nak b�s�g�vel, az �r�k hegyek, az �si halmok �ld�s�nak b�s�g�vel. Ez mind sz�lljon J�zsef fej�re, a testv�rei k�z�l kiv�lasztott fej�nek tetej�re. 

[Graduale]
Allel�ja, allel�ja.
v. In whatever tribulation they shall cry to me, I will hear them and be their protector always. Allel�ja.
V. Obtain to us, O Joseph, to lead an innocent life; and may it ever be safe through thy patronage. Allel�ja.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 3:21-23
v. Ekkor t�rt�nt, hogy amikor m�r az eg�sz n�p megkeresztelkedett �s J�zus is f�lvette a kereszts�get, �s amikor im�dkozott, megny�lt az �g,  �s a Szentl�lek galamb alakj�ban lesz�llt r�. Sz�zat is hallatszott az �gb�l: "Te vagy az �n szeretett Fiam, benned telik kedvem."  F�ll�p�sekor J�zus mintegy harminc�ves volt. Azt tartott�k r�la, hogy J�zsefnek a fia, aki �li fia volt, 

[Offertorium]
!Ps 147:12; 147:13
v. Praise the Lord, O Jerusalem: praise thy God, O Sion. Because he hath strengthened the bolts of thy gates, he hath blessed thy children within thee, allel�ja, allel�ja.

[Secreta]
Supported by the patronage of the spouse of thy most holy Mother, we beseech thy clemency, O Lord, that thou wouldst make our hearts despise all eartly things and love thee, the true God with perfect charity.
$Qui vivis

[Communio]
!Matt 1:16
v. And Jacob begot Joseph the husband of Mary, of whom was born Jesus, who is called Christ, allel�ja, allel�ja.

[Postcommunio]
Refreshed at the fount of divine blessing, we beseech thee, O Lord our God, that as thou dost gladden us by the protection of blessed Joseph, so by his merits and intercession thou woulds make us partakers of his heavenly glory.
$Per Dominum
