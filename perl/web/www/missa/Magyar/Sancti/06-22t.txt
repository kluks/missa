[Rank]
S. Paulini episcopi et Confessoris;;Duplex;;3;;vide C4

[RankNewcal]
S. Paulini episcopi et Confessoris;;Duplex optional;;2;;vide C4

[Rule]
vide C4;
Gloria

[Introitus]
!Ps 131:9-10
v. May Your priests, O Lord, be clothed with justice; let Your faithful ones~
shout merrily for joy. For the sake of David Your servant, reject not the plea~
of Your anointed.
!Ps 131:1
Remember, O Lord, David and all his meekness.
&Gloria
v. May Your priests, O Lord, be clothed with justice; let Your faithful ones~
shout merrily for joy. For the sake of David Your servant, reject not the plea~
of Your anointed.

[Oratio]
O God, Who promised to those who forsake all things in this world for You a~
hundred-fold reward in the world to come and life everlasting, mercifully grant~
that, following closely in the footsteps of the holy Bishop Paulinus, we may~
look upon earthly things as nought, and long only for those of heaven.
$Qui vivis

[Lectio]
Olvasm�ny szent P�l apostol Korintusiakhoz irott m�sodik level�b�l
!2 Cor. 8:9-15
v. Hiszen ismeritek Urunk, J�zus Krisztus j�t�konys�g�t. Noha gazdag volt, �rtetek szeg�nny� lett, hogy szeg�nys�ge �ltal meggazdagodjatok.  Erre n�zve tan�csot adok nektek, mert javatokra v�lik. Tavaly �ta nemcsak hogy megkezdt�tek a gy�jt�st, hanem �szinte sz�nd�kotokat is megmutatt�tok.  Most h�t gyakorlatban is hajts�tok v�gre, hogy a k�szs�ges akarat tehets�getekhez m�rten tett� v�ljon.  Ha az akarat k�szs�ges, aszerint tetszik az Istennek, amije van, nem aszerint, amije nincs.  Nem az�rt kell gy�jteni, hogy m�sok megszabaduljanak a sz�ks�gt�l, ti meg bajba jussatok, hanem az egyenl�s�g�rt.  Most az � sz�ks�g�ket a ti b�s�getek enyh�ti, hogy majd az � b�s�g�k nektek szolg�ljon sz�ks�getekben seg�ts�g�l, s �gy a javak kiegyenl�t�djenek.  Az �r�sban is az �ll: "Aki sokat gy�jt�tt, nem b�velkedett, aki meg keveset, nem sz�k�lk�d�tt." 

[Graduale]
!Eccl 44:16, 20.
Behold a great priest, who in his days pleased God.
V. There was not found the like to him, who kept the law of the Most High.~
Alleluia, alleluia.
!Ps 109:4
V. You are a priest forever, according to the order of Melchisedec. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 12:32-34
v. Ne f�lj, te kisded ny�j, hisz Aty�tok �gy l�tta j�nak, hogy nektek adja orsz�g�t.  Adj�tok el, amitek van, adj�tok oda a r�szorul�knak. K�sz�tsetek magatoknak kimer�thetetlen ersz�nyt, elfogyhatatlan kincset a mennyben, ahol nem f�r hozz� tolvaj, �s nem r�gja sz�t a moly.  Ahol a kincsetek, ott a sz�vetek is. 

[Offertorium]
!Ps 88:21-22
I have found David, My servant; with My holy oil I have anointed him, that My~
hand may be always with him, and that My arm may make him strong.

[Secreta]
Grant us, O Lord, by the example of the blessed Bishop Paulinus, to join the~
sacrifice of perfect charity with the offering of the altar, that by zeal for~
good works we may win everlasting mercy.
$Per Dominum

[Communio]
!Luke 12:42
The faithful and prudent servant whom the master will set over his household to~
give them their ration of grain in due time.

[Postcommunio]
Bestow upon us, O Lord, through the Blessed Sacrament, that inspiration of~
holiness and humility which Your saintly Bishop Paulinus drew from this divine~
source, and, by his intercession, graciously pour forth the riches of Your grace~
upon all who call upon You.
$Per Dominum
