[Rank]
S. Johannis Vianney Confessoris;;Duplex;;3;;vide C5

[Rule]
vide C5;
Gloria

[Introitus]
!Ps 36:30-31
v. The mouth of the just man tells of wisdom, and his tongue utters what is~
right. The law of his God is in his heart.
!Ps 36:1
Be not vexed over evildoers, nor jealous of those who do wrong.
&Gloria
v. The mouth of the just man tells of wisdom, and his tongue utters what is~
right. The law of his God is in his heart.

[Oratio]
Almighty and merciful God, Who made St. John Mary glorious by priestly zeal and~
untiring fervor in prayer and penance, grant, we beseech You, that by his~
example and intercession, we may have strength to win for Christ the souls of~
our brethren and, with them, attain everlasting glory.
$Per Dominum

[Lectio]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Sir 31:8-11
v. Boldog a gazdag, ki hiba n�lk�l tal�ltatott, �s ki az arany ut�n nem j�rt, �s nem b�zott a p�nzben �s kincsekben.  Kicsoda ez, �s dics�rni fogjuk �t mert csodadolgokat cselekedett �let�ben.  Ki megpr�b�ltatott abban, �s t�k�letes maradt, �r�k dics�s�ge leszen; ki v�tkezhetett, �s nem v�tkezett; gonoszat cselekedhetett, �s nem cselekedett;  az�rt biztos�tv�k javai az �rban, �s alamizsn�it besz�li a szentek eg�sz gy�lekezete. 

[Graduale]
!Ps 91:12, 14.
The just man shall flourish like the palm tree, like a cedar of Lebanon shall he~
grow in the house of the Lord.
!Ps 91:3
V. To proclaim Your kindness at dawn and Your faithfulness throughout the night.~
Alleluia, alleluia.
!James 1:12
V. Blessed is the man who endures temptation; for when he has been tried, he~
will receive the crown of life. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 12:35-40
v. Cs�p�t�k legyen fel�vezve �s �gjen a l�mp�sotok.  Hasonl�tsatok azokhoz az emberekhez, akik urukra v�rnak, hogy mihelyt meg�rkezik a menyegz�r�l �s z�rget, r�gt�n ajt�t nyissanak neki.  Boldogok azok a szolg�k, akiket uruk meg�rkez�sekor �bren tal�l. Bizony mondom nektek, fel�vezi mag�t, asztalhoz �lteti �ket, �s megy, hogy kiszolg�lja �ket.  �s ha a m�sodik vagy a harmadik �rv�lt�skor �rkezve is �gy tal�lja �ket, boldogok azok a szolg�k.  Gondolj�tok meg: ha tudn� a h�zigazda, hogy melyik �r�ban j�n a tolvaj, nem engedn� bet�rni h�z�ba.  Legyetek ti is k�szen, mert olyan �r�ban, amikor nem is gondolj�tok, elj�n az Emberfia." 

[Offertorium]
!Ps 88:25
My faithfulness and My kindness shall be with him, and through My name shall his~
horn be exalted. ( Alleluia.)

[Secreta]
We offer You sacrifices of praise, O Lord, in memory of Your saints; trusting~
that by them we may be delivered from both present and future evils.
$Per Dominum

[Communio]
!Matt 24:46-47
Blessed is that servant whom his master, when he comes, shall find watching.~
Amen I say to you, he will set him over all his goods. ( Alleluia.)

[Postcommunio]
Refreshed with heavenly food and drink, we humbly pray You, our God, that we~
also may be helped by his prayers in memory of whom we have partaken of these~
gifts.
$Per Dominum

[Commemoratio Oratio]
!Vigilia S. Laurentii
Heed our prayers, O Lord, and by the intercession of Your blessed Martyr~
Lawrence, whose feast we anticipate, graciously grant us Your everlasting mercy.
$Per Dominum
_
!For ST. Romanus Martyr
Grant, we beseech You, almighty God, that by the intercession of St. Romanus,~
Your Martyr, we may be delivered from all afflictions in the body and purified~
in mind from all evil thoughts.
$Per Dominum

[Commemoratio Secreta]
!Vigilia S. Laurentii
Look with favor upon the sacrificial gifts we offer You, O Lord, and through~
this holy exchange, loosen the bonds of our sins. Look with favor upon the~
sacrificial gifts we offer You, O Lord, and through this holy exchange, loosen~
the bonds of our sins. Through our Lord.
_
!For ST. ROMANUS martyr
Having received our gifts and prayers, we beseech You, O Lord, mercifully hear~
us and cleanse us by Your heavenly sacrament.
$Per Dominum

[Commemoratio Postcommunio]
!Vigilia S. Laurentii
Grant, we beseech You, O Lord, our God, that as we joyfully commemorate St.~
Lawrence, Your Martyr, in this life, so may we rejoice by beholding him in~
eternity. $Per Dominum
_
!For ST. ROMANUS martyr
We beseech You, O almighty God, that we who have eaten the food of heaven may~
find in it, by the intercession of blessed Romanus, Your Martyr, strength~
against all harm.
$Per Dominum
