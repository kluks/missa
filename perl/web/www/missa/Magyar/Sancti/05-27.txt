[Rank]
S. Bedae Venerabilis Confessoris et Ecclesiae Doctoris;;Duplex majus;;4;;vide C5a

[RankNewcal]
S. Bedae Venerabilis Confessoris et Ecclesiae Doctoris;;Duplex optional;;2;;vide C5a

[Rule]
vide C5a;
CPapaM=Janos;
Gloria

[Introitus]
!Sir 15:5
v. In the midst of the assembly he opened his mouth; and the Lord filled him~
with the spirit of wisdom and understanding; He clothed him with a robe of~
glory.
!Ps 91:2
It is good to give thanks to the Lord, to sing praise to Your name, Most High.
&Gloria
v. In the midst of the assembly he opened his mouth; and the Lord filled him~
with the spirit of wisdom and understanding; He clothed him with a robe of~
glory.

[Oratio]
O God, Who enlightened Your Church with the learning of blessed Bede, Your~
Confessor and Doctor, graciously grant that Your servants may ever be~
enlightened by his wisdom and helped by his merits.
$Per Dominum

[Lectio]
Olvasm�ny szent P�l apostol Tim�teushoz irott m�sodik level�b�l
!2 Tim 4:1-8
v. K�rve-k�rlek az Istenre �s Krisztus J�zusra, aki �t�lkezni fog �l�k �s holtak f�l�tt, az � elj�vetel�re �s orsz�g�ra:  hirdesd az evang�liumot, �llj vele el�, ak�r alkalmas, ak�r alkalmatlan. �rvelj, ints, buzd�ts nagy t�relemmel �s hozz��rt�ssel.  Mert j�n id�, amikor az eg�szs�ges tan�t�st nem hallgatj�k sz�vesen, hanem saj�t �zl�s�k szerint szereznek maguknak tan�t�kat, hogy f�l�ket csiklandoztass�k.  Az igazs�got nem hallgatj�k meg, de a mes�ket elfogadj�k.  Te azonban maradj mindenben meggondolt, viseld el a bajokat, teljes�tsd az evang�lium hirdet�j�nek feladat�t, t�ltsd be szolg�latodat.  Az �n v�remet ugyanis nemsok�ra kiontj�k �ldozatul, elt�voz�som ideje k�zel van.  A j� harcot megharcolam, a p�ly�t v�gigfutottam, hitemet megtartottam.  K�szen v�r az igazs�g gy�zelmi koszor�ja, amelyet azon a napon megad nekem az �r, az igazs�gos b�r�, de nemcsak nekem, hanem mindenkinek, aki �r�mmel v�rja elj�vetel�t. 

[Graduale]
!Ps 106:32; 106:31
Let them extol in the assembly of the people and praise him in the council of~
the elders.
V. Let them give thanks to the Lord for His kindness and His wondrous deeds to~
the children of men. Alleluia, alleluia.
!Matt 16:18
V. You are Peter, and upon this rock I will build My Church. Alleluia.

[Tractus]
!Ps 39:10-11
I announced Your justice in the vast assembly; I did not restrain my lips as You, O Lord, know.
V. Your justice I kept not hid within my heart; Your faithfulness and Your salvation I have spoken of.
V. I have made no secret of Your kindness and Your truth in the vast assembly.

[GradualeP]
Alleluia, alleluia.
!Matt 16:18
V. You are Peter and upon this rock I will build My Church. Alleluia.
!Ps 44:17-18
V. You shall make them princes through all the land; they shall remember Your~
name through all generations. Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 5:13-19
v. Ti vagytok a f�ld s�ja. Ha a s� �z�t veszti, ugyan mivel s�zz�k meg? Nem val� egy�bre, mint hogy kidobj�k, s az emberek eltaposs�k.  Ti vagytok a vil�g vil�goss�ga. A hegyen �p�lt v�rost nem lehet elrejteni.  S ha vil�got gy�jtan�nak, nem rejtik a v�ka al�, hanem a tart�ra teszik, hogy mindenkinek vil�g�tson a h�zban.  Ugyan�gy a ti vil�goss�gotok is vil�g�tson az embereknek, hogy j�tetteiteket l�tva dics��ts�k mennyei Aty�tokat!  Ne gondolj�tok, hogy megsz�ntetni j�ttem a t�rv�nyt vagy a pr�f�t�kat. Nem megsz�ntetni j�ttem, hanem teljess� tenni.  Bizony mondom nektek, m�g �g �s f�ld el nem m�lik, egy i bet� vagy egy vessz�cske sem v�sz el a t�rv�nyb�l, hanem minden beteljesedik.  Aki teh�t csak egyet is elt�r�l e legkisebb parancsok k�z�l, �s �gy tan�tja az embereket, azt igen kicsinek fogj�k h�vni a mennyek orsz�g�ban. Aki viszont megtartja �s tan�tja �ket, az nagy lesz a mennyek orsz�g�ban. 

[Offertorium]
!Ps 91:13
The just shall flourish like the palm tree; like a cedar of Libanus shall he~
grow.

[Secreta]
May the loving prayer of blessed Bede, Your Bishop and Doctor, fail us never, O~
Lord; may it commend our offerings and ever secure for us Your forgiveness.
$Per Dominum

[Communio]
!Luke 12:42
The faithful and prudent servant whom the master will set over his household to~
give them their ration of grain in due time.

[Postcommunio]
So that your sacrificial rites may grant us salvation, we pray you, O Lord, that~
blessed Bede, Your Bishop and illustrious Doctor, may draw nigh as our~
intercessor.
$Per Dominum

[Commemoratio Oratio]
!For ST. JOHN I.
@Commune/C2b:Oratio:s/N\./J�nos/
(sed communi Summorum Pontificum dicitur)
@Commune/C4b:Oratio Gregem

[Commemoratio Secreta]
!For ST. JOHN I.
@Commune/C2b:Secreta:s/N\./J�nos/
(sed communi Summorum Pontificum dicitur)
@Commune/C4b:Secreta Gregem

[Commemoratio Postcommunio]
!For ST. JOHN I.
@Commune/C2b:Postcommunio:s/N\./J�nos/
(sed communi Summorum Pontificum dicitur)
@Commune/C4b:Postcommunio Gregem
