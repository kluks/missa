[Rank]
In Vigilia S. Andrae Apostoli;;Vigilia;;1.5;;vide C1v

[Rule]
vide C1v;
no Gloria

[Introitus]
!Matth 4:18; 4:19
v. D�minus secus mare Galilae� vidit duos fratres, Petrum et Andr�am, et voc�vit~
eos: Ven�te post me: f�ciam vos f�eri piscat�res h�minum
!Ps 18:2. 
Coeli en�rrant gl�riam Dei: et �pera m�nuum ejus ann�ntiat firmam�ntum.
&Gloria
v. D�minus secus mare Galilae� vidit duos fratres, Petrum et Andr�am, et voc�vit~
eos: Ven�te post me: f�ciam vos f�eri piscat�res h�minum

[Oratio]
Quaesumus, omn�potens Deus: ut be�tus Andr�as Ap�stolus, cujus pr�ven�mus~
festivit�tem, tuum pro nobis impl�ret aux�lium; ut, a nostris re�tibus absol�ti, a~
cunctis �tiam per�culis eru�mur.
$Per Dominum

[Lectio]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Eccli 44:25-27; 45:2-4; 45:6-9
v. Minden nemzetek �ld�s�t neki adta az �r, �s sz�vets�g�t meger�s�tette J�kob fej�n.  Elismerte �t �ld�sai �ltal, �s �r�ks�get adott neki, �s r�szenkint eloszt� azt a tizenk�t nemzets�g k�z�tt.  �s megtartotta neki az irgalmass�g f�rfiait, kik kedvet tal�ltak minden ember szemei el�tt.  Hasonl�v� tette �t dics�s�gben a szentekhez, �s f�lmagasztalta �t f�lelem�l az ellens�geknek, �s ig�j�vel megsz�ntette a csodajeleket.  Megdics�itette �t a kir�lyok szine el�tt, �s parancsot adott neki n�p�hez, �s megmutatta neki dics�s�g�t.  Az � h�s�ge �s szel�ds�ge miatt megszentel� �t, �s kiv�lasztotta �t minden test k�z�l.  �s szemt�l szembe parancsokat adott neki, az �let �s fegyelem t�rv�ny�t, hogy megtanitsa J�kobot sz�vets�g�re, �s Izraelt it�leteire.  Felmagasztalta �ront, az � b�tyj�t, L�vi nemzets�g�b�l, �s hozz�ja hasonl�v� tette �t.  �r�k sz�vets�get k�t�tt vele, �s neki adta a n�p k�zt a paps�got, �s megboldog�totta �t dics�s�ggel.  �s k�r�l�vezte �t a dics�s�g �v�vel, �s a dics�s�g k�nt�s�be �lt�ztette, �s megkoron�zta �t a hatalom eszk�zeivel. 

[Graduale]
!Ps 138:17-18
Nimis honor�ti sunt am�ci tui, Deus: nimis confort�tus est princip�tus e�rum.
V. Dinumer�bo eos: et super ar�nam multiplicab�ntur.

[Evangelium]
Evang�lium szent J�nos Apostol k�nyv�b�l
!Joann 1:35-51
v. M�snap megint ott �llt J�nos k�t tan�tv�ny�val,  s mihelyt megl�tta J�zust, amint k�zeledett, �gy sz�lt: "N�zz�tek, az Isten B�r�nya!"  E szavak hallat�ra a k�t tan�tv�ny J�zus nyom�ba szeg�d�tt.  Amikor J�zus megfordult, s l�tta, hogy k�vetik, megk�rdezte: "Mit akartok?" �gy feleltek: "Rabbi - ami annyit jelent, mint Mester -, hol lakol?"  "Gyertek, n�zz�tek meg!" - mondta nekik. Elmentek vele, megn�zt�k, hol lakik, s aznap n�la is maradtak. A tizedik �ra k�r�l j�rhatott.  A kett� k�z�l, aki J�nos szav�ra k�vette, az egyik Andr�s volt, Simon P�ter testv�re.  Reggel tal�lkozott testv�r�vel, Simonnal, s sz�lt neki: "Megtal�ltuk a Messi�st, vagy m�s sz�val a F�lkentet", s  elvitte J�zushoz. J�zus r�emelte tekintet�t, s �gy sz�lt hozz�: "Te Simon vagy, J�nos fia, de K�fa, azaz P�ter lesz a neved."  M�snap Galilea fel� tartva tal�lkozott F�l�ppel. Felsz�l�totta: "Gyere �s k�vess!"  F�l�p Betszaid�b�l, Andr�s �s P�ter v�ros�b�l val� volt.  F�l�p tal�lkozott Nat�naellel �s elmondta neki: "Megtal�ltuk, akir�l M�zes t�rv�nye �s a pr�f�t�k �rnak, a n�z�reti J�zust, J�zsefnek a fi�t."  "J�het valami j� N�z�retb�l?" - k�rdezte Nat�nael. "Gyere �s gy�z�dj�l meg r�la!" - felelte F�l�p.  Amikor J�zus megl�tta Nat�naelt, amint fel�je tartott, ezt mondta r�la: "L�m, egy igazi izraelita, akiben nincs semmi �lnoks�g."  Nat�nael megk�rdezte t�le: "Honn�t ismersz?" J�zus �gy v�laszolt: "Miel�tt F�l�p h�vott volna, l�ttalak, a f�gefa alatt volt�l."  Nat�nael erre felki�ltott: "Rabbi azaz: Mester, te vagy az Isten Fia, te vagy Izrael kir�lya!"  J�zus ezt felelte neki: "Mivel megmondtam, hogy l�ttalak a f�gefa alatt, hiszel. De nagyobb dolgokat is fogsz m�g l�tni." Majd hozz�tette:  "Bizony, bizony mondom nektek: L�tni fogj�tok, hogy megny�lik az �g, s az Isten angyalai f�l- s lesz�llnak az Emberfia f�l�tt." 

[Offertorium]
!Ps 8:6-7
Gl�ria et hon�re coron�sti eum: et constitu�sti eum super �pera m�nuum tu�rum,~
D�mine.

[Secreta]
 Sacr�ndum tibi, D�mine, munus off�rimus: quo be�ti Andr�� Ap�stoli soll�mnia~
recol�ntes, purificati�nem quoque nostris m�ntibus implor�mus.
$Per Dominum

[Communio]
!Joann 1:41; 1:42
Dicit Andr�as Sim�ni fratri suo: Inv�nimus Mess�am, qui d�citur Christus: et~
add�xit eum ad Jesum.

[Postcommunio]
Perc�ptis, D�mine, sacram�ntis suppl�citer exor�mus: ut, interced�nte be�to~
Andr�a Apostolo tuo, qu� pro ill�us vener�nda g�rimus passi�ne, nobis prof�ciant~
ad med�lam.
$Per Dominum

[Commemoratio Oratio]
!Pro S. Saturnino Mart.
Deus, qui nos be�ti Saturn�ni Martyris tui conc�dis natal�tia p�rfrui: ejus nos~
tr�bue m�ritis adjuv�ri.
$Per Dominum

[Commemoratio Secreta]
!Pro S. Saturnino Mart.
M�nera, D�mine, tibi dic�ta sanct�fica: et, interced�nte be�to Saturn�no M�rtyre~
tuo, per h�c e�dem nos plac�tus int�nde.
$Per Dominum

[Commemoratio Postcommunio]
!Pro S. Saturnino Mart.
Sanct�ficet nos, quaesumus, D�mine, tui perc�ptio sacram�nti: et intercessi�ne~
Sanct�rum tu�rum tibi reddat acc�ptos.
$Per Dominum
