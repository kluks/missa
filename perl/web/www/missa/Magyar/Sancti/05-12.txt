[Rank]
Ss. Nerei, Achillei et Domitillae Virgini atque Pancratii Martyrum;;Semiduplex;;2;;vide C3

[Rank1570]
Ss. Nereus, Achilleus and Pancras martyrs;;Simplex;;1.1;;vide C3

[Rule]
Gloria

[Introitus]
!Ps. 32:18-20.
v. But see, the eyes of the Lord are upon those who fear Him, upon those who~
hope in His kindness, alleluia: to deliver them from death; for He is our help~
and our shield, alleluia, alleluia.
!Ps 32:1
Exult, you just, in the Lord; praise from the upright is fitting.
&Gloria
v. But see, the eyes of the Lord are upon those who fear Him, upon those who~
hope in His kindness, alleluia: to deliver them from death; for He is our help~
and our shield, alleluia, alleluia.

[Oratio]
May the holy feast of Your Martyrs, Nereus, Achilleus, Domitilla and Pancras,~
ever comfort us, we beseech You, O Lord, and make us worthy to serve You.
$Per Dominum

[Lectio]
Olvasm�ny a B�lcsess�g k�nyv�b�l
!Wis 5:1-5
v. Akkor az igaz teljes biztons�ggal �ll szemben azokkal, akik sanyargatt�k, �s lebecs�lt�k a f�radoz�s�t.  Amikor ezt l�tj�k, iszony� f�lelem fogja el �ket, �s nem tudnak hov� lenni; annak nem v�rt �dv�ss�ge miatt.  B�nk�dva sz�lnak egym�shoz, �s s�hajtoznak lelk�k f�lelm�ben:  "Ez az, akit egykor kinevett�nk �s akib�l g�nyt �zt�nk. Mi esztelenek! �letm�dj�t �r�lts�gnek tartottuk �s hal�l�t dicstelennek.  Hogy lehet az, hogy Isten fiai k�z� sorolt�k �s a szentek k�z�tt van az oszt�lyr�sze? 

[Graduale]
Alleluia, alleluia.
V. This is the true brotherhood, which overcame the wickedness of the world; it~
followed Christ, attaining the noble kingdom of heaven. Alleluia.
V. The white-robed army of Martyrs praises You, O Lord. Alleluia.

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 4:46 - 53.
v. �gy �jra a galileai K�n�ba ment, ahol a vizet borr� v�ltoztatta. Kafarnaumban volt egy kir�lyi tisztvisel�, akinek a fia beteg volt.  Amikor meghallotta, hogy J�zus J�de�b�l Galile�ba �rkezett, f�lkereste, �s arra k�rte, menjen el, gy�gy�tsa meg a fi�t, aki m�r a hal�l�n volt.  J�zus azonban �gy sz�lt: "Hacsak jeleket �s csod�kat nem l�ttok, nem hisztek."  De a kir�lyi tisztvisel� tov�bb k�rlelte: "Gyere, Uram, miel�tt m�g meghalna a fiam!"  J�zus megnyugtatta: "Menj csak, �l a fiad!" Az ember hitt J�zus szav�nak, amelyet hozz� int�zett, �s elindult haza.  �tk�zben el� j�ttek szolg�i �s jelentett�k, hogy �l a fia.  Megk�rdezte t�l�k, hogy melyik �r�ban lett jobban. "Tegnap, a hetedik �ra k�r�l hagyta el a l�z" - felelt�k.  Akkor az apa r�eszm�lt, hogy �pp ebben az �r�ban t�rt�nt, hogy J�zus azt mondta neki: "�l a fiad." �s h�v� lett eg�sz h�za n�p�vel egyetemben. 

[Offertorium]
!Ps. 88:6
The heavens proclaim Your wonders, O Lord, and Your faithfulness, in the~
assembly of the holy ones, alleluia, alleluia, alleluia.

[Secreta]
May the witnessing to the faith made by Your holy Martyrs, Nereus, Achilleus,~
Domitilla and Pancras, be pleasing to You, O Lord, both to commend our offerings~
to You and to assure us of Your everlasting mercy.
$Per Dominum

[Communio]
!Ps. 32:1
Exult, you just, in the Lord, alleluia: praise from the upright is fitting,~
alleluia.

[Postcommunio]
O Lord, we beseech You, that the holy sacrament we have received may, by the~
pleadings of Your blessed Martyrs, Nereus, Achilleus, Domitilla and Pancras,~
grant us always more of Your forgiveness.
$Per Dominum
