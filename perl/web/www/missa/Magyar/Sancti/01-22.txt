[Rank]
Ss. Vincentii et Anastasii Martyrum;;Semiduplex;;2;;vide C3

[Rule]
vide C3;
Gloria

[Introitus]
!Ps 78:11, 2, 10.
v. Let the prisoners� sighing come before You, O Lord; repay our neighbors~
sevenfold into their bosoms; avenge the shedding of Your servants� blood.
!Ps 78:1
O God, the nations have come into Your inheritance; they have defiled Your holy~
temple, they have made Jerusalem as a place to keep fruit.
&Gloria
v. Let the prisoners� sighing come before You, O Lord; repay our neighbors~
sevenfold into their bosoms; avenge the shedding of Your servants� blood.

[Oratio]
Give heed to our humble prayers, O Lord, that we who know we are guilty of our~
own sin, may be saved by the intercession of Your blessed Martyrs Vincent and~
Anastasius.
$Per Dominum

[Lectio]
Olvasm�ny a B�lcsess�g k�nyv�b�l
!Wis 3:1-8
v. Az igazak lelke azonban Isten kez�ben van, �s gy�trelem nem �rheti �ket.  Az esztelenek szem�ben �gy l�tszott, hogy meghaltak; a vil�gb�l val� t�voz�sukat balsorsnak v�lt�k,  elmenetel�ket megsemmis�l�snek. De b�kess�gben vannak.  Mert ha az emberek szem�ben szenvedtek is, a rem�ny�k tele volt halhatatlans�ggal.  Kev�s feny�t�s ut�n nagy j�t�tem�nyekben r�szes�lnek, mert Isten pr�b�ra tette �s mag�hoz m�lt�nak tal�lta �ket.  Mint aranyat a koh�ban, megvizsg�lta �s elfogadta �ket �g��ldozatul.  L�togat�suk idej�n majd felragyognak, �s olyanok lesznek, mint a szikra, amely a tarl�n tovaharap�dzik.  Nemzetek f�l�tt �t�lkeznek majd �s n�peken uralkodnak; s az �r lesz a kir�lyuk mind�r�kre. 

[Graduale]
!Ex 15:11, 6.
God is glorious in His Saints, wonderful in majesty, a worker of wonders.
V. Your right hand, O Lord, is magnificent in power; Your right hand has~
shattered the enemy. Alleluia, alleluia.
V. Ecclus. 44:14 The bodies of the Saints are buried in peace, but their name~
lives on and on. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 21:9-19
v. Amikor h�bor�kr�l �s l�zad�sokr�l hallotok, ne r�m�ld�zzetek. Ilyeneknek kell el�bb t�rt�nni�k, de ezzel m�g nincs itt a v�g!"  Azt�n �gy folytatta: "Nemzet nemzet ellen �s orsz�g orsz�g ellen t�mad.  Nagy f�ldreng�s lesz itt is, ott is, �h�ns�g �s d�gv�sz. Sz�rny� t�nem�nyek �s k�l�n�s jelek t�nnek fel az �gen.  De el�bb kezet emelnek r�tok, �s �ld�zni fognak benneteket. Kiszolg�ltatnak a zsinag�g�knak �s b�rt�nbe vetnek, kir�lyok �s helytart�k el� hurcolnak a nevem�rt,  az�rt, hogy tan�s�got tegyetek.  V�ss�tek h�t sz�vetekbe: Ne t�rj�tek fejeteket, hogyan v�dekezzetek.  Olyan �kessz�l�st �s b�lcsess�get adok nektek, hogy egyetlen ellenfeletek sem tud ellen�llni vagy ellentmondani nektek.  Kiszolg�ltatnak benneteket a sz�l�k, testv�rek, rokonok �s bar�tok, s n�melyeket meg�lnek k�z�letek.  Nevem�rt mindenki gy�l�lni fog benneteket.  De nem v�sz el egy hajsz�l sem fejetekr�l.  Kitart�stokkal megmentitek lelketeket. 

[Offertorium]
!Ps 67:36
God is wonderful in His saints; the God of Israel is He Who gives power and~
strength to His people. Blessed be God! Alleluia.

[Secreta]
We offer You, O Lord, the gifts our service; may they be pleasing to You for the~
honor of Your just ones and, through Your mercy, bring us salvation.
$Per Dominum

[Communio]
!Wis 3: 4-6.
For if before men they were punished, God tried them; as gold in the furnace He~
proved them, and as sacrificial offerings He took them to Himself.

[Postcommunio]
We beseech You, almighty God, that we who have eaten heavenly food may through~
it be protected from all harm by the intercession of Your blessed Martyrs~
Vincent and Anastasius.
$Per Dominum
