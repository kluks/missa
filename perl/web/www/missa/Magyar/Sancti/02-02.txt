[Rank]
In Purificatione Beatae Mariae Virginis;;Duplex 2 class;;5.1;;ex C11

[Rank1960]
In Purificatione Beatae Mariae Virginis;;Duplex 2 class;;5;;ex C11

[Rule]
Gloria
Credo
Prefatio=Nat
(rubrica 1960) omit commemoratio incipit
(rubrica 1960) Festum Domini
Prelude

[Prelude]
#Benedictio Candelarum
! The priest, vested in a purple cope, blesses the candles, which are placed near the altar.
V. The Lord be with you.
R. And with thy spirit.
v. Let us pray. O Holy Lord, Father almighty, everlasting God, who hast created all things out of nothing, and by Thy command hast caused this liquid to become perfect wax by the labor of bees: and who, on this day didst fulfill the petition of the righteous man Simeon: we humbly entreat Thee, that by the invocation of Thy most holy Name and through the intercession of Blessed Mary ever Virgin whose feast is today devoutly observed, and by the prayers of all Thy Saints, Thou wouldst vouchsafe to bless + and sanctify + these candles for the service of men and for the health of their bodies and souls, whether on land or on sea: and that Thou wouldst hear from Thy holy heaven, and from the throne of Thy Majesty the voices of this Thy people, who desire to carry them in their hands with honor, and to praise Thee with hymns; and wouldst be propitious to all that call upon Thee, in the unity of the Holy Ghost, God, world without end.
R.: Amen. 	
_
_
v. Let us pray. O almighty and everlasting God, who on this day didst present Thine only-begotten Son in Thy holy temple to be received in the arms of holy Simeon: we humbly entreat Thy clemency, that Thou wouldst vouchsafe to bless + and sanctify + and to kindle with the light of Thy heavenly benediction these candles, which we, Thy servants, desire to receive and to bear lighted in the honor of Thy Name: that, by offering them to Thee our Lord God, being worthily inflamed with the holy fire of Thy most sweet charity, we may deserve to be presented in the holy temple of Thy glory. 
$Per eundem
_
_
v. Let us pray. O Lord Jesus Christ, the true Light who enlightenest every man that cometh into this world: pour forth Thy blessing + upon these candles, and sanctify + them with the light of Thy grace, and mercifully grant, that as these lights enkindled with visible fire dispel the darkness of night, so our hearts illumined by invisible fire, that is, by the splendor of the Holy Spirit, may be free from the blindness of all vice, that the eye of our mind being cleansed, we may be able to discern what is pleasing to Thee and profitable to our salvation; so that after the perilous darkness of this life we may deserve to attain to neverfailing light: through Thee, O Christ Jesus, Savior of the world, who in the perfect Trinity, livest and reignest, God, world without end.
R.: Amen. 	
_
_
v. Let us pray. O almighty and everlasting God, who by Thy servant Moses didst command the purest oil to be prepared for lamps to burn continuously before Thee: vouchsafe to pour forth the grace of Thy blessing + upon these candles: that they may so afford us light outwardly that by Thy gift, the gift of Thy Spirit may never be wanting inwardly to our minds. 
$Per Dominum
_
_ 
v. Let us pray. O Lord Jesus Christ, who appearing on this day among men in the substance of our flesh, wast presented by Thy parents in the temple: whom the venerable and aged Simeon, illuminated by the light of Thy Spirit, recognized, received into his arms, and blessed: mercifully grant that, enlightened and taught by the grace of the same Holy Ghost, we may truly acknowledge Thee and faithfully love Thee; Who with God the Father in the unity of the same Holy Ghost livest and reignest, God, world without end.
R.: Amen. 	
_
_
! The celebrant sprinkles the candles three times with holy water, saying the anthem Asperges me Domine . . ., and also incensing them thrice.
_
!Antiphon Luc 2:32
Ant. A light for the revelation of the Gentiles: and for the glory of Thy people Israel.
!Luc 2:29-31
v. Now Thou dost dismiss Thy servant, O Lord, according to Thy word in peace.
Ant. A light for the revelation of the Gentiles: and for the glory of Thy people Israel.
v. Because mine eyes have seen Thy salvation.
Ant. A light for the revelation of the Gentiles: and for the glory of Thy people Israel.
v.  Which Thou hast prepared, before the face of all peoples.
Ant. A light for the revelation of the Gentiles: and for the glory of Thy people Israel.
v. Glory be to the Father, and to the Son, and to the Holy Ghost.
Ant. A light for the revelation of the Gentiles: and for the glory of Thy people Israel.
v. As it was in the beginning, is now, and ever shall be, world without end. Amen.
Ant. A light for the revelation of the Gentiles: and for the glory of Thy people Israel.
_
_
! After this sings
Ant. Arise, O Lord, help us and deliver us for Thy Name's sake. 
!Ps 43:26
We have heard, O God, with our ears: our fathers have declared to us. 
&Gloria
Ant. Arise, O Lord, help us and deliver us for Thy Name's sake. 
_
_
! After this the priest says
v. Let us pray
! After Septuagesimam except on Sundays
V. Let us kneel.
R. Arise
v. We beseech Thee, O Lord, hearken unto Thy people, and grant that by the light of Thy grace, we may inwardly attain to those things which Thou grantest us outwardly to venerate by this yearly observance. Through Christ our Lord.
R. Amen
_
_
! The deacon turns to the people and says:
V. Let us go forth in peace.
R. In the Name of Christ. Amen.
!  The procession takes place in the usual order, all bearing the lighted candles: the following anthems are sung the while:
Ant. Adorn thy bridal-chamber, O Sion, and welcome Christ the King: with loving embrace greet Mary who is the very gate of heaven; for she bringeth to thee the glorious King of the new light: remaining ever a Virgin yet she bearest in her arms the Son begotten before the day-star: even the Child, whom Simeon taking into his arms, declared to the peoples to be the Lord of life and death, and the Savior of the world.
_
!Luc 2:26, 27 et 28-29.
Ant. Simeon received an answer from the Holy Ghost, that he should not see death before he had seen the Christ of the Lord; and when they brought the Child into the temple, he took Him into His arms, and blessed God, and said: Now dost Thou dismiss Thy servant, O Lord, in peace. V.: When His parents brought in the Child Jesus, to do for Him according to the custom of the law, he took Him into His arms.
V. They offered for Him to the Lord a pair of turtle doves, or two young pigeons: * As it is written in the Law of the Lord.
V. After the days of the purification of Mary, according to the law of Moses, were fulfilled, they carried Jesus to Jerusalem, to present Him to the Lord. * As it is written in the Law of the Lord.
V. Glory be to the Father, and to the Son, and to the Holy Ghost.
R. As it is written in the Law of the Lord.
! 

[Introitus]
!Ps 47:10-11
v. O God, we ponder Your kindness within Your temple. As Your name, O God, so also Your praise reaches to the ends of the earth. Of justice Your right hand is full.
!Ps 47:2
Great is the Lord, and wholly to be praised in the city of our God, His holy mountain.
&Gloria
v. O God, we ponder Your kindness within Your temple. As Your name, O God, so also Your praise reaches to the ends of the earth. Of justice Your right hand is full.

[Oratio]
Almighty, eternal God, we humbly beseech Your majesty that, as Your only-begotten Son was this day presented in the temple in the nature of our flesh, so may You grant us to be presented to You with purified minds.
$Per Dominum

[Lectio]
Olvasm�ny Malaki�s pr�f�ta k�nyv�b�l
!Mal 3:1-4
v. N�zz�tek, elk�ld�m h�rn�k�met, hogy elk�sz�tse el�ttem az utat. Hamarosan bel�p szent�ly�be az �r, akit kerestek, �s a sz�vets�g angyala, aki ut�n v�gyakoztok. L�m, m�r j�n is! - mondja a Seregek Ura.  De ki fogja tudni elviselni j�vetele napj�t? �s ki maradhat �llva, amikor megjelenik? Mert olyan, mint az olvaszt�k t�ze �s a v�nyol�k l�gja.  Le�l, mint az ez�stolvaszt� �s -tiszt�t� mester. Megtiszt�tja L�vi fiait, megfinom�tja �ket, mint az aranyat �s az ez�st�t, hogy m�lt�k�ppen mutass�k be az �ldozatot az �rnak.  J�da �s Jeruzs�lem �ldozata tetszeni fog akkor az �rnak, �pp�gy, mint a r�gm�lt id�kben, mint az els� �vekben. 

[Graduale]
!Ps 47:10-11, 9.
O God, we ponder Your kindness within Your temple. As Your name, O God, so also Your praise reaches to the ends of the earth.
V. As we have heard so have we seen, in the city of our God, in His holy mountain.Allel�ja, allel�ja.
V. The old man bore the Child, but the Child was the old man's King; Allel�ja.

[Tractus]
!Ps 47:10-11, 9.
O God, we ponder Your kindness within Your temple. As Your name, O God, so also Your praise reaches to the ends of the earth.
V. As we have heard so have we seen, in the city of our God, in His holy mountain.
!Luke 2:29-32
Now You dismiss Your servant, O Lord, according to Your word, in peace.
V. Because my eyes have seen Your salvation.
V. Which You have prepared before the face of all peoples.
V. A light of revelation to the Gentiles, and a glory for Your people Israel.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 2:22-32
v. Amikor elteltek a tisztul�snak a M�zes t�rv�ny�ben megszabott napjai, felvitt�k Jeruzs�lembe, hogy bemutass�k az �rnak,  ahogy az �r t�rv�ny�ben el� volt �rva: az anyam�het megnyit� minden els�sz�l�tt fi� az �r szentj�nek h�vatik,  s az �ldozatot is be akart�k mutatni, ahogy az �r t�rv�nye el��rta: egy p�r gerlic�t vagy k�t galambfi�k�t.  �me, volt Jeruzs�lemben egy Simeon nev� igaz �s istenf�l� ember. V�rta Izrael vigasz�t, �s a Szentl�lek volt rajta.  Kinyilatkoztat�st kapott a Szentl�lekt�l, hogy addig nem hal meg, am�g meg nem l�tja az �r F�lkentj�t.  A L�lek ind�t�s�ra a templomba ment. Amikor a sz�l�k a gyermek J�zust bevitt�k, hogy a t�rv�ny el��r�s�nak eleget tegyenek,  karj�ba vette �s �ldotta az Istent ezekkel a szavakkal:  "Most bocs�sd el, Uram, szolg�dat, szavaid szerint b�k�ben,  mert l�tta szemem �dv�ss�gedet,  melyet minden n�p sz�ne el�tt k�sz�tett�l,  vil�goss�gul a pog�nyok megvil�gos�t�s�ra, �s dics�s�g�l n�pednek, Izraelnek." 

[Offertorium]
!Ps 44:3
Grace is poured out upon your lips; thus God has blessed you forever, and for ages of ages.

[Secreta]
O Lord, heed our prayer, and give us the help of Your loving kindness so that the gifts we offer before the eyes of Your majesty may be worthy of You.
$Per Dominum

[Communio]
!Luke 2:26
It was revealed to Simeon by the Holy Spirit that he should not see death before he had seen the Christ of the Lord.

[Postcommunio]
We beseech You, O Lord our God, that the sacrament You have given as the bulwark of our atonement may be made a saving remedy for us in this life and in the life to come.
$Per Dominum
