[Rank]
Cathedrae S. Petri;;Duplex majus;;4;;ex C4

[Rule]
ex C4;
Gloria
Credo
!Prefatio Apostolis

[Introitus]
!Sir 45:30.
v. Therefore he made to him a covenant of peace, to be the prince of the~
sanctuary, and of his people, that the dignity of priesthood should be to him~
and to his seed for ever.
!Ps 131:1
v. O Lord, remember David, and all his meekness.
&Gloria
v. Therefore he made to him a covenant of peace, to be the prince of the~
sanctuary, and of his people, that the dignity of priesthood should be to him~
and to his seed for ever.

[Oratio]
O God, who together with the keys of the kingdom of heaven didst bestow on thy~
blessed Apostle Peter the pontifical power of binding and loosing, grant that by~
the aid of this intercession, we may be released from the bonds of our sins.
$Qui vivis

[Lectio]
Olvasm�ny szent P�ter apostol els� level�b�l
!1 Pet 1:1-7
v. P�ter, J�zus Krisztus apostola a zar�ndokoknak, akik Pontusz, Gal�cia, Kappad�cia, �zsia �s Bitinia ter�let�n sz�rv�nyokban �lnek, s akiket az  Atyaisten el�retud�sa birtok�ban kiv�lasztott, a L�lek megszentel� erej�b�l az engedelmess�gre �s a J�zus Krisztus v�r�vel val� meghint�sre. Kegyelemben �s b�k�ben b�ven legyen r�szetek!  Legyen �ldott az Isten �s Urunk, J�zus Krisztus Atyja, aki minket nagy irgalm�ban J�zusnak a hal�lb�l val� felt�mad�sa �ltal �j �letre h�vott, az �l� rem�nyre,  hogy a mennyekben elpuszt�thatatlan, tiszta �s soha el nem hervad� �r�ks�g v�rjon r�tok.  Az Isten hatalma meg�rz�tt titeket a hitben az �r�k �dv�ss�gre, amely k�szen �ll, hogy az utols� id�ben majd megnyilv�nuljon.  Abban �r�lni fogtok, noha most egy kicsit szomorkodnotok kell, mert k�l�nf�le k�s�rt�sek �rnek benneteket,  hogy a pr�b�t ki�llva hitetek, amely �rt�kesebb a t�z pr�b�lta veszend� aranyn�l, J�zus Krisztus megjelen�sekor m�lt� legyen a dics�retre, a dics�s�gre �s a tiszteletre. 

[Graduale]
!Ps 106:32; 106:31
v. And let them exalt him in the church of the people: and praise him in the~
chair of the ancients. Let the mercies of the Lord give glory to him, and his~
wonderful works to the children of men. Allel�ja, allel�ja
!Matt 16:18
v. And I say to thee: That thou art Peter; and upon this rock I will build my~
church, and the gates of hell shall not prevail against it. Alleluia.

[Tractus]
!Matt 16:18-19
v. Thou art Peter; and upon this rock I will build my church,
V. And the gates of hell shall not prevail against it. And I will give to thee the keys of the kingdom of heaven.
V. And whatsoever thou shalt bind upon earth, it shall be bound also in heaven:
V. And whatsoever thou shalt loose upon earth, it shall be loosed also in heaven.

[GradualeP]
Allel�ja, allel�ja
!Ps 106:15
v. Let the mercies of the Lord give glory to him, and his wonderful works to~
the children of men. Allel�ja.
!Matt 16:18
v. And I say to thee: That thou art Peter; and upon this rock I will build my~
church. Allel�ja.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 16:13-19
v. Amikor J�zus F�l�p Cez�re�j�nak vid�k�re �rt, megk�rdezte tan�tv�nyait�l: "Kinek tartj�k az emberek az Emberfi�t?"  �gy v�laszoltak: "Van, aki Keresztel� J�nosnak, van, aki Ill�snek, Jeremi�snak vagy valamelyik m�sik pr�f�t�nak."  J�zus most hozz�juk fordult: "H�t ti mit mondtok, ki vagyok?"  Simon P�ter v�laszolt: "Te vagy Krisztus, az �l� Isten Fia."  Erre J�zus azt mondta neki: "Boldog vagy, Simon, J�n�s fia, mert nem a test �s v�r nyilatkoztatta ki ezt neked, hanem az �n mennyei Aty�m.  �n is mondom neked: P�ter vagy, erre a szikl�ra �p�tem egyh�zamat, s az alvil�g kapui sem vesznek rajta er�t.  Neked adom a mennyek orsz�ga kulcsait. Amit megk�tsz a f�ld�n, a mennyben is meg lesz k�tve, s amit feloldasz a f�ld�n, a mennyben is fel lesz oldva." 

[Offertorium]
!Matt 16:18-19
v. And I say to thee: That thou art Peter; and upon this rock I will build my~
church, and the gates of hell shall not prevail against it. And I will give to~
thee the keys of the kingdom of heaven.

[Secreta]
We beseech You, O Lord, may the prayer of the blessed Apostle, Peter, recommend~
the petitions and offerings of Your Church, so that what we celebrate in his~
honor may obtain forgiveness for us.
$Per Dominum

[Communio]
!Matt 16:18
v. Thou art Peter; and upon this rock I will build my church.

[Postcommunio]
O Lord, may the gift we offer bring us joy, that as we praise Your wondrous work~
in Your Apostle Peter, so through him may we share Your generous forgiveness.
$Per Dominum

[Commemoratio Oratio]
!Commemoratio S. Pauli
O God, Who taught vast numbers of the Gentiles by the preaching of the blessed~
Apostle Paul, grant, we beseech You, that by honoring his memory, we may enjoy~
the benefit of his patronal intercession with You.
$Per Dominum
_
!Pro S. Priscae virg.
Grant, we beseech You, O almighty God, that we who celebrate the anniversary of~
the death of blessed Prisca, Your Virgin and Martyr, may rejoice on her yearly~
feast-day and benefit by the example of such great faith.
$Per Dominum

[Commemoratio Secreta]
!Pro S. Paulo
O Lord, make holy the offerings of Your people, through the prayer of Paul, Your~
Apostle, that those things which You have established as pleasing to You may be~
even more pleasing through his patronal intercession.
$Per Dominum
_
!Pro S. Priscae virg.
We beseech You, O Lord, that this sacrificial gift which we offer in~
commemorating the death of Your saints, may loose the bonds of our sins and~
bestow upon us the gifts of Your mercy.
$Per Dominum

[Commemoratio Postcommunio]
!Pro S. Paulo
Made holy by the sacrament of salvation, we beseech You, O Lord, that the~
prayers of him under whose patronal care You have granted us to be ruled may~
never fail us.
$Per Dominum
_
!Pro S. Priscae virg.
Filled with the sacrament of salvation, we beseech You, O Lord, to help us by~
the prayers of her whose feast we are celebrating.
$Per Dominum

