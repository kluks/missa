[Rank]
S. Gregorii Thaumaturgi Episcopi et Confessoris;;Semiduplex;;2;;vide C4

[Rank1570]
S. Gregorii Thaumaturgi Episcopi et Confessoris;;Simplex;;1.1;;vide C4

[RankTrident]
S. Gregorii Thaumaturgi Episcopi et Confessoris;;Duplex;;3;;vide C4

[Rank1960]
S. Gregorii Thaumaturgi Episcopi et Confessoris;;Duplex;;3;;vide C4

[Rule]
vide C4;
Gloria

[Introitus]
!Sir 45:30
v. The Lord made a covenant of friendship with him, and made him a prince; that~
he should possess the dignity of priesthood forever.
!Ps 131:1
Remember, O Lord, David and all his meekness.
&Gloria
v. The Lord made a covenant of friendship with him, and made him a prince; that~
he should possess the dignity of priesthood forever.

[Oratio]
Grant, we beseech You, almighty God, that the venerable feast of Your blessed~
Confessor and Bishop may increase our devotion and promote our salvation.
$Per Dominum

[Lectio]
Olvasm�ny J�zus S�r�k fia k�nyv�b�l
!Ecclus 44:16-27: 45:3- 20.
v. H�nok tetszett az Istennek, �s a paradicsomba vitetett, hogy a n�peknek b�nb�natot hirdessen.  No� t�k�letesnek �s igaznak tal�ltatott, �s a harag idej�n engesztel�s lett.  Az�rt hagyatott a f�ld�n marad�k, mikor a v�z�z�n volt.  �r�k sz�vets�get k�t�tt vele, hogy ne t�r�ltess�k el minden test v�z�z�nnel.  �brah�m sok nemzet nagy atyja, nem tal�l mag�hoz hasonl�t dics�s�gben; � megtartotta a F�ls�ges t�rv�ny�t, �s sz�vets�gben volt vele.  Annak test�n �llap�totta meg a sz�vets�get, �s � a kis�rtetben h�vnek tal�ltatott.  Az�rt dics�s�get adott neki nemzet�ben, megesk�dv�n: hogy megsokas�tja �t, mint a f�ld por�t;  �s mint a csillagokat, f�lmagasztalja ivad�k�t, �s �r�k�ss� teszi tengert�l tengerig, �s a foly�v�zt�l a f�ld hat�raig.  �s Izs�kkal szintazon m�don cselekedett atyj��rt, �brah�m�rt.  Minden nemzetek �ld�s�t neki adta az �r, �s sz�vets�g�t meger�s�tette J�kob fej�n.  Elismerte �t �ld�sai �ltal, �s �r�ks�get adott neki, �s r�szenkint eloszt� azt a tizenk�t nemzets�g k�z�tt.  �s megtartotta neki az irgalmass�g f�rfiait, kik kedvet tal�ltak minden ember szemei el�tt. 

[Graduale]
!Sir 44:16, 20.
Behold, a great priest, who in his days pleased God.
V. There was not found the like to him, who kept the law of the Most High.~
Alleluia, alleluia.
!Ps 109:4
V. You are a priest forever, according to the order of Melchisedec. Alleluia.

[Evangelium]
Evang�lium + szent M�rk Evang�lista k�nyv�b�l
!Mark 11:22-24
v. J�zus �gy felelt: "Ha hisztek Istenben,  bizony mondom nektek, hogyha valaki azt mondja ennek a hegynek: Emelkedj�l fel �s vesd magad a tengerbe, �s nem k�telkedik sz�v�ben, hanem hiszi, hogy amit mond, megt�rt�nik, akkor az csakugyan megt�rt�nik.  Ez�rt mondom nektek, hogy ha im�dkoztok �s k�ny�r�gt�k valami�rt, higgy�tek, hogy megkapj�tok, �s akkor val�ban teljes�l k�r�setek. 

[Offertorium]
!Ps 88:21-22
I have found David, My servant; with My holy oil I have anointed him, that My~
hand may be always with him, and that My arm may make him strong.

[Secreta]
May Your Saints, we beseech You, O Lord, everywhere make us joyful: so that,~
while we reflect upon their merits, we may enjoy their help.
$Per Dominum

[Communio]
!Luke 12:42
The faithful and prudent servant whom the master will set over his household to~
give them their ration of grain in due time.

[Postcommunio]
Grant, we beseech You, almighty God, that as we thank You for the favors we have~
received, we may, by the intercession of blessed Gregory, Your Confessor and~
Bishop, obtain still greater blessings.
$Per Dominum
