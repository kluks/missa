[Rank]
In Conversione S. Pauli Apostoli;;Duplex majus;;4;;ex Sancti/06-30

[RankNewcal]
In Conversione S. Pauli Apostoli;;Duplex Fest;;5;;ex Sancti/06-30

[Rule]
ex Sancti/06-30;
Gloria
Credo
Prefatio=Apostolis

[Introitus]
!2 Tim. 1:12
v. I know Whom I have believed, and I am certain that He is able to guard the~
trust committed to me, against that day; being a just Judge.
!Ps 138:1-2
O Lord, You have probed me and You know me; You know when I sit and when I~
stand.
&Gloria
v. I know Whom I have believed, and I am certain that He is able to guard the~
trust committed to me, against that day; being a just Judge.

[Oratio]
O God, Who taught the whole world by the preaching of Your blessed Apostle Paul,~
grant, we beseech You, that we who today celebrate his conversion may draw~
nearer to You by way of example.
$Per Dominum

[Commemoratio Oratio]
!For ST. PETER
O God, Who, when giving blessed Peter, Your Apostle, the keys of the heavenly~
kingdom, bestowed on him the power of binding and loosing, grant that by the~
help of his intercession we may be delivered from the bonds of our sins.
$Qui vivis

[Lectio]
Olvasm�ny az Apostolok Cselekedeib�l
!Acts 9:1-22
v. Saul m�g mindig lihegett a d�ht�l, �s hal�llal fenyegette az �r tan�tv�nyait. Elment a f�paphoz,  s arra k�rte, adjon neki aj�nl�levelet a damaszkuszi zsinag�g�hoz, hogy ha tal�l ott embereket, f�rfiakat vagy n�ket, akik ezt az utat k�vetik, megk�t�zve Jeruzs�lembe hurcolhassa �ket.  M�r Damaszkusz k�zel�ben j�rt, amikor az �gb�l egyszerre nagy f�nyess�g ragyogta k�r�l.  F�ldre hullott, �s hallotta, hogy egy hang �gy sz�l hozz�: "Saul, Saul, mi�rt �ld�z�l engem?"  Erre megk�rdezte: "Ki vagy, Uram?" Az folytatta: "�n vagyok J�zus, akit te �ld�z�l.  De �llj fel �s menj a v�rosba, ott majd megmondj�k neked, mit kell tenned."  �tit�rsainak elakadt a szavuk, mert hallott�k a hangot, de l�tni nem l�ttak semmit.  Saul felt�p�szkodott a f�ldr�l, kinyitotta a szem�t, de nem l�tott. �gy vezett�k be Damaszkuszba, k�zen fogva.  H�rom napig nem l�tott, nem evett, �s nem ivott.  Damaszkuszban volt egy Anani�s nev� tan�tv�ny. Az �r egy l�tom�sban megsz�l�totta: "Anani�s!" "Itt vagyok, Uram!" - felelte.  S az �r folytatta: "Kelj fel �s siess az Egyenes utc�ba. Keresd meg J�d�s h�z�ban a tarzuszi Sault. N�zd: im�dkozik."  �s l�tom�sban l�tott egy Anani�s nev� f�rfit, ahogy bel�p hozz�, �s r�teszi a kez�t, hogy visszanyerje l�t�s�t."  Anani�s tiltakozott: "Uram, sokakt�l hallottam, hogy ez az ember mennyit �rtott szentjeidnek Jeruzs�lemben.  Ide pedig megb�zat�sa van a f�papokt�l, hogy bilincsbe verjen mindenkit, aki seg�ts�g�l h�vja a nevedet."  Az �r azonban ezt v�laszolta neki: "Menj csak, mert v�lasztott ed�nyem �, hogy nevemet hordozza a pog�ny n�pek, a kir�lyok �s Izrael fiai k�z�tt.  Megmutatom majd neki, mennyit kell nevem�rt szenvednie."  Anani�s elment, bet�rt a h�zba, �s e szavakkal tette r� kez�t: "Saul testv�r, Urunk J�zus k�ld�tt, aki megjelent neked idej�vet az �ton, hogy visszakapd szemed vil�g�t, �s eltelj a Szentl�lekkel."  Azon nyomban valami h�lyogf�le v�lt le szem�r�l, �s visszanyerte l�t�s�t. Fel�llt, megkeresztelkedett,  majd evett �s er�re kapott. N�h�ny napig ottmaradt Damaszkuszban a tan�tv�nyokkal,  �s m�ris tan�totta a zsinag�g�kban, hogy J�zus az Isten Fia.  Akik hallgatt�k, mind csod�lkoztak, mondv�n: "H�t nem ez t�rt veszt�kre Jeruzs�lemben azoknak, akik ezt a nevet seg�ts�g�l h�vj�k? Ide is nem az�rt j�tt, hogy bilincsbe verve a f�papok el� hurcolja �ket?"  Saul azonban mind hat�rozottabban l�pett fel, s mert azt bizony�totta, hogy J�zus a Messi�s, zavarba hozta a damaszkuszi zsid�kat. 

[Graduale]
!Gal 2:8-9
He Who worked in Peter for the apostleship, worked also in me among the~
Gentiles: and they recognized the grace of God that was given to me.
V. The grace of God in me has not been fruitless; but His grace always remains~
in me. Alleluia, alleluia.
V. A great saint is Paul, a vessel of election, he is indeed worthy to be~
glorified, for he was made worthy to sit upon one of the twelve thrones.~
Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt. 19:27-29
v. Akkor P�ter vette �t a sz�t: "N�zd, mi minden�nket elhagytuk, �s k�vett�nk t�ged. Mi lesz h�t a jutalmunk?"  J�zus �gy v�laszolt: "Bizony mondom nektek, ti, akik k�vettetek, a meg�jul�skor, amikor az Emberfia dics�s�ges tr�nj�ra �l, ti is ott �lt�k majd vele tizenk�t tr�non �s �t�lkezni fogtok Izrael tizenk�t t�rzse f�l�tt.  Aki nevem�rt elhagyja otthon�t, testv�reit, n�v�reit, apj�t, anyj�t, feles�g�t, gyermekeit vagy a f�ldj�t, sz�zannyit kap, s az �r�k �let lesz az �r�ks�ge. 

[Offertorium]
!Ps 138:17
To me, Your friends, O God, are made exceedingly honorable; their principality~
is exceedingly strengthened.

[Secreta]
O Lord, make holy the offerings of Your people, through the prayer of Paul, Your~
Apostle, that those things which You have established as pleasing to You may be~
more pleasing through his patronal intercession.
$Per Dominum

[Commemoratio Secreta]
!For ST. PETER
We beseech You, Lord, may the prayer of the blessed Apostle Peter recommend the~
petitions and offerings of Your Church, so that what we celebrate in his honor~
may obtain forgiveness for us.
$Per Dominum

[Communio]
!Matt 19:28-29
Amen I say to you, that you who have left all things, and followed Me, shall~
receive a hundredfold, and shall possess life everlasting.

[Postcommunio]
Made holy by the sacrament of salvation, we beseech You, O Lord, that the~
prayers of him under whose patronal care You have granted us to be ruled may~
never fail us.
$Per Dominum

[Commemoratio Postcommunio]
!For ST. PETER
O Lord, may the gift we offer bring us joy, that as we praise Your wondrous work~
in Your Apostle Peter, so through him may we share Your generous forgiveness.
$Per Dominum
