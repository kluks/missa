[Rank]
S. Damasi Papae et confessoris;;Semiduplex;;2.5;;vide C4b

[Rank1960]
S. Damasi Papae et confessoris;;Duplex;;3;;vide C4b

[RankNewcal]
S. Damasi Papae et confessoris;;Semiduplex;;2.5;;vide C4b

[Rule]
OPapaC=Damasus;
vide C4;
Gloria

[Introitus]
!John 21:15-17
v. If you love Me, Simon Peter, feed My lambs, feed my sheep.
!Ps 29:1
I will extol You, O Lord, for You drew me clear and did not let my enemies~
rejoice over me.
&Gloria
v. If you love Me, Simon Peter, feed My lambs, feed my sheep.

[Oratio]
Eternal Shepherd, look with favor upon Your flock. Safeguard and shelter it~
forevermore through blessed Damasus Supreme Pontiff, whom You constituted~
shepherd of the whole church.
$Per Dominum

[Lectio]
Olvasm�ny szent P�ter apostol els� level�b�l
!1 Pet. 5:1-4; 5:10-11
v. Akik k�z�letek el�lj�r�k, azokat mint magam is el�lj�r� �s Krisztus szenved�seinek tan�ja, s egyszer majd kinyilv�nul� dics�s�g�nek is r�szese, k�rem:  Legeltess�tek az Istennek r�tok b�zott ny�j�t, viselj�tek gondj�t, ne k�nyszerb�l, hanem �nk�nt, az Isten sz�nd�ka szerint ne haszonles�sb�l, hanem buzg�s�gb�l.  Ne zsarnokoskodjatok a v�lasztottak f�l�tt, hanem legyetek a ny�jnak p�ldak�pei.  Ha majd megjelenik a legf�bb p�sztor, elnyeritek a dics�s�g hervadhatatlan koszor�j�t.  A minden kegyelem Istene pedig, aki Krisztusban �r�k dics�s�gre h�vott meg benneteket, r�vid szenved�s ut�n maga fog majd titeket t�k�letess� tenni, meger�s�teni, megszil�rd�tani �s biztos alapra helyezni.  Neki legyen dics�s�g, �s �v� legyen az uralom �r�kk�n-�r�kk�! Amen. 

[Graduale]
!Ps 106:32, 31.
Let them extol him in the assembly of the people and praise him in the council~
of the elders.
V. Let them give thanks to the Lord for His kindness and His wondrous deeds to~
the children of men. Alleluia, alleluia.
!Matt 16:18
You are Peter, and upon this rock I will build My Church. Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt. 16:13-19
v. Amikor J�zus F�l�p Cez�re�j�nak vid�k�re �rt, megk�rdezte tan�tv�nyait�l: "Kinek tartj�k az emberek az Emberfi�t?"  �gy v�laszoltak: "Van, aki Keresztel� J�nosnak, van, aki Ill�snek, Jeremi�snak vagy valamelyik m�sik pr�f�t�nak."  J�zus most hozz�juk fordult: "H�t ti mit mondtok, ki vagyok?"  Simon P�ter v�laszolt: "Te vagy Krisztus, az �l� Isten Fia."  Erre J�zus azt mondta neki: "Boldog vagy, Simon, J�n�s fia, mert nem a test �s v�r nyilatkoztatta ki ezt neked, hanem az �n mennyei Aty�m.  �n is mondom neked: P�ter vagy, erre a szikl�ra �p�tem egyh�zamat, s az alvil�g kapui sem vesznek rajta er�t.  Neked adom a mennyek orsz�ga kulcsait. Amit megk�tsz a f�ld�n, a mennyben is meg lesz k�tve, s amit feloldasz a f�ld�n, a mennyben is fel lesz oldva." 

[Offertorium]
!Jer 1:9-10
See, I place My words in your mouth! I set you over nations and over kingdoms,~
to root up and to tear down, and to build and to plant.

[Secreta]
Having offered You our gifts, we beseech You, O Lord, mercifully to enlighten~
Your Church, so that Your flock may everywhere prosper, and its shepherds, under~
Your guidance, be rendered acceptable to You.
$Per Dominum

[Communio]
!Matt 16:18
You are Peter, and upon this rock I will build My Church.

[Postcommunio]
Since Your Church has been nourished by sacred food, govern her in Your clemency,~
we beseech You, O Lord, so that under the guidance of Your almighty rule she may~
enjoy ever greater freedom and remain steadfast in purity of faith.
$Per Dominum
