[Rank]
Die Octava Epiphaniae;;Duplex majus;;4;;ex Sancti/01-06

[Rank1960]
Commemoratio Baptismi Domini;;Duplex II. classis;;5;;ex Sancti/01-06

[Rule]
ex Sancti/01-06;
Gloria
Credo
Prefatio=Epi

[Introitus]
!Mal 3:1:1; Par. 29:12
v. Behold, the Lord and Ruler is come; and the kingdom is in His hand, and power,~
and dominion.
!Ps 71:1
O God, with Your judgment endow the King, and with Your justice, the King�s Son.
&Gloria
v. Behold, the Lord and Ruler is come; and the kingdom is in His hand, and power,~
and dominion.

[Oratio]
O God, Whose only-begotten Son appeared in the substance of our flesh, grant, we~
pray You, that we who acknowledge His outward likeness to us may deserve to be~
inwardly refashioned in His image.
$Qui tecum

[Lectio]
Olvasm�ny Izi�s pr�f�ta k�nyv�b�l
!Isa 60:1-6
v. Kelj f�l, ragyogj f�l, mert el�rkezett vil�goss�god, �s az �r dics�s�ge felragyogott f�l�tted!  Mert m�g s�t�ts�g bor�tja a f�ldet, �s hom�ly a nemzeteket, de f�l�tted ott ragyog az �r, �s dics�s�ge megnyilv�nul rajtad.  N�pek j�nnek vil�goss�godhoz, �s kir�lyok a benned t�madt f�nyess�ghez.  Hordozd k�r�l tekintetedet �s l�sd: mind egybegy�lnek �s idej�nnek hozz�d. Fiaid messze t�volb�l �rkeznek, s a l�nyaidat �l�kben hozz�k.  Ennek l�tt�ra f�lder�lsz, sz�ved dobog az �r�mt�l �s kit�gul. Mert fel�d �ramlik a tengerek gazdags�ga, �s ide �z�nlik a nemzetek kincse.  Tev�k �radata bor�t majd el, Midi�n �s Efa dromed�rjai. Mind S�b�b�l j�nnek; aranyat �s t�mj�nt hoznak �s az �r dics�s�g�t zengik. 

[Graduale]
!Isa 60:6, 1.
All they from Saba shall come, bringing gold and frankincense, and proclaiming~
the praises of the Lord.
V. Rise up in splendor, O Jerusalem, for the glory of the Lord shines upon you.~
Alleluia, alleluia.
!Matt 2:2
V. We have seen His star in the East: and have come with gifts to worship the~
Lord. Alleluia.

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 1:29-34
v. M�snap, amikor l�tta, hogy J�zus fel�je tart, �gy sz�lt: "N�zz�tek, az Isten B�r�nya! � veszi el a vil�g b�neit.  R�la mondtam: A nyomomba l�p valaki, aki nagyobb n�lam, mert el�bb volt, mint �n.  �n sem ismertem, de az�rt j�ttem v�zzel keresztelni, hogy megismertessem Izraellel."  Ezut�n J�nos tan�s�totta: "L�ttam a Lelket, amint galamb alakj�ban lesz�llt r� a mennyb�l, s rajta is maradt.  Magam sem ismertem, de aki v�zzel keresztelni k�ld�tt, azt mondta: Akire l�tod, hogy lesz�ll a L�lek s rajta is marad, az majd Szentl�lekkel fog keresztelni.  L�ttam �s tan�skodom r�la, hogy � az Isten Fia." 

[Offertorium]
!Ps 71:10-11
The kings of Tharsis and the Isles shall offer gifts; the kings of Arabia and~
Saba shall bring tribute. All kings shall pay Him homage, all nations shall~
serve Him.

[Secreta]
We bring You offerings, O Lord, for the epiphany of Your Son, Who has been born,~
humbly beseeching You that, as He is the creator of our gifts, so also He may~
mercifully receive them, our Lord Jesus Christ.
$Qui tecum

[Communio]
!Matt 2:2
We have seen His star in the East and have come with gifts to worship the Lord.

[Postcommunio]
We pray You, O Lord, to go before us at all times and in all places with Your~
heavenly light, that we may discern with clear sight and receive with the right~
disposition the sacrament of which You have willed that we should partake.
$Per Dominum
