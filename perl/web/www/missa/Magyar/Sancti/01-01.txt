[Rank]
In Circumcisione Domini;;Duplex II classis;;5.1;;ex Sancti/12-25m3
[Rank1960]
Die Octavae Nativitatis Domini;;Duplex I classis;;6;;ex Sancti/12-25m3

[RankNewcal]
Die Octavae Nativitatis Domini;;Duplex I classis;;6;;ex Sancti/12-25m3

[Rule]
ex Sancti/12-25m3;
Gloria
Credo
Prefatio=Nat

[Introitus]
!Isa 9:6
v. A Child is born to us, a Son is given to us; upon His shoulder dominion rests; and His name shall be called the Angel of great counsel. 
!Ps 97:1 
Sing to the Lord a new song, for He has done wondrous deeds. 
&Gloria
v. A Child is born to us, a Son is given to us; upon His shoulder dominion rests; and His name shall be called the Angel of great counsel. 

[Oratio] 
O God, You Who by the fruitful virginity of blessed Mary, have bestowed upon  mankind the rewards of eternal salvation, grant, we beseech You, that we may enjoy the intercession of her through whom we have been found worthy to receive among us the author of life, our Lord Jesus Christ Your Son. 
$Qui tecum

[Lectio]
Olvasm�ny szent P�l apostol Tituszhoz irott level�b�l
!Titus 2:11-15
v. Megjelent ugyanis megv�lt� Isten�nk kegyelme minden ember sz�m�ra,  s arra tan�t minket, hogy szak�tsunk az istentelens�ggel �s az evil�gi v�gyakkal, �lj�nk fegyelmezetten, szent�l �s buzg�n ezen a vil�gon.  V�rjuk rem�ny�nk boldog beteljes�l�s�t: a nagy Istennek �s �dv�z�t�nknek, J�zus Krisztusnak dics�s�ges elj�vetel�t,  aki �nmag�t adta �rt�nk, hogy minden gonoszs�gt�l megv�ltson, megtiszt�tson, �s j�tettekben buzgolkod�, v�lasztott n�p�v� tegyen.  Err�l besz�lj, erre buzd�ts �s figyelmeztess teljes hat�rozotts�ggal. Senki meg ne vessen! 

[Graduale]
!Ps 97:3-4, 2.
All the ends of the earth have seen the salvation by our God. Sing joyfully to God, all you lands. 
V. The Lord has made His salvation known: in the sight of the nations He has revealed His justice. Alleluia, alleluia. 
!Heb 1:1-2 
V. God, Who in diverse ways spoke in times past to the fathers by the prophets; last of all, in these days, has spoken to us by His Son. Alleluia.

[Evangelium]
Evang�lium + szent Luk�cs Evang�lista k�nyv�b�l
!Luke 2:21
v. Amikor eltelt a nyolc nap �s k�r�lmet�lt�k, a J�zus nevet adt�k neki, ahogy az angyal nevezte, miel�tt m�g a m�hben megfogamzott volna. 

[Offertorium]
!Ps 88:12, 15.
Yours are the heavens, and Yours is the earth; the world and its fullness You have founded. Justice and judgment are the foundation of Your throne.

[Secreta]
Accept, we beseech You, O Lord, our offerings and prayers; cleanse us by this heavenly rite, and in Your mercy, hear us.  
$Per Dominum

[Communio]
!Ps 97:3
All the ends of the earth have seen the salvation by our God.

[Postcommunio]
May this Communion, O Lord, cleanse us from guilt and, by the intercession of the Blessed Virgin Mary, mother of God, impart to us heavenly healing. 
$Per eumdem
