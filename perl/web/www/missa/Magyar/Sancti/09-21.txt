[Rank]
S. Matthei Apostoli et Evangelistae;;Duplex II classis;;5.1;;ex C1a

[Rank1960]
S. Matthei Apostoli et Evangelistae;;Duplex II classis;;5;;ex C1a

[Rule]
ex C1a;
Gloria
Credo
Prefatio=Apostolis

[Introitus]
!Ps 36:30-31
v. The mouth of the just man tells of wisdom and his tongue utters what is~
right. The law of his God is in his heart.
!Ps 36:1
Be not vexed over evildoers, nor jealous of those who do wrong.
&Gloria
v. The mouth of the just man tells of wisdom and his tongue utters what is~
right. The law of his God is in his heart.

[Oratio]
May we be helped, O Lord, by the prayers of the blessed Apostle and Evangelist~
Matthew, that what we ourselves cannot obtain, may be granted by his~
intercession.
$Per Dominum

[Lectio]
Olvasm�ny Ezekiel pr�f�ta k�nyv�b�l
!Ezek 1:10-14
v. Arcuk emberi archoz hasonl�tott, �s jobb fel�l mind a n�gynek oroszl�narca volt, bal fel�l meg mind a n�gynek bikaarca volt, s mind a n�gynek sasarca volt.  Sz�rnyaik fel�l ki voltak terjesztve. K�t sz�rnya mindegyiknek �ssze�rt, kett� meg a test�ket f�dte.  Mindegyik egyenesen ment maga el�tt, oda mentek, ahova a l�lek ir�ny�totta �ket, nem fordultak meg, amikor mentek.  Az �l�l�nyek k�z�tt olyasmit l�ttam, mint az �g� sz�nf�klya, amely imbolygott az �l�l�nyek k�z�tt. A t�z lobogott, �s vill�mok t�rtek el� a t�zb�l.  Az �l�l�nyek meg j�ttek-mentek, ak�r a vill�mok. 

[Graduale]
!Ps 111:1-2
Happy the man who fears the Lord, who greatly delights in His commands.
V. His posterity shall be mighty upon the earth; the upright generation shall be~
blessed. Alleluia, alleluia.
V. The glorious choir of Apostles praises You, O Lord. Alleluia.

[Evangelium]
Evang�lium + szent M�t� Apostol k�nyv�b�l
!Matt 9:9-13
v. Amikor J�zus tov�bbment, l�tott egy M�t� nev� embert, amint ott �lt a v�mn�l. Sz�lt neki: "K�vess engem!" Az fel�llt �s k�vette.  Amikor k�s�bb vend�g�l l�tta h�z�ban, sok v�mos meg b�n�s j�tt oda, s J�zussal �s tan�tv�nyaival egy�tt asztalhoz telepedett.  Ezt l�tva a farizeusok megk�rdezt�k tan�tv�nyait�l: "Mi�rt eszik mesteretek v�mosokkal �s b�n�s�kkel?"  J�zus meghallotta, �s �gy v�laszolt: "Nem az eg�szs�geseknek kell az orvos, hanem a betegeknek.  Menjetek �s tanulj�tok meg, mit jelent: Irgalmass�got akarok, nem �ldozatot. Nem az�rt j�ttem, hogy az igazakat h�vjam, hanem hogy a b�n�s�ket." 

[Offertorium]
!Ps 20:4-5
O Lord, You placed on his head a crown of pure gold; he asked life of You, and~
You gave it to him, alleluia.

[Secreta]
May the prayers of the blessed Apostle and Evangelist, Matthew, recommend to You~
the offering made by Your Church, which is taught by his glorious preaching.
$Per Dominum

[Communio]
!Ps 20:6
Great is his glory in Your victory; majesty and splendor You conferred upon him, O~
Lord.

[Postcommunio]
Having received Your sacrament, O Lord, we implore You, that what we have~
celebrated in honor of St. Matthew, Your Apostle and Evangelist, may by his~
intercession, benefit us as a healing remedy.
$Per Dominum
