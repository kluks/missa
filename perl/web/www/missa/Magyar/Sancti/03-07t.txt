[Rank]
S. Thomae de Aquino Confessoris Ecclesiae Doctoris.;;Duplex;;3;;vide C5a

[Name]
Thoma

[Rule]
vide C5a;
Gloria
CredoDa

[Oratio]
O God, Who enlightened Your Church with the wondrous learning of blessed Thomas, Your Confessor, and enriched her through his holy life, grant us, we beseech You, both to understand what he taught and by following his example to do what he did.
$Per Dominum

[Lectio]
Olvasm�ny a B�lcsess�g k�nyv�b�l
!Wis 7:7 - 14.
v. Ez�rt k�ny�r�gtem, �s megkaptam az okoss�got; esdekeltem, �s lesz�llt r�m a b�lcsess�g lelke.  T�bbre becs�ltem a jogarn�l �s a tr�nn�l, �s hozz� m�rten semminek tartottam a gazdags�got.  A f�lbecs�lhetetlen dr�gak�vet sem �ll�tottam vele egy sorba; mert mellette minden arany csak egy mar�k homok, �s vele �sszem�rve az ez�st csak s�rnak sz�m�t.  Jobban szerettem eg�szs�gn�l �s sz�ps�gn�l, �s birtokl�s�t a vil�goss�gn�l is t�bbre tartottam; mert a f�ny, amely bel�le �rad, nem alszik ki soha.  De vele egy�tt a t�bbi javak is mind hozz�m j�ttek, m�rhetetlen gazdags�g volt a kez�ben.  �r�ltem mindennek, mert a b�lcsess�g volt a vez�r�k; nem tudtam, hogy a sz�l�anyjuk is az.  Csal�rds�g n�lk�l saj�t�tottam el, irigys�g n�lk�l adom tov�bb, nem rejtem el gazdags�g�t.  Hisz kifogyhatatlan kincs az emberek sz�m�ra. Akik szert tettek r�, megszerezt�k Isten bar�ts�g�t, mert aj�nlott�k �ket a fegyelem adom�nyai. 

