[Rank]
S. Stanislai Episcopi Martyri;;Duplex;;3;;vide C2

[Rule]

[Introitus]
!Ps 63:3
v. You have sheltered me, against the council of malefactors, alleluia, against~
the tumult of evildoers, alleluia, alleluia.
!Ps 63:2
Hear, O God, my voice in lament; from the dread enemy preserve my life.
&Gloria
v. You have sheltered me, against the council of malefactors, alleluia, against~
the tumult of evildoers, alleluia, alleluia.

[Oratio]
O God, in defense of Whose honor Stanislaus, the glorious Bishop, died by the~
swords of wicked men, grant, we beseech You, that all who seek his help may~
obtain salvation as a result of his prayers.
$Per Dominum

[Lectio]
Olvasm�ny a B�lcsess�g k�nyv�b�l
!Wis 5:1-5
v. Akkor az igaz teljes biztons�ggal �ll szemben azokkal, akik sanyargatt�k, �s lebecs�lt�k a f�radoz�s�t.  Amikor ezt l�tj�k, iszony� f�lelem fogja el �ket, �s nem tudnak hov� lenni; annak nem v�rt �dv�ss�ge miatt.  B�nk�dva sz�lnak egym�shoz, �s s�hajtoznak lelk�k f�lelm�ben:  "Ez az, akit egykor kinevett�nk �s akib�l g�nyt �zt�nk. Mi esztelenek! �letm�dj�t �r�lts�gnek tartottuk �s hal�l�t dicstelennek.  Hogy lehet az, hogy Isten fiai k�z� sorolt�k �s a szentek k�z�tt van az oszt�lyr�sze? 

[Graduale]
Alleluia, alleluia.
!Ps 88:6
The heavens proclaim Your wonders, O Lord, and Your faithfulness in the assembly~
of the holy ones. Alleluia.
!Ps 20:4
You placed on his head, O Lord, a crown of pure gold.

[Evangelium]
Evang�lium + szent J�nos Apostol k�nyv�b�l
!John 15:1-7.
v. �n vagyok az igazi sz�l�t�, s Aty�m a sz�l�m�ves.  Minden sz�l�vessz�t, amely nem hoz gy�m�lcs�t, lemetsz r�lam, azt pedig, amely terem, megtiszt�tja, hogy m�g t�bbet teremjen.  Ti m�r tiszt�k vagytok a tan�t�s �ltal, amelyet hirdettem nektek.  Maradjatok h�t bennem, s akkor �n is bennetek maradok. Amint a sz�l�vessz� nem teremhet maga, csak ha a sz�l�t�n marad, �gy ti sem, ha nem maradtok bennem.  �n vagyok a sz�l�t�, ti a sz�l�vessz�k. Aki bennem marad, s �n benne, az b� term�st hoz. Hisz n�lk�lem semmit sem tehettek.  Aki nem marad bennem, azt kivetik, mint a sz�l�vessz�t, ha elsz�rad. �sszeszedik, t�zre vetik �s el�g.  Ha bennem maradtok, �s tan�t�som is bennetek marad, akkor b�rmit akartok, k�rjetek, �s megkapj�tok. 

[Offertorium]
!Ps 88:6
The heavens proclaim Your wonders, O Lord, and Your faithfulness in the assembly~
of the holy ones, alleluia, alleluia.

[Secreta]
Look with favor, O Lord, upon our humble prayers which we are offering in~
commemoration of Your Saints, so that we, who have no confidence in our own~
righteousness, may be helped by the merits of those who have been pleasing to~
You.
$Per Dominum

[Communio]
!Ps 63:11
The just man is glad in the Lord and takes refuge in Him; all the upright of~
heart shall be praised, alleluia, alleluia.

[Postcommunio]
Filled with the sacrament of salvation, we beseech You, O Lord, that we may be~
helped by the prayers of those whose feast-day we celebrate.
$Per Dominum
